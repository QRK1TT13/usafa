# ##############################
#
# Hunter Manter
#
# DOCUMENTATION: None
#
# ###############################

# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util
import math
import time

from game import Agent


class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """

    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE Question 1***"
        pos = currentGameState.getPacmanPosition()
        score = 0
        for x in successorGameState.getGhostPositions():
            dist = util.manhattanDistance(newPos, x)
            if (dist < 3):
                score += 5 * dist - 30
        food = currentGameState.getFood()
        nearestFood = 100
        rows=0
        for x in food:
            rows+=1
        for x in range(rows):
            for y in range(len(food[x])):
                if food[x][y] == True:
                    temp = manhattanDistance(newPos,(x,y))
                    if temp<nearestFood:
                        nearestFood=temp
        score -= nearestFood
        return score


def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()


class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn='scoreEvaluationFunction', depth='2'):
        self.index = 0  # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (Question 2)
    """
    def minimize(self, depth,agent, next):
        if depth == self.depth and agent%next.getNumAgents() == next.getNumAgents()-1:
            actions = next.getLegalActions(agent%next.getNumAgents())
            succ = []
            for action in actions:
                val = self.evaluationFunction(next.generateSuccessor(agent%next.getNumAgents(), action))
                succ.append(val)
            return succ[succ.index(min(succ))]
        else:
            actions = next.getLegalActions(agent%next.getNumAgents())
            succ = []
            for action in actions:
                val = self.value(depth, agent+1,next.generateSuccessor(agent%next.getNumAgents(), action))
                succ.append(val)
            return succ[succ.index(min(succ))]

    def maximize(self, depth, agent, next):
        if depth == self.depth and agent%next.getNumAgents() == next.getNumAgents()-1:
            actions = next.getLegalActions(agent%next.getNumAgents())
            succ = []
            for action in actions:
                val = self.evaluationFunction(next.generateSuccessor(agent%next.getNumAgents(), action))
                succ.append(val)
            return succ[succ.index(max(succ))]
        else:
            actions = next.getLegalActions(agent%next.getNumAgents())
            succ = []
            for action in actions:
                val = self.value(depth, agent+1,next.generateSuccessor(agent%next.getNumAgents(), action))
                succ.append(val)
            return succ[succ.index(max(succ))]

    def value(self, depth, agent, next):
        if next.isWin() or next.isLose():
            return self.evaluationFunction(next)
        if 0 == agent%next.getNumAgents():
            return self.maximize(depth+1, agent, next)
        else:
            return self.minimize(depth, agent, next)


    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        depth = 1
        agent = 1
        actions = gameState.getLegalActions(0)
        succ = []
        for action in actions:
            next = gameState.generateSuccessor(self.index, action)
            val = self.value(depth,agent, next)
            succ.append(val)
        return actions[succ.index(max(succ))]


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 4 - optional)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (Question 3)
    """

    def minimize(self, depth, agent, next):
        if depth == self.depth and agent % next.getNumAgents() == next.getNumAgents() - 1:
            actions = next.getLegalActions(agent % next.getNumAgents())
            succ = []
            for action in actions:
                val = self.evaluationFunction(next.generateSuccessor(agent % next.getNumAgents(), action))
                succ.append(val)
            return succ[succ.index(min(succ))]
        else:
            actions = next.getLegalActions(agent % next.getNumAgents())
            succ = []
            for action in actions:
                val = self.value(depth, agent + 1, next.generateSuccessor(agent % next.getNumAgents(), action))
                succ.append(val)
            return sum(succ)/len(succ)

    def maximize(self, depth, agent, next):
        if depth == self.depth and agent % next.getNumAgents() == next.getNumAgents() - 1:
            actions = next.getLegalActions(agent % next.getNumAgents())
            succ = []
            for action in actions:
                val = self.evaluationFunction(next.generateSuccessor(agent % next.getNumAgents(), action))
                succ.append(val)
            return succ[succ.index(max(succ))]
        else:
            actions = next.getLegalActions(agent % next.getNumAgents())
            succ = []
            for action in actions:
                val = self.value(depth, agent + 1, next.generateSuccessor(agent % next.getNumAgents(), action))
                succ.append(val)
            return succ[succ.index(max(succ))]

    def value(self, depth, agent, next):
        if next.isWin() or next.isLose():
            return self.evaluationFunction(next)
        if 0 == agent % next.getNumAgents():
            return self.maximize(depth + 1, agent, next)
        else:
            return self.minimize(depth, agent, next)

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        depth = 1
        agent = 1
        actions = gameState.getLegalActions(0)
        succ = []
        for action in actions:
            next = gameState.generateSuccessor(self.index, action)
            val = self.value(depth, agent, next)
            succ.append(val)
        return actions[succ.index(max(succ))]


def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5 - optional).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"

    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()


# Abbreviation
better = betterEvaluationFunction
