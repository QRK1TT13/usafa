SECTION .text
message db "Hello world", 0

SECTION .data

global _start

_start:
        mov QWORD [RSP-8], 0x0	;8 bytes of padding
	mov dword [rsp-12], 0x0100007f
	mov word [rsp-14], 0x5c11
	mov word [rsp-16], 0x02
	sub rsp, 16

	mov rdi, 0x02
	mov rsi, 1
	mov rdx, 0
	mov rax, 41
	SYSCALL
	mov r10, rax

	mov rdi, r10
	mov rsi, rsp
	mov rdx, 16
	mov rax, 42
	SYSCALL

	mov rax, 44
	mov rdi, r10
	mov rsi, message
	mov rdx, 0x0c
	mov rcx, 0x00
	mov r8, 0x00
	mov r9, 0x00
	SYSCALL

        mov rax, 60
        mov rdi, 1
        SYSCALL

