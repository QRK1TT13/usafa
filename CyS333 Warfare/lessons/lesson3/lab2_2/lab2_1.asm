SECTION .data
msg db "hello world!", 10, 0

SECTION .text
global _start

_start:
	mov rax, $
	mov rbx, [rel $]
	lea rcx, [rel $]
	mov rcx, [rel msg]
	mov r9, [rel msg+6]
	mov r10, msg+6
	mov eax, 60
	mov rdi, 1
	SYSCALL
