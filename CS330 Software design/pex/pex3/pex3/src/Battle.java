import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

import static javax.swing.JOptionPane.*;

/**
 * CS 330 - Mini-Massive - Phase 1
 * <p>
 * large-scale autonomous life simulation along the lines of the Massive software first developed for The Lord of
 * the Rings software and subsequently used in many applications. (See http://www.massivesoftware.com/index.html).
 *
 * @author Steve Hadfield
 * @date 7 Sep 2019
 */
public class Battle {

    private static final int BLUE_ARMY_SIZE = 50;
    private static final int RED_ARMY_SIZE = 50;

    private static final int WINDOW_WIDTH = 800;
    private static final int WINDOW_HEIGHT = 600;
    private static final int TIME_STEP = 50;

    private boolean pause = true;
    DrawingPanel panel;

    private boolean initialized = false;
    boolean guiStart = false;

    private ArrayList<Army> armies = new ArrayList<>();

    private Random rand = new Random();

    /**
     * Zero-argument constructor
     */
    public Battle() {
        // set up the DrawingPanel and Graphics2D objects

        panel = new DrawingPanel(WINDOW_WIDTH, WINDOW_HEIGHT);
        panel.setWindowTitle("CS330 Mini-Massive Reference Solution");

        Graphics2D g = panel.getGraphics();
        panel.setBackground(Color.LIGHT_GRAY);

        // connect Army and Warrior classes to graphics display

        Army.setGraphics(g);
        Army.setScreenWidth(WINDOW_WIDTH);
        Army.setScreenHeight(WINDOW_HEIGHT);

        Warrior.setGraphics(g);
        Warrior.setScreenWidth(WINDOW_WIDTH);
        Warrior.setScreenHeight(WINDOW_HEIGHT);

        // Create the two armies
        armies.add(new Army(BLUE_ARMY_SIZE, Army.LEFT_SIDE,
                3.0, 10, Color.BLUE, 0.98, 0.8, false));
        armies.add(new Army(RED_ARMY_SIZE, Army.RIGHT_SIDE,
                4.0, 6, Color.RED, 0.7, 0.8, true));


        // set the armies as adversaries
        armies.get(0).setAdversary(armies.get(1));
        armies.get(1).setAdversary(armies.get(0));

        initialized = true;
    }

    /**
     * Creates two armies and has them interact in a battle.
     */
    public void doBattle() {

        // have armies draw themselves

        for (Army army : armies) {
            army.draw();
        }

        panel.setWindowTitle("CS330 PEX2 Mini-Massive Reference Solution (Space key to pause/resume. Left-click to end)");

        panel.copyGraphicsToScreen();

        // animate the battle

        while (!panel.mouseClickHasOccurred(DrawingPanel.LEFT_BUTTON)) {

            if (panel.keyHasBeenHit(DrawingPanel.SPACE_KEY)) pause = !pause;

            if (!pause) {

                // clear last scene

                panel.setBackground(Color.LIGHT_GRAY);

                // move each army's warriors
                for (Army army : armies) {
                    army.move();
                }

                // update to their next position (from move())
                for (Army army : armies) {
                    army.updateToNextPosition();
                }

                // have warriors fight at their new positions
                for (Army army : armies) {
                    army.fight();
                }

                // draw each army in new positions

                for (Army army : armies) {
                    army.draw();
                }

                panel.setWindowTitle("(PEX3)");
            } // end of if not paused

            panel.sleep(TIME_STEP); // pause to pace the animation
            panel.copyGraphicsToScreen();  // update the graphics
        }
        panel.closeWindow();  // all done

    } // end main() method

    /**
     * gets score of army
     */
    void progressUpdate() {
        int army = gui.getSelected();
        int enemies = 0;
        int friendly = 0;
        for (int i = 0; i < armies.size(); i++) {
            enemies += armies.get(i).getNumWarriors();
            if (i == army) {
                friendly = armies.get(i).getNumWarriors();
            }
        }
        gui.setProgress((int) ((double) friendly / enemies * 100));
    }

    /**
     * @return if battle is paused
     */
    boolean isPaused() {
        return pause;
    }

    /**
     * Resumes the battle
     */
    void resume() {
        pause = false;
    }

    /**
     * pauses the battle
     */
    void pause() {
        pause = true;
    }

    /**
     * @param selectedIndex army index that gets color set
     */
    public void setArmyColour(int selectedIndex) {
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
        Color randomColor = new Color(r, g, b);
        armies.get(selectedIndex).setNewColor(randomColor);
    }

    /**
     * @param selectedIndex which army to look at
     * @return string representation of color
     */
    public String getColor(int selectedIndex) {
        return ("RGB is: " + armies.get(selectedIndex).getColor().getRed() + ", " + armies.get(selectedIndex).getColor().getGreen() + ", " + armies.get(selectedIndex).getColor().getBlue());
    }

    /**
     * wait for battle to be initialized
     */
    public void waitForInit() {
        while (initialized == false) {
        }
    }

    /**
     * Redraw the window while paused
     */
    public void redrawPaused() {
        panel.setBackground(Color.LIGHT_GRAY);
        for (Army army : armies) {
            army.draw();
        }
    }

    /**
     * @param selectedIndex army to set
     * @param value         speed to set
     */
    public void setArmySpeed(int selectedIndex, double value) {
        armies.get(selectedIndex).setSpeed(value);
    }

    /**
     * @param selectedIndex army to set
     * @param v             strength to set to
     */
    public void setArmyStrength(int selectedIndex, double v) {
        armies.get(selectedIndex).setStrength(v);
    }

    /**
     * @param selectedIndex army to get
     * @return speed
     */
    public double getSpeed(int selectedIndex) {
        return armies.get(selectedIndex).getSpeed();
    }

    /**
     * @param i army to look at
     * @return strength
     */
    public double getStrength(int i) {
        return armies.get(i).getStrength();
    }

    /**
     * Add an army to the simulation
     */
    public void addArmy() {
        armies.add(new Army(rand.nextInt(75) + 1, rand.nextInt(4) + 1, rand.nextDouble() * 9 + 1, rand.nextInt(5) + 6, Color.BLACK, rand.nextDouble(), rand.nextDouble() * 1.5 + .5, false));
        setArmyColour(armies.size() - 1);
        redrawPaused();
    }

    /**
     * reset the battle
     */
    public void reset() {
        armies = new ArrayList<>();
        armies.add(new Army(BLUE_ARMY_SIZE, Army.LEFT_SIDE,
                3.0, 10, Color.BLUE, 0.98, 0.8, false));
        armies.add(new Army(RED_ARMY_SIZE, Army.RIGHT_SIDE,
                4.0, 6, Color.RED, 0.7, 0.8, true));

        // set the armies as adversaries
        armies.get(0).setAdversary(armies.get(1));
        armies.get(1).setAdversary(armies.get(0));

        redrawPaused();
    }

    /**
     * unpause the simulation
     */
    public void unpause() {
        pause = false;
    }

    /**
     * @param num index of army to delete
     */
    public void deleteArmy(int num) {
        armies.remove(num);
        redrawPaused();
    }

    /**
     * @param selectedIndex  army index 1
     * @param selectedIndex1 army index 2
     */
    public void setAdversary(int selectedIndex, int selectedIndex1) {
        if (selectedIndex == selectedIndex1) {
            return;
        }
        armies.get(selectedIndex).setAdversary(armies.get(selectedIndex1));
        armies.get(selectedIndex1).setAdversary(armies.get(selectedIndex));
    }
}  // end Battle class
