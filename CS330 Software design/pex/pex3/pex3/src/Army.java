import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Army class aggregates warriors and performs actions on an army as a whold
 */
class Army {

    // available constants

    static final int TOP_SIDE = 1;
    static final int LEFT_SIDE = 2;
    static final int RIGHT_SIDE = 3;
    static final int BOTTOM_SIDE = 4;

    // class attributes

    private static Graphics2D g = null;
    private static int screenWidth = 0;
    private static int screenHeight = 0;

    // instance attributes

    private ArrayList<Warrior> warriors = new ArrayList<>();
    private ArrayList<Warrior> adversary = new ArrayList<>();
    private double speed = 0.0;
    private double strength = 0.0;
    private int size = 6;
    private Color color = Color.WHITE;
    private int leftX = 0;
    private int rightX = 0;
    private int topY = 0;
    private int bottomY = 0;

    // constructor

    /**
     * Creates an army with the specified characteristics
     *
     * @param number
     * @param placement
     * @param speed
     * @param size
     * @param color
     * @param courage
     * @param strength
     * @param evadingArmy
     */
    Army(int number, int placement, double speed, int size, Color color, double courage, double strength,
         boolean evadingArmy) {

        if ((Army.getScreenWidth() == 0) || (Army.getScreenHeight() == 0)) {
            throw new ExceptionInInitializerError(
                    "Must set screen dimension in Army class prior to creating army objects.");
        }

        // set army's warrior'  speed, size, and color

        this.speed = speed;
        this.size = size;
        this.color = color;
        this.strength = strength;


        // set placement parameters

        setPlacementParameters(placement);

        // create each of the warriors

        Random rand = new Random(System.currentTimeMillis());

        for (int i = 0; i < number; i++) {

            // determine position bounds

            int x = this.leftX + rand.nextInt(this.rightX - this.leftX);
            int y = this.topY + rand.nextInt(this.bottomY - this.topY);

            // add the new warrior

            if (evadingArmy) {
                warriors.add(new EvadingWarrior(x, y, this.speed, this.size, this.color, courage, strength));
            } else {
                warriors.add(new Warrior(x, y, this.speed, this.size, this.color, courage, strength));
            }

            warriors.get(i).setMyFriendlies(this);

        }
    }

    // Class setters and getters

    /**
     * Set the graphics object for the entire Army class so they know where to draw
     *
     * @param g
     */
    static void setGraphics(Graphics2D g) {
        Army.g = g;
    }

    /**
     * Set the screen width for the graphics object
     *
     * @param width
     */
    static void setScreenWidth(int width) {
        Army.screenWidth = width;
    }

    /**
     * Set the screen height for the graphics object
     *
     * @param height
     */
    static void setScreenHeight(int height) {
        Army.screenHeight = height;
    }

    /**
     * Get the graphics object reference
     *
     * @return graphics object
     */
    static Graphics2D getGraphics() {
        return Army.g;
    }

    /**
     * Get the screen width of the graphics object
     *
     * @return screen width
     */
    static int getScreenWidth() {
        return Army.screenWidth;
    }

    /**
     * Get the screen height of the graphics object
     *
     * @return screen height
     */
    static int getScreenHeight() {
        return Army.screenHeight;
    }

    // Instance setters and getters

    /**
     * Set the adversary for the army and all of its warriors
     *
     * @param adversary
     */
    void setAdversary(Army adversary) {
        this.adversary.addAll(adversary.warriors);  // set this army's adversary

        // set adversary for each warrior in this army

        for (Warrior w : this.warriors) {
            w.setadversary(this.adversary);
        }
    }

    /**
     * Get the array of Warriors
     *
     * @return warriors array
     */
    ArrayList<Warrior> getWarriors() {
        return this.warriors;
    }

    /**
     * Count the number of warriors that are alive
     *
     * @return number of alive warriors
     */
    int count() {

        int num = 0;

        for (Warrior w : this.warriors) {
            if (w.isAlive()) num++;
        }
        return num;
    }

    /**
     * Move all the warriors in this army by setting their next location
     */
    void move() {

        for (Warrior w : this.warriors) {
            w.move();
        }
    }

    /**
     * Update all warriors in this army to their next location
     */
    void updateToNextPosition() {

        for (Warrior w : this.warriors) {
            w.updateToNextPostion();
        }
        for (int i = 0; i < warriors.size(); i++) {
            if (!warriors.get(i).isAlive()) {
                warriors.remove(i);
            }
        }
    }

    /**
     * Have each warrior in this army fight an adversaries near them
     */
    void fight() {

        for (Warrior w : this.warriors) {
            w.fight();
        }
    }

    /**
     * Have each warrior in this army draw itself
     */
    void draw() {

        for (Warrior w : this.warriors) {
            w.draw();
        }
    }

    /**
     * Set the parameters for placement of the army's warriors
     *
     * @param placement
     */
    private void setPlacementParameters(int placement) {

        switch (placement) {
            case Army.LEFT_SIDE:
                this.leftX = (int) (0.1 * Army.getScreenWidth());
                this.rightX = (int) (0.2 * Army.getScreenWidth());
                this.topY = (int) (0.3 * Army.getScreenHeight());
                this.bottomY = (int) (0.7 * Army.getScreenHeight());
                break;
            case Army.RIGHT_SIDE:
                this.leftX = (int) (0.8 * Army.getScreenWidth());
                this.rightX = (int) (0.9 * Army.getScreenWidth());
                this.topY = (int) (0.3 * Army.getScreenHeight());
                this.bottomY = (int) (0.7 * Army.getScreenHeight());
                break;
            case Army.TOP_SIDE:
                this.leftX = (int) (0.3 * Army.getScreenWidth());
                this.rightX = (int) (0.7 * Army.getScreenWidth());
                this.topY = (int) (0.1 * Army.getScreenHeight());
                this.bottomY = (int) (0.2 * Army.getScreenHeight());
                break;
            case Army.BOTTOM_SIDE:
                this.leftX = (int) (0.3 * Army.getScreenWidth());
                this.rightX = (int) (0.7 * Army.getScreenWidth());
                this.topY = (int) (0.8 * Army.getScreenHeight());
                this.bottomY = (int) (0.9 * Army.getScreenHeight());
                break;
        }
    }

    /**
     * @param randomColor Color to set army to
     */
    public void setNewColor(Color randomColor) {
        this.color = randomColor;
        for (Warrior w : this.warriors) {
            w.setColor(randomColor);
        }
    }

    /**
     * @return color of army for display in gui
     */
    public Color getColor() {
        return this.color;
    }

    /**
     * @param value value to set army speed to
     */
    public void setSpeed(double value) {
        this.speed = value;
        for (Warrior w : this.warriors) {
            w.setSpeed(value);
        }
    }

    /**
     * @param v value to set strength to
     */
    public void setStrength(double v) {
        this.strength = v;
        for (Warrior w : this.warriors) {
            w.setStrength(v);
        }
    }

    /**
     * @return number of warriors
     */
    public int getNumWarriors() {
        int count = 0;
        for (Warrior w : this.warriors) {
            if (w.isAlive()) {
                count++;
            }
        }
        return count;
    }

    /**
     * @return speed of army
     */
    public double getSpeed() {
        return this.speed;
    }

    /**
     * @return strength of army
     */
    public double getStrength() {
        return this.strength;
    }
}  // end of Army class
