import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JList;

public class gui extends Container {
    private static int selector = 0;
    private boolean running = false;
    private static int val = 0;
    public JPanel panel1;
    private JSlider StrengthSlide;
    private JButton pauseButton;
    private JComboBox<String> ArmySelector;
    JProgressBar GameProgress;
    private JButton startGameButton;
    private JButton addArmyButton;
    private JSlider SpeedSlide;
    private JButton deleteArmyButton;
    private JLabel SpeedLabel;
    private JLabel StrengthLabel;
    private JButton ColorButton;
    private JTextField colorTextField;
    private JComboBox ArmySelector2;
    private JButton setAdversary;
    private Battle battle;
    private ArrayList<String> armynames = new ArrayList<>();
    int count;

    /**
     * Gui for the battle
     *
     * @param battle
     */
    public gui(Battle battle) {
        this.battle = battle;
        armynames.add("Army 1");
        armynames.add("Army 2");
        count = 3;

        pauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (running == true) {
                    if (battle.isPaused()) {
                        battle.resume();
                        pauseButton.setText("Pause");
                    } else {
                        battle.pause();
                        pauseButton.setText("Resume");
                    }
                    battle.progressUpdate();
                    GameProgress.setValue(val);
                }
            }
        });
        colorTextField.setText(battle.getColor(ArmySelector.getSelectedIndex()));
        ColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                battle.setArmyColour(ArmySelector.getSelectedIndex());
                colorTextField.setText(battle.getColor(ArmySelector.getSelectedIndex()));
                if (battle.isPaused()) {
                    battle.redrawPaused();
                }
            }
        });
        SpeedLabel.setText("Speed: " + battle.getSpeed(ArmySelector.getSelectedIndex()));
        SpeedSlide.setValue((int) (battle.getSpeed(ArmySelector.getSelectedIndex()) * 10));
        SpeedSlide.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                battle.setArmySpeed(ArmySelector.getSelectedIndex(), (double) SpeedSlide.getValue() / 10);
                SpeedLabel.setText("Speed: " + (double) SpeedSlide.getValue() / 10);
            }
        });
        StrengthLabel.setText("Strength: " + battle.getStrength(ArmySelector.getSelectedIndex()));
        StrengthSlide.setValue((int) (battle.getStrength(ArmySelector.getSelectedIndex()) * 10));
        StrengthSlide.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                battle.setArmyStrength(ArmySelector.getSelectedIndex(), (double) StrengthSlide.getValue() / 10);
                StrengthLabel.setText("Strength: " + (double) StrengthSlide.getValue() / 10);
            }
        });

        ArmySelector.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                updateStats();
            }
        });
        addArmyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                battle.addArmy();
                armynames.add("Army " + (count));
                updateDropDown();
                count++;
            }
        });
        deleteArmyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                battle.deleteArmy(ArmySelector.getSelectedIndex());
                armynames.remove(ArmySelector.getSelectedIndex());
                ArmySelector.removeItemAt(ArmySelector.getSelectedIndex());
                updateDropDown();
            }
        });
        pauseButton.setEnabled(false);
        startGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (startGameButton.getText().equals("Start Game")) {
                    running = true;
                    pauseButton.setEnabled(true);
                    startGameButton.setText("Reset Game");
                    battle.unpause();
                } else {
                    battle.pause();
                    battle.progressUpdate();
                    GameProgress.setValue(val);
                    battle.reset();
                    running = false;
                    pauseButton.setText("Pause");
                    pauseButton.setEnabled(false);
                    armynames = new ArrayList<>();
                    armynames.add("Army 1");
                    armynames.add("Army 2");
                    updateDropDown();
                    startGameButton.setText("Start Game");
                    updateStats();
                }
            }
        });
        setAdversary.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                battle.setAdversary(ArmySelector.getSelectedIndex(), ArmySelector2.getSelectedIndex());
            }
        });
    }

    /**
     * Updates both dropdown windows to show available armies
     */
    private void updateDropDown() {
        for (int i = 0; i < ArmySelector.getItemCount(); i++) {
            ArmySelector.removeItemAt(i);
        }
        for (String s : armynames) {
            ArmySelector.addItem(s);
        }
        ArmySelector.removeItemAt(0);

        for (int i = 0; i < ArmySelector2.getItemCount(); i++) {
            ArmySelector2.removeItemAt(i);
        }
        for (String s : armynames) {
            ArmySelector2.addItem(s);
        }
        ArmySelector2.removeItemAt(0);
    }

    /**
     * Updates stats when switching views of the armies
     */
    private void updateStats() {
        SpeedLabel.setText("Speed: " + battle.getSpeed(ArmySelector.getSelectedIndex()));
        SpeedSlide.setValue((int) (battle.getSpeed(ArmySelector.getSelectedIndex()) * 10));
        StrengthLabel.setText("Strength: " + battle.getStrength(ArmySelector.getSelectedIndex()));
        StrengthSlide.setValue((int) (battle.getStrength(ArmySelector.getSelectedIndex()) * 10));
        colorTextField.setText(battle.getColor(ArmySelector.getSelectedIndex()));
    }

    /**
     * @return selectedArmy
     */
    static int getSelected() {
        return selector;
    }

    /**
     * @param i sets progress bar to number of 100
     */
    static void setProgress(int i) {
        val = i;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel1 = new JPanel();
        panel1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(8, 2, new Insets(0, 0, 0, 0), -1, -1));
        StrengthLabel = new JLabel();
        StrengthLabel.setText("Strength");
        panel1.add(StrengthLabel, new com.intellij.uiDesigner.core.GridConstraints(5, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        StrengthSlide = new JSlider();
        StrengthSlide.setMaximum(20);
        panel1.add(StrengthSlide, new com.intellij.uiDesigner.core.GridConstraints(6, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        ArmySelector = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        defaultComboBoxModel1.addElement("Army 1");
        defaultComboBoxModel1.addElement("Army 2");
        ArmySelector.setModel(defaultComboBoxModel1);
        panel1.add(ArmySelector, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(93, 56), null, 0, false));
        GameProgress = new JProgressBar();
        panel1.add(GameProgress, new com.intellij.uiDesigner.core.GridConstraints(7, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        startGameButton = new JButton();
        startGameButton.setText("Start Game");
        panel1.add(startGameButton, new com.intellij.uiDesigner.core.GridConstraints(7, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        addArmyButton = new JButton();
        addArmyButton.setText("Add Army");
        panel1.add(addArmyButton, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        deleteArmyButton = new JButton();
        deleteArmyButton.setText("Delete Army");
        panel1.add(deleteArmyButton, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(100, 34), null, 0, false));
        pauseButton = new JButton();
        pauseButton.setText("Pause");
        panel1.add(pauseButton, new com.intellij.uiDesigner.core.GridConstraints(6, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        SpeedLabel = new JLabel();
        SpeedLabel.setText("Speed");
        panel1.add(SpeedLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        SpeedSlide = new JSlider();
        SpeedSlide.setMaximum(100);
        panel1.add(SpeedSlide, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(300, 16), null, 0, false));
        ColorButton = new JButton();
        ColorButton.setText("New Color");
        panel1.add(ColorButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        colorTextField = new JTextField();
        colorTextField.setText("Color");
        panel1.add(colorTextField, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        ArmySelector2 = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
        defaultComboBoxModel2.addElement("Army 1");
        defaultComboBoxModel2.addElement("Army 2");
        ArmySelector2.setModel(defaultComboBoxModel2);
        panel1.add(ArmySelector2, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(93, 56), null, 0, false));
        setAdversary = new JButton();
        setAdversary.setText("Set Adversary");
        panel1.add(setAdversary, new com.intellij.uiDesigner.core.GridConstraints(4, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }

}