import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Warrior class creates individual warriors that are cognizant of its environment to include the
 * battlespace, its teammates, and its adversaries.
 */
class Warrior {

    // random number generator for the Warrior class

    private static Random rand = new Random(System.currentTimeMillis());

    // class attributes

    private static Graphics2D g = null;
    private static int screenWidth = 0;
    private static int screenHeight = 0;

    // instance attributes

    private Vector330Class pos;
    private Vector330Class nextPos;
    private double speed = 0.0;
    private int size = 6;
    private Color color = Color.WHITE;
    private double courage = 0.0;
    private double strength = 0.0;
    private boolean alive = true;
    private ArrayList<Warrior> adversary = new ArrayList<>();
    private Army myFriendlies = null;

    // constructor

    /**
     * Construct a Warrior with the specified characteristics
     *
     * @param x
     * @param y
     * @param speed
     * @param size
     * @param color
     * @param courage
     * @param strength
     */
    Warrior(double x, double y, double speed, int size, Color color, double courage, double strength) {
        this.pos = new Vector330Class(x, y);
        this.nextPos = new Vector330Class();
        this.speed = speed;
        this.size = size;
        this.color = color;
        this.courage = courage;
        this.strength = strength;
        this.alive = true;
    }

    // Class setters and getters

    /**
     * Set the graphics object so warriors know where to draw themselves
     *
     * @param g
     */
    static void setGraphics(Graphics2D g) {
        Warrior.g = g;
    }

    /**
     * Set the screen width for the graphics object
     *
     * @param width
     */
    static void setScreenWidth(int width) {
        Warrior.screenWidth = width;
    }

    /**
     * Set the screen height for the graphics object
     *
     * @param height
     */
    static void setScreenHeight(int height) {
        Warrior.screenHeight = height;
    }

    /**
     * Get the graphics object for warriors
     *
     * @return graphics object
     */
    static Graphics2D getGraphics() {
        return Warrior.g;
    }

    /**
     * Get the screen width of the graphics object
     *
     * @return screen width of the graphics object
     */
    static int getScreenWidth() {
        return Warrior.screenWidth;
    }

    /**
     * Get the screen height of the graphics object
     *
     * @return screen height of the graphics object
     */
    static int getScreenHeight() {
        return Warrior.screenHeight;
    }

    /**
     * Get the random number generator for warriors
     *
     * @return random number generator
     */
    static Random getRand() {
        return rand;
    }

    // instance setters and getters

    /**
     * Set the adversary army for this warrior
     *
     * @param enemy
     */
    void setAdversary(ArrayList<Warrior> enemy) {
        this.adversary = enemy;
    }

    /**
     * Set the friendly army for this warrior
     *
     * @param friendlies
     */
    void setMyFriendlies(Army friendlies) {
        this.myFriendlies = friendlies;
    }

    /**
     * Establish the next position that this warrior will move to
     *
     * @param nextPos
     */
    void setNextPos(Vector330Class nextPos) {
        this.nextPos = nextPos;
    }

    /**
     * Get the current position of this warrior
     *
     * @return current position vector
     */
    Vector330Class getPosition() {
        return this.pos;
    }

    /**
     * Get the courage of this warrior
     *
     * @return this warrior's courage
     */
    double getCourage() {
        return courage;
    }

    /**
     * Get the speed for this warrior
     *
     * @return this warrior's speed
     */
    double getSpeed() {
        return speed;
    }

    /**
     * Get the size of this warrior
     *
     * @return this warrior's size
     */
    int getSize() {
        return this.size;
    }

    /**
     * Report if this warrior is alive
     *
     * @return alive status of this warrior
     */
    boolean isAlive() {
        return this.alive;
    }

    /**
     * Get the array of the adversaries warriors
     *
     * @return array of adversary Warriors
     */
    ArrayList<Warrior> getAdversaryWarriors() {
        return adversary;
    }

    /**
     * Get the array of the friendly warriors
     *
     * @return array of friendly Warriors
     */
    ArrayList<Warrior> getFriendlyWarriors() {
        return this.myFriendlies.getWarriors();
    }

    /**
     * Update the current position of this warrior to their next position, if it is alive.
     * If the new position is off of the screen, the warrior dies.
     */
    void updateToNextPostion() {
        if (this.alive) {

            this.pos.setX(this.nextPos.getX());
            this.pos.setY(this.nextPos.getY());

            if ((this.pos.getX() > screenWidth) || (this.pos.getX() < 0)) {
                this.alive = false;
            }

            if ((this.pos.getY() > screenHeight) || (this.pos.getY() < 0)) {
                this.alive = false;
            }

        }
    }

    /**
     * Draw this warrior in its current location, if it is alive
     */
    void draw() {
        if (this.alive) {
            g.setColor(this.color);
            g.fillOval(this.pos.getXint() - (this.size / 2), this.pos.getYint() - (this.size / 2), this.size, this.size);
        }
    }

    /**
     * Move this warrior, if alive, by determining its next position based upon moving toward the closest adversary
     */
    void move() {

        if (this.alive) {  // only move warriors that are alive

            Warrior nearestEnemy = this.findNearestAdversary();  // find the nearest enemy

            Vector330Class toNearestAdversary = new Vector330Class();  // vector to the nearest enemy

            if (nearestEnemy != null) {

                // determine displacement vector to the nearest enemy

                toNearestAdversary = nearestEnemy.getPosition().subtract(this.getPosition());

                // calculate how far away this enemy is

                double distanceToNearAdversary = toNearestAdversary.magnitude();

                // take care not to over jump the enemy

                if (distanceToNearAdversary > this.speed) {
                    toNearestAdversary = toNearestAdversary.normalize().scale(this.speed);
                }

                // if this warrior's courage fails, it runs away for this step

                if (rand.nextDouble() > this.courage) {
                    toNearestAdversary = toNearestAdversary.scale(-1.0);
                }
            }

            // add the velocity vector to the position vector to get the next postion

            this.nextPos = this.pos.add(toNearestAdversary);

        }
    }

    /**
     * This warrior, if alive, will fight its nearest enemy
     */
    void fight() {

        if (this.alive) {

            Warrior nearestEnemy = this.findNearestAdversary();  // find the nearest enemy

            Vector330Class toNearestAdversary = new Vector330Class();

            if (nearestEnemy != null) {

                // determine displacement vector to nearest enemy

                toNearestAdversary = nearestEnemy.getPosition().subtract(this.getPosition());
                double distanceToNearAdversary = toNearestAdversary.magnitude();

                // check if close enough to fight

                if (distanceToNearAdversary < ((nearestEnemy.size + this.size) / 2)) {
                    if (rand.nextDouble() > (this.strength / (this.strength + nearestEnemy.strength))) {
                        this.alive = false;
                    } else {
                        nearestEnemy.alive = false;
                    }
                }

            }
        }

    }  // end of fight method


    /**
     * Find the nearest enemy warrior
     *
     * @return nearest enemy warrior
     */
    Warrior findNearestAdversary() {

        Vector330Class toNearestAdversary = new Vector330Class();
        double distanceToNearAdversary = Double.MAX_VALUE;
        Warrior nearestEnemy = null;

        // find the closest enemy warrior
        if (adversary.isEmpty()) {
            return new Warrior(this.pos.getX(), this.pos.getY(), 0, 0, Color.WHITE, 0, 0);
        }

        ArrayList<Warrior> enemies = getAdversaryWarriors();
        // get array of all enemies

        // check against each enemy warrior

        for (Warrior enemy : enemies) {
            // if the current enemy is alive, fight them
            if (enemy == null) continue;
            if (enemy.alive) {

                // calculate the displacement vector to the current enemy

                Vector330Class toEnemy = enemy.getPosition().subtract(this.getPosition());

                // if this enemy is nearest, update nearest enemy

                if (toEnemy.magnitude() < distanceToNearAdversary) {
                    toNearestAdversary = toEnemy;
                    distanceToNearAdversary = toNearestAdversary.magnitude();
                    nearestEnemy = enemy;
                } // end if
            }
        } // end for loop

        return nearestEnemy;

    }  // end findNearest Adversary

    /**
     * Sets Color of warrior
     *
     * @param randomColor random color to set warrior to
     */
    public void setColor(Color randomColor) {
        this.color = randomColor;
    }

    /**
     * @param value value to set speed to
     */
    public void setSpeed(double value) {
        this.speed = value;
    }

    /**
     * @param v value to set strength to
     */
    public void setStrength(double v) {
        this.strength = v;
    }

    /**
     * @param adversary ArrayList to set adversary to
     */
    public void setadversary(ArrayList<Warrior> adversary) {
        this.adversary = adversary;
    }
}
