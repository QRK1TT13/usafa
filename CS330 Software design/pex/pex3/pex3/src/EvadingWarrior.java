import java.awt.*;
import java.util.ArrayList;

/**
 * Warrior with a new evading behavior if the number of enemies exceeds the number of friendlies
 */
class EvadingWarrior extends Warrior {

    static private final int lookRange = 3;  // how far to look when counting enemies and friends

    /**
     * Constructs an evading warrior
     *
     * @param x
     * @param y
     * @param speed
     * @param size
     * @param color
     * @param courage
     * @param strength
     */
    EvadingWarrior(double x, double y, double speed, int size, Color color, double courage, double strength) {
        super(x, y, speed, size, color, courage, strength);
    }

    /**
     * New move method that moves towards the nearest enemy unless the number of near enemies exceeds the number
     * of nearby friendly; in which case it runs away from the nearest enemy
     */
    void move() {

        if (this.isAlive()) {  // only applies to warriors that are alive

            Warrior nearestEnemy = this.findNearestAdversary();  // find the nearest enemy

            Vector330Class toNearestAdversary = new Vector330Class();  // velocity vector to nearest enemy

            if (nearestEnemy != null) {

                // calculate the displacement to the nearest enemy

                toNearestAdversary = nearestEnemy.getPosition().subtract(this.getPosition());
                double distanceToNearAdversary = toNearestAdversary.magnitude();

                // take care not to over jump the enemy

                if (distanceToNearAdversary > this.getSpeed()) {
                    toNearestAdversary = toNearestAdversary.normalize().scale(this.getSpeed());
                }

                // check if outnumbered

                boolean outNumbered =
                        (this.countNeighbors(this.getAdversaryWarriors(), lookRange * this.getSize() ) >=
                                this.countNeighbors(this.getFriendlyWarriors(), lookRange * this.getSize()) );

                // if outnumbered or if courage fails, reverse direction

                if ( (outNumbered) || (Warrior.getRand().nextDouble() > this.getCourage())) {
                    toNearestAdversary = toNearestAdversary.scale(-1.0);
                }
            }

            // add the velocity vector to the position to get the next position

            this.setNextPos( this.getPosition().add(toNearestAdversary) );

        }
    }  // end move()

    /**
     * Count how many other warriors (from an army's array of warriors) are within a specified distance
     *
     * @param others
     * @param distance
     * @return number of near warriors
     */
    private int countNeighbors(ArrayList<Warrior> others, int distance ) {

        int count = 0;

        // for each other warrior, compare magnitude of the displacement vector to the specified distance

        for (Warrior w : others) {
            if(w==null)continue;
            if (this.getPosition().subtract(w.getPosition()).magnitude() < distance ) count++;
        }

        return count;

    }

}
