import javax.swing.*;

/**
 * Main - CS 330 - Mini-Massive - Phase 1
 * <p>
 * large-scale autonomous life simulation along the lines of the Massive software first developed for The Lord of
 * the Rings software and subsequently used in many applications. (See http://www.massivesoftware.com/index.html).
 *
 * @author Steve Hadfield
 * @date 7 Sep 2019
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        JFrame frame = new JFrame("Battle Arena");
        Battle battle = new Battle();
        battle.waitForInit();
        frame.setContentPane(new gui(battle).panel1);
        frame.setDefaultCloseOperation((JFrame.EXIT_ON_CLOSE));
        frame.pack();
        frame.setVisible(true);
        battle.doBattle();
    }
}
