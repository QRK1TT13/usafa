/*
@author Hunter Manter
Documentation: None
Extra things I added:
    Third army
    And alliance parameter so that two of the armies dont fight until the third is dead
    Viking warriors can randomly become berserkers which will increase their strength, changes thier size, and changes their color
 */


import javax.swing.*;
import java.awt.*;

public class main {
    static final int XSIZE = 500;
    static final int YSIZE = 500;
    static DrawingPanel panel = new DrawingPanel(XSIZE, YSIZE);
    static Graphics2D g = panel.getGraphics();

    public static void main(String[] args) {
        panel.setBackground(Color.WHITE);
        panel.copyGraphicsToScreen();
        Battle battle = new Battle();
        battle.draw();
        panel.copyGraphicsToScreen();
        OptionFrame frame = new OptionFrame();

        while (true) { //wait for user to say to go
            if (frame.getClick() == JOptionPane.YES_OPTION) {
                break;
            } else {
                System.out.println("Program exit");
                System.exit(40);
            }
        }
        while (!panel.mouseClickHasOccurred(DrawingPanel.LEFT_BUTTON) && battle.inSession) {
            battle.move();
            battle.offEdge();
            battle.fight();
            battle.checkTruce();
            battle.draw();
            panel.copyGraphicsToScreen();
            panel.setBackground(Color.WHITE);
            panel.setWindowTitle("Left click to exit.   " + battle.status());
            panel.sleep(50);
        }
        panel.sleep(2000); //pause so you can see end result
        panel.closeWindow();
    }
}

