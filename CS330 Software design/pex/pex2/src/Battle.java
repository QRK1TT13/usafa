import java.awt.*;
import java.lang.reflect.Array;
import java.security.Signature;

/**
 * Overtsees all the armies
 */
public class Battle {
    Army viking;
    Army french;
    Army english;
    static final int SIZE = 50;
    private Army army;
    static boolean inSession = true;

    /**
     * Contructor for battle and sets up armies
     */
    public Battle() {
        this.viking = new Army(SIZE, Color.BLACK, false);
        this.french = new Army(SIZE, Color.BLUE, true);
        this.english = new Army(SIZE, Color.RED, true);
        viking.setOpponents(french, english);
        french.setOpponents(viking, english);
        english.setOpponents(viking, french);
    }

    /**
     * print out all army details
     *
     * @return
     */
    String printAll() {
        return viking.warriors.toString() + "\n" + french.warriors.toString() + "\n" + english.warriors.toString();
    }

    /**
     * move all armies
     */
    void move() {
        viking.move();
        french.move();
        english.move();
    }

    /**
     * draw all armies
     */
    public void draw() {
        viking.draw();
        french.draw();
        english.draw();
    }

    /**
     * @return sizes of armies for window
     */
    String status() {
        return "Vikings: " + viking.size + ", French: " + french.size + ", English: " + english.size;
    }

    /**
     * all armies fight
     */
    void fight() {
        viking.fight();
        french.fight();
        english.fight();
    }

    /**
     * all armies check if warriors are off edge
     */
    public void offEdge() {
        viking.removeOffEdge();
        french.removeOffEdge();
        english.removeOffEdge();
    }

    /**
     * update truce if vikings are dead
     */
    public void checkTruce() {
        if (viking.size == 0) {
            french.alliance = false;
        }
    }
}

