import javax.swing.*;

public class OptionFrame {
    public OptionFrame() {
        JDialog.setDefaultLookAndFeelDecorated(true);
    }

    /**
     * @return what the user clicks
     */
    int getClick() {
        return JOptionPane.showConfirmDialog(null, "NPC's are waiting to die.  ", "Confirm",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    }
}
