import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Army {

    ArrayList<Warrior> warriors = new ArrayList<>();
    Color color;
    boolean alliance;
    int strength;
    Army opp1;
    Army opp2;
    int size;

    /**
     * @param size     number of warriors
     * @param color    color of the army
     * @param alliance whether army is in an alliance
     */
    public Army(int size, Color color, boolean alliance) {
        this.color = color;
        this.size = size;
        this.alliance = alliance;
        createAllWarriors();
        setAllValues();
    }

    /**
     * creates all the warriors in the army
     */
    private void createAllWarriors() {
        for (int i = 0; i < size; i++) {
            warriors.add(new Warrior());
        }
    }

    /**
     * sets all the values for all the warriors in the army
     */
    private void setAllValues() {
        for (int i = 0; i < size; i++) {
            warriors.get(i).setColor(this.color);
            warriors.get(i).setPostition(1, 1);
            warriors.get(i).setSize(5);
            warriors.get(i).setArmy(this);
            warriors.get(i).setAlliance(alliance);
            if (color == Color.BLACK) {
                warriors.get(i).setStrengthInit(25);
                warriors.get(i).setSpeed(4);
            } else {
                warriors.get(i).setStrengthInit(9);
                warriors.get(i).setSpeed(5);
            }
        }
    }

    /**
     * tell each warrior to move
     */
    void move() {
        for (int i = 0; i < size; i++) {
            warriors.get(i).move();
        }
    }

    /**
     * tell each warrior to draw itself
     */
    public void draw() {
        for (int i = 0; i < size; i++) {
            warriors.get(i).draw();
        }
    }

    /**
     * @return if army is in alliance
     */
    public boolean getAlliance() {
        return this.alliance;
    }

    /**
     * remove warriors that are off the edge of the board
     */
    public void removeOffEdge() {
        for (int i = 0; i < size; i++) {
            if (warriors.get(i).getX() < 0 || warriors.get(i).getX() > main.XSIZE || warriors.get(i).getY() < 0 || warriors.get(i).getY() > main.YSIZE) {
                warriors.remove(i);
            }
        }
    }

    /**
     * @param opp1 first opponent army
     * @param opp2 second opponent army
     */
    public void setOpponents(Army opp1, Army opp2) {
        this.opp1 = opp1;
        this.opp2 = opp2;
        for (int i = 0; i < size; i++) {
            warriors.get(i).setOpp(opp1, opp2);
        }
    }

    /**
     * Each fighter fights each fighter in the other armies if they are close enough
     */
    void fight() {
        if ((this.opp1.alliance != this.alliance)) {
            Vector330 temp;
            int strength1;
            int strength2;
            Random rand = new Random();
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < opp1.size; j++) {
                    temp = new Vector330(Math.abs(warriors.get(i).getX() - opp1.warriors.get(j).getX()), Math.abs(warriors.get(i).getY() - opp1.warriors.get(j).getY()));
                    if (temp.magnitude() <= 4) {
                        strength1 = warriors.get(i).getStrength() + rand.nextInt(4);
                        strength2 = opp1.warriors.get(j).getStrength() + rand.nextInt(4);
                        warriors.get(i).setStrength(strength1 - strength2);
                        opp1.warriors.get(j).setStrength(strength2 - strength1);
                    }
                    if (opp1.warriors.get(j).getStrength() <= 0) {
                        opp1.warriors.remove(j);
                        opp1.size = opp1.size - 1;
                    }
                }
                if (warriors.get(i).getStrength() <= 0) {
                    warriors.remove(i);
                    size = size - 1;
                }
            }
        }
        if ((this.opp2.alliance != this.alliance)) {
            Vector330 temp;
            int strength1;
            int strength2;
            Random rand = new Random();
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < opp2.size; j++) {
                    temp = new Vector330(Math.abs(warriors.get(i).getX() - opp2.warriors.get(j).getX()), Math.abs(warriors.get(i).getY() - opp2.warriors.get(j).getY()));
                    if (temp.magnitude() <= 4) {
                        strength1 = warriors.get(i).getStrength() + rand.nextInt(4);
                        strength2 = opp2.warriors.get(j).getStrength() + rand.nextInt(4);
                        warriors.get(i).setStrength(strength1 - strength2);
                        opp2.warriors.get(j).setStrength(strength2 - strength1);
                    }
                    if (opp2.warriors.get(j).getStrength() <= 0) {
                        opp2.warriors.remove(j);
                        opp2.size = opp2.size - 1;
                    }
                }
                if (warriors.get(i).getStrength() <= 0) {
                    warriors.remove(i);
                    size = size - 1;
                }
            }
        }
        if ((size == 0 && opp1.size == 0) || (opp1.size == 0 && opp2.size == 0) || (opp2.size == 0 && size == 0)) {
            Battle.inSession = false;
        }
    }
}

