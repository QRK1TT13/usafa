import java.awt.*;
import java.util.Random;

public class Warrior {
    private Vector330 pos = new Vector330();
    private int strength = 0;
    private boolean berserk;
    private Color color;
    private boolean alliance;
    private int charSize;
    private int speed;
    private Army opp1;
    private Army opp2;
    private Army army;
    static Random rand = new Random();

    /**
     * warrior constructor
     */
    public Warrior() {
    }

    /**
     * @param strength the strength of the warrior
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * @param strength set the strength initially for warrior
     */
    public void setStrengthInit(int strength) {
        this.strength += strength;
    }

    /**
     * @param color Color to set warrior to
     */
    public void setColor(Color color) {
        if (color == Color.BLACK) {
            berserk = rand.nextInt(Battle.SIZE / 2) == 1;
            if (berserk) {
                strength = 75;
                this.color = Color.GRAY;
            } else {
                this.color = color;
            }
        } else {
            this.color = color;
        }
    }

    /**
     * @param size size of thing to draw
     */
    public void setSize(int size) {
        if (berserk) {
            this.charSize = size * 2;
        } else {
            this.charSize = size;
        }
    }

    /**
     * @param x x position of warrior
     * @param y y position of warrior
     */
    void setPostition(int x, int y) {
        if (this.color.equals(Color.BLACK) || this.color.equals(Color.GRAY)) {
            this.pos.setX(rand.nextInt(main.XSIZE / 6) + (main.XSIZE / 6));
            this.pos.setY(rand.nextInt(main.YSIZE / 3) + (main.YSIZE / 3));
        } else if (this.color.equals(Color.BLUE)) {
            this.pos.setX(rand.nextInt(main.XSIZE / 6) + 4 * (main.XSIZE / 6));
            this.pos.setY(rand.nextInt(main.YSIZE / 4) + 2 * (main.YSIZE / 4));
        } else if (this.color.equals(Color.RED)) {
            this.pos.setX(rand.nextInt(main.XSIZE / 6) + 4 * (main.XSIZE / 6));
            this.pos.setY(rand.nextInt(main.YSIZE / 4) + (main.YSIZE / 4));
        } else {
            System.exit(420);
        }
    }

    /**
     * @param speed speed to set warrior to
     */
    void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @return string of warrior parameters
     */
    public String toString() {
        return "[" + pos.getXint() + "," + pos.getYint() + "," + strength + "," + berserk + "," + color + "," + charSize + "]";
    }

    /**
     * move warrior based on nearest enemmy
     */
    void move() {
        Vector330 min = new Vector330(main.XSIZE, main.YSIZE);
        Vector330 temp;
        if ((this.opp1.alliance != this.army.alliance)) {
            for (int i = 0; i < opp1.size; i++) {
                temp = new Vector330(opp1.warriors.get(i).getX() - this.pos.getXint(), (opp1.warriors.get(i).getY() - this.pos.getYint()));
                if (temp.magnitude() < min.magnitude()) {
                    min = temp;
                }
            }
        }
        if ((this.opp2.alliance != this.army.alliance)) {
            for (int i = 0; i < opp2.size; i++) {
                temp = new Vector330((opp2.warriors.get(i).getX() - this.pos.getXint()), (opp2.warriors.get(i).getY() - this.pos.getYint()));
                if (temp.magnitude() < min.magnitude()) {
                    min = temp;
                }
            }
        }
        if (min.magnitude() < speed || min.getXint() == main.XSIZE) {
            return;
        }
        this.pos.setX(pos.getXint() + min.normalize().scale(speed).getX());
        this.pos.setY(pos.getYint() + min.normalize().scale(speed).getY());
    }

    /**
     * @return x of warrior
     */
    public double getX() {
        return pos.getXint();
    }

    /**
     * @return y of warrior
     */
    public double getY() {
        return pos.getYint();
    }

    /**
     * @return size of object to draw
     */
    public int getSize() {
        return charSize;
    }

    /**
     * draw warrrior
     */
    public void draw() {
        main.g.setColor(color);
        main.g.fillOval((int) this.pos.getXint() - this.charSize, (int) this.pos.getYint() - this.charSize,
                this.charSize * 2, this.charSize * 2);
    }

    /**
     * @param opp1 army enemy 1
     * @param opp2 army enemy 2
     */
    public void setOpp(Army opp1, Army opp2) {
        this.opp1 = opp1;
        this.opp2 = opp2;
    }

    /**
     * @param army army that is friend
     */
    public void setArmy(Army army) {
        this.army = army;
    }

    /**
     * @param alliance set if the warrior is in the alliance
     */
    public void setAlliance(boolean alliance) {
        this.alliance = alliance;
    }

    /**
     * @return strength of warrior
     */
    public int getStrength() {
        return this.strength;
    }
}
