//Documentation: none
import java.util.Scanner;
import java.util.regex.Pattern;

public class VectorCalculator {
    public static void	main​(java.lang.String[] args) throws Exception {
        boolean end  = false;
        System.out.println("Welcome to the vector calculator\nPlease enter your expressions");
        Scanner scan = new Scanner(System.in);
        Pattern startTokens = Pattern.compile("exit|<|\\d|\\||dir|unit");
        String input;
        while(!end)
        {
            System.out.print(">");
            input = scan.nextLine();
            Scanner lineScanner = new Scanner(input);
            if(lineScanner.hasNext(startTokens))
            {
                if(lineScanner.hasNext("exit"))
                {
                    end = true;
                }
                else
                {
                    try
                    {
                        parseExpression​(lineScanner);
                    }
                    catch (Exception e)
                    {
                        System.out.println(e);
                    }

                }
            }
            else
            {
                scan.next();
                System.out.println("That doesn't seem like a valid input, please try again.");
            }
        }
        System.out.println("\nThank you for using the Vector Calculator");
    }

    /**
     * Takes the input and checks for key expressions to know what to do with the expression
     * @param s
     * @throws java.lang.Exception
     */
    private static void parseExpression​(java.util.Scanner s) throws java.lang.Exception
    {
        if(s.hasNext("unit"))
        {
            parseNormalizeExpression​(s);
        }
        else if(s.hasNext("dir"))
        {
            parseDirectionExpression​(s);
        }
        else if(s.hasNext("\\|"))
        {
            parseNormExpression​(s);
        }
        else if(s.hasNext("\\d"))
        {
            parseScaleExpression​(s);
        }
        else if(s.hasNext("<"))
        {
            parseVectorExpression​(s);
        }
    }

    /**
     * Takes in a new vector and then checks which operation to do on it
     * @param s
     * @throws java.lang.Exception
     */
    private static void parseVectorExpression​(java.util.Scanner s) throws java.lang.Exception
    {//=+-.
        Vector330 vec1 = Vector330.parseVector(s);
        Vector330 vec2;
        if(s.hasNext("."))
        {
            s.next(".");
            vec2 = Vector330.parseVector(s);
            System.out.println("Result is "+vec1.dotProduct(vec2));
        }
        else if (s.hasNext("-"))
        {
            s.next("-");
            vec2 = Vector330.parseVector(s);
            System.out.println("Result is "+ vec1.subtract(vec2));
        }
        else if (s.hasNext("\\+"))
        {
            s.next("\\+");
            vec2 = Vector330.parseVector(s);
            System.out.println("Result is "+ vec1.add(vec2));
        }
        else if(s.hasNext("="))
        {
            s.next("=");
            vec2 = Vector330.parseVector(s);
            System.out.println("Result is "+ vec1.equals(vec2));
        }
        else if(s.hasNext("=="))
        {
            s.next("==");
            vec2 = Vector330.parseVector(s);
            System.out.println("Result is "+ vec1.equals(vec2));
        }
        else
        {
            System.out.println("Result is "+vec1.toString());
        }
    }

    /**
     * Takes in a string with a number to scale the following vector with
     * @param s
     * @throws java.lang.Exception
     */
    private static void parseScaleExpression​(java.util.Scanner s) throws java.lang.Exception
    {
        String num = s.next("\\d");
        if(s.hasNext("\\*"))
        {
            s.next("\\*");
            Vector330 vec = Vector330.parseVector(s);
            double n = Double.parseDouble(num);
            vec = vec.scale(n);
            System.out.println("Result is "+ vec.toString());
        }
        else
        {
            throw new Exception("Invalid expression. No * used when trying to scale");
        }
    }

    /**
     * Find the magnitude of the passed vector
     * @param s
     * @throws java.lang.Exception
     */
    private static void parseNormExpression​(java.util.Scanner s) throws java.lang.Exception
    {
        s.next("\\|");
        Vector330 vec = Vector330.parseVector(s);
        System.out.printf("Result is %.1f\n",vec.magnitude());
    }

    /**
     * Takes in a vector and finds the direction of it
     * @param s
     * @throws java.lang.Exception
     */
    private static void parseDirectionExpression​(java.util.Scanner s) throws java.lang.Exception
    {
        s.next("dir");
        Vector330 vec = Vector330.parseVector(s);
        double dir = vec.direction();
        System.out.printf("Result is %.5f radians (%.5f degrees)\n", dir, Math.toDegrees(dir));
    }

    /**
     * Takes in a expression and find the mormalized expression
     * @param s
     * @throws java.lang.Exception
     */
    private static void parseNormalizeExpression​(java.util.Scanner s) throws java.lang.Exception
    {
        s.next("unit");
        Vector330 vec = Vector330.parseVector(s);
        System.out.println(vec.toString());
        vec = vec.normalize();
        System.out.println(vec.toString());
    }
}
/*
 *  ▄▄▄▄▄▄▄   ▄ ▄ ▄▄▄▄▄▄▄
 *  █ ▄▄▄ █ █ █▄▀ █ ▄▄▄ █
 *  █ ███ █ ▄▄▄▄█ █ ███ █
 *  █▄▄▄▄▄█ █▀█ ▄ █▄▄▄▄▄█
 *  ▄▄ ▄  ▄▄▀ ▄▄▄ ▄▄▄ ▄▄
 *    ██▄ ▄ █▀▀█ ▀  ▀▀  █
 *    ▀▄▄█▄▄▀▄ ▄▀▀█ ▄▀  █
 *  ▄▄▄▄▄▄▄ ██▄▄▄▀█ ▄▀▀█▀
 *  █ ▄▄▄ █  ▄▀ ▄▄██ █ █
 *  █ ███ █ ▀██▄ ██▄█▄▄▀█
 *  █▄▄▄▄▄█ █  ▀▀▄ ██  ▄
 */
