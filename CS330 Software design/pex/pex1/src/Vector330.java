//Documentation: none
import java.util.regex.Pattern;

public class Vector330 {

    private double x;
    private double y;
    private static final double EPS = 1e-9;

    /**
     * creates a new vector, taking in no parameters and sets values to 0
     */
    public Vector330(){this.x = 0; this.y = 0;}

    /**
     * takes in two doubles and sets the vectors values to them
     * @param x
     * @param y
     */
    public Vector330(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Takes in two ints and sets the vectors to them
     * @param x
     * @param y
     */
    public Vector330(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Contructor that takes in two longs to make a Vector
     * @param x
     * @param y
     */
    public Vector330(long x, long y)
    {
        this.x = (double)x;
        this.y = (double)y;
    }

    /**
     * Returns the x value of a Vector
     * @return x
     */
    public double getX()
    {
        return x;
    }

    /**
     * Returns the y value of a vector
     * @return y
     */
    public double getY()
    {
        return y;
    }

    /**
     * Returns the x value of a vector in int form
     * @return x
     */
    public int getXint()
    {
        return (int)x;
    }

    /**
     * Returns the y value of a vector in int form
     * @return y
     */
    public int getYint()
    {
        return (int)y;
    }

    /**
     * Returns the x value of a vector in long form
     * @return x
     */
    public long getXlong()
    {
        return (long)x;
    }

    /**
     * returns the y value of a vector in long form
     * @return y
     */
    public long getYlong()
    {
        return (long)y;
    }

    /**
     * sets the x value of a vector to the double given
     * @param x
     */
    public void setX(double x)
    {
        this.x = x;
    }

    /**
     * Sets the y value of a vector to the double given
     * @param y
     */
    public void setY(double y)
    {
        this.y = y;
    }

    /**
     * Sets the x value of a vector to the int given
     * @param x
     */
    public void setX(int x)
    {
        this.x = x;
    }

    /**
     * Sets the y value of a vector to the int given
     * @param y
     */
    public void setY(int y)
    {
        this.y = y;
    }

    /**
     * Sets the x value of a vector to the long given
     * @param x
     */
    public void setX(long x)
    {
        this.x = (double)x;
    }

    /**
     * Sets the y value of a vector to the long given
     * @param y
     */
    public void setY(long y)
    {
        this.y = (double)y;
    }

    /**
     * Checks to see if vectors are equal to each other
     * @param v the second vecotr to be compared
     * @return boolean value of equality
     */
    public boolean equals(Vector330 v)
    {
        return (Math.abs(this.x - v.getX()) < EPS) && (Math.abs(this.y - v.getY()) < EPS);
    }

    /**
     * adds one vector to another
     * @param v the second vector to be added
     * @return a vector representing the added two vectors
     */
    public Vector330 add(Vector330 v)
    {
        return new Vector330(v.getX()+ this.x,v.getY()+ this.y);
    }

    /**
     * Subract one vector from another
     * @param v the vector that is subtracted
     * @return a vector representing what the answer is
     */
    public Vector330 subtract(Vector330 v)
    {
        return new Vector330(this.x-v.getX(), this.y-v.getY());
    }

    /**
     * Give the dot product of one vector and another
     * @param v the second vector to be in the equation
     * @return the scalar representation of the dot product
     */
    public double dotProduct(Vector330 v)
    {
        return this.x * v.getX() + this.y * v.getY();
    }

    /**
     * Scales the given vector by a scalar
     * @param s the amount to scale the vector by
     * @return the new vector of different size
     */
    public Vector330 scale(double s)
    {
        return new Vector330(this.x * s, this.y * s);
    }

    /**
     * Finds the length of the vector
     * @return the length of called vecttor
     */
    public double magnitude()
    {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }

    /**
     * Finds the direction of the vector in radians
     * @return the direction of the vector
     */
    public double direction()
    {
        return Math.atan(this.y/this.x);
    }

    /**
     * Finds the normilization of a vector
     * @return the vector of length 1 in same direction
     */
    public Vector330 normalize()
    {
        if(this.magnitude()<EPS)
        {
            return new Vector330(0,0);
        }
        return new Vector330(this.x/this.magnitude(), this.y/this.magnitude());
    }

    /**
     * Gets the vector in string form
     * @return the vector in string form
     */
    public java.lang.String toString()
    {
        return "< "+x+", "+y+" >";
    }

    /**
     * Parses the input and returns a new vector from valid input
     * @param s a scanner that contains the info to create a new vector from
     * @return A vector representing the passed info
     * @throws java.lang.Exception If there is a problem parsing the input throw error
     */
    public static Vector330 parseVector(java.util.Scanner s) throws java.lang.Exception
    {
        double xCoord;
        double yCoord;
        Pattern originalPattern = s.delimiter();
        s.useDelimiter("[" + originalPattern + ",]");
        if(s.hasNext("<"))
        {
            s.next("<");
            if(s.hasNextDouble())
            {
                xCoord = s.nextDouble();
                s.useDelimiter(originalPattern);
                if(s.hasNext(","))
                {
                    s.next(",");
                    if(s.hasNextDouble())
                    {
                        yCoord = s.nextDouble();
                        //s.useDelimiter(originalPattern);
                        if(s.hasNext(">"))
                        {
                            s.next(">");
                            return new Vector330(xCoord, yCoord);
                        }
                        else
                        {
                            throw new Exception("Did not find the final > in the scanner");
                        }
                    }
                    else
                    {
                        throw new Exception("Did not find the second double in the scanner");
                    }
                }
                else
                {
                    throw new Exception("Did not find a comma in the scanner");
                }
            }
            else
            {
                throw new Exception("Did not find a double in the scanner");
            }
        }
        else
        {
            throw new Exception("Did not find a < in the scanner ");
        }
    }
}
/*
 *  ▄▄▄▄▄▄▄   ▄ ▄ ▄▄▄▄▄▄▄
 *  █ ▄▄▄ █ █ █▄▀ █ ▄▄▄ █
 *  █ ███ █ ▄▄▄▄█ █ ███ █
 *  █▄▄▄▄▄█ █▀█ ▄ █▄▄▄▄▄█
 *  ▄▄ ▄  ▄▄▀ ▄▄▄ ▄▄▄ ▄▄
 *    ██▄ ▄ █▀▀█ ▀  ▀▀  █
 *    ▀▄▄█▄▄▀▄ ▄▀▀█ ▄▀  █
 *  ▄▄▄▄▄▄▄ ██▄▄▄▀█ ▄▀▀█▀
 *  █ ▄▄▄ █  ▄▀ ▄▄██ █ █
 *  █ ███ █ ▀██▄ ██▄█▄▄▀█
 *  █▄▄▄▄▄█ █  ▀▀▄ ██  ▄
 */
