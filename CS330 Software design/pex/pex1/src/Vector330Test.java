//Documentation: none
import java.util.Scanner;

public class Vector330Test {
    private static final double EPS = 1e-9;
    Vector330 a;
    Vector330 b;

    Vector330Test(){}
    @org.junit.jupiter.api.BeforeEach
    void setUp(){
        a = new Vector330(3,4);
        b = new Vector330(7,12);
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
        a = null;
        b = null;
    }

    @org.junit.jupiter.api.Test
    void testGetX() {
        System.out.println("Testing testGetX");
        assert(a.getX() == 3);
        assert(b.getX() ==7);
    }

    @org.junit.jupiter.api.Test
    void testGetY() {
        System.out.println("Testing testGetY");
        assert(a.getY() == 4);
        assert(b.getY() ==12);
    }

    @org.junit.jupiter.api.Test
    void testGetXint() {
        System.out.println("Testing testGetXint");
        assert(a.getXint() == 3);
        assert(b.getXint() ==7);
    }

    @org.junit.jupiter.api.Test
    void testGetYint() {
        System.out.println("Testing testGetYint");
        assert(a.getYint() == 4);
        assert(b.getYint() ==12);
    }

    @org.junit.jupiter.api.Test
    void testGetXlong() {
        System.out.println("Testing testGetXlong");
        assert(a.getXlong() == 3);
        assert(b.getXlong() ==7);
    }

    @org.junit.jupiter.api.Test
    void testGetYlong() {
        System.out.println("Testing testGetYlong");
        assert(a.getYlong() == 4);
        assert(b.getYlong() ==12);
    }

    @org.junit.jupiter.api.Test
    void testSetX() {
        System.out.println("Testing testSetX");
        a.setX(5);
        assert(a.getX() == 5);
        b.setX(20);
        assert(b.getX() == 20);
    }

    @org.junit.jupiter.api.Test
    void testSetY() {
        System.out.println("Testing testSetY");
        a.setY(9);
        assert(a.getY() == 9);
        b.setY(20);
        assert(b.getY() == 20);
    }

    @org.junit.jupiter.api.Test
    void testSetX1() {
    }

    @org.junit.jupiter.api.Test
    void testSetY1() {
    }

    @org.junit.jupiter.api.Test
    void testSetX2() {
    }

    @org.junit.jupiter.api.Test
    void testSetY2() {
    }

    @org.junit.jupiter.api.Test
    void testEquals() {
        System.out.println("Testing testEquals");
        assert(!a.equals(b));
        assert(b.equals(new Vector330(7,12)));
    }

    @org.junit.jupiter.api.Test
    void testAdd() {
        System.out.println("Testing testAdd");
        assert (a.add(b).magnitude()==new Vector330(10,16).magnitude());
        assert (a.add(new Vector330(4,8)).magnitude()==b.magnitude());
    }

    @org.junit.jupiter.api.Test
    void testSubtract() {
        System.out.println("Testing testSubtract");
        assert (b.subtract(a).magnitude()==new Vector330(4,8).magnitude());
        assert (b.subtract(new Vector330(4,8)).magnitude()==a.magnitude());
    }

    @org.junit.jupiter.api.Test
    void testDotProduct() {
        System.out.println("Testing testDotProduct");
        assert (a.dotProduct(b) == 69);
        assert (new Vector330(2, 5).dotProduct(new Vector330(6, 7))==47);
    }

    @org.junit.jupiter.api.Test
    void testScale() {
        System.out.println("Testing testScale");
        System.out.println(b.scale(5).magnitude());
        assert (a.scale(5).magnitude()==25);
        assert (b.scale(5).magnitude()==69.46221994724903);
    }

    @org.junit.jupiter.api.Test
    void testMagnitude() {
        System.out.println("Testing testMagnitude");
        assert (a.magnitude()==5);
        assert (b.magnitude()==Math.sqrt(193));
    }

    @org.junit.jupiter.api.Test
    void testDirection() {
        System.out.println("Testing testDirection");
        assert (Math.abs(a.direction()-0.9272952180016122)<1);
        assert (Math.abs(b.direction()-1.042721878368537)<1);
    }

    @org.junit.jupiter.api.Test
    void testNormalize() {
        System.out.println("Testing testNormalize");
        assert (a.normalize().equals(new Vector330(a.getX()/a.magnitude(),a.getY()/a.magnitude())));
        assert (b.normalize().equals(new Vector330(b.getX()/b.magnitude(),b.getY()/b.magnitude())));
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        System.out.println("Testing testToString");
        assert (a.toString().equalsIgnoreCase("< 3.0, 4.0 >"));
        assert (b.toString().equalsIgnoreCase("< 7.0, 12.0 >"));
    }

    @org.junit.jupiter.api.Test
    void testParseVector(){
        System.out.println("Testing testParseVector");
        Scanner testa = new Scanner("< 3, 4 >");
        Vector330 testavec;
        Scanner testb = new Scanner("< 7, 12 >");
        Vector330 testbvec;
        try
        {
            testavec = Vector330.parseVector(testa);
            assert (testavec.equals(a));
            testbvec = Vector330.parseVector(testb);
            assert (testbvec.equals(b));
        }catch(Exception e)
        {
            System.out.println(e);
        }


    }
}
/*
 *  ▄▄▄▄▄▄▄   ▄ ▄ ▄▄▄▄▄▄▄
 *  █ ▄▄▄ █ █ █▄▀ █ ▄▄▄ █
 *  █ ███ █ ▄▄▄▄█ █ ███ █
 *  █▄▄▄▄▄█ █▀█ ▄ █▄▄▄▄▄█
 *  ▄▄ ▄  ▄▄▀ ▄▄▄ ▄▄▄ ▄▄
 *    ██▄ ▄ █▀▀█ ▀  ▀▀  █
 *    ▀▄▄█▄▄▀▄ ▄▀▀█ ▄▀  █
 *  ▄▄▄▄▄▄▄ ██▄▄▄▀█ ▄▀▀█▀
 *  █ ▄▄▄ █  ▄▀ ▄▄██ █ █
 *  █ ███ █ ▀██▄ ██▄█▄▄▀█
 *  █▄▄▄▄▄█ █  ▀▀▄ ██  ▄
 */
