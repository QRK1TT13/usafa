/**
 * @author Hunter Manter
 */

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
public class HW1_Manter {
    private static Scanner scan = new Scanner(System.in);
    private static Random rand = new Random(System.currentTimeMillis());
    private static int difficulty = 1;  //level 4 difficulty will be a long that is randomly generated each guess.  level 4 is impossible but implemented for learning how to reverse engineer java files
    private static long number = 0;//the number the player is to guess
    private static boolean gameOver = false;//used to stop game loop
    private static String name;  //the players name
    private static long maxGuess;//will be set when difficulty is chosen
    private static long minGuess;//will be set when difficulty is chosen
    private static int guesses = 0;//number of guesses in a round
    private static ArrayList<Integer> totalGuesses = new ArrayList<Integer>();//keeps track of number of guesses per game
    private static ArrayList<Integer> totalDifficulty = new ArrayList<Integer>();//keeps track of the difficulty of the games
    public static void main(String[] args) {
        int numOfGames=0;
        welcome();  //Prints a welcome message for the player
        name = getPlayerName();
        do {//big loop allowing multiple play-throughs
            numOfGames++;
            getDifficulty(); //gets how hard the player wants the game to be
            setNumber(); //sets the number for the person to guess
            playGame();
        }while(playAgain());
        endGame();
    }
    /***********************************************************
     * 
     * description: Prints out a welcome message when the program is first called.  It will print sassy message and erase it so that it can print the nice, normal message
     */
    private static void welcome() {
        //prints welcome message.  The program will out put the snarky "the game where you lose." and the fixes it so that it only flashes on the screen
        System.out.print("Welcome to the high low game. The game where you lose.");
        sleep();
        System.out.print("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
        System.out.println("The game where you try and guess what number im thinking of.");
    }
    /***********************************************************
     * @return String
     * description: Gets the players name and stores it in the global variable
     */
    private static String getPlayerName()
    {
        System.out.print("What is your name? \n> ");
        String name = scan.nextLine();//a temp variable for the players name
        if(name.equals(""))
        {
            System.out.println("Your parents didn't give you a name? Mine either. I think I'll call you Player 1.");
            name="Player 1";
        }
        System.out.println("Alright "+ name+ ", welcome.  Lets begin.");
        return name;
    }
    /***********************************************************
     * 
     * 
     * description: Gets the level that the player wants and stores in global
     */
    private static void getDifficulty()
    {
        //set up game parameters
        gameOver=false;
        guesses=0;
        boolean valid = false;//keeping track of valid inputs
        int num = 0;// a temp value for the difficulty
        String input;
        while (true) {
            System.out.print("How hard would you like this round to be from 1 (easy) to 3 (you can't win)\n> ");
            if(scan.hasNextInt())
            {
                num= scan.nextInt();
                if (num<=4 &&num>=1)
                {
                    difficulty=num;
                    return;
                }
            }
            else
            {
                scan.nextLine();
                System.out.println("Lets try this again smarty pants.");
            }
        }

    }
    /***********************************************************
     * 
     * 
     * description: Uses the difficulty number to set the number that the player should be guessing and the numbers to track if the guesses are getting worse
     */
    private static void setNumber()
    {
        if (difficulty ==1 )
        {
            number = Math.abs(rand.nextInt())%100+1;
            maxGuess=0;
            minGuess=100;
            System.out.println("Thanks "+name+".  The range for difficulty "+difficulty+" will be from 1 to 100");
        }
        else if (difficulty ==2)
        {
            maxGuess=Integer.MIN_VALUE;
            minGuess=Integer.MAX_VALUE;
            number = rand.nextInt();
            System.out.println("Thanks "+name+".  The range for difficulty "+difficulty+" will be from "+ Integer.MIN_VALUE+ " to "+ Integer.MAX_VALUE);
        }
        else
        {
            maxGuess=Long.MIN_VALUE;
            minGuess=Long.MAX_VALUE;
            number = rand.nextLong();
            System.out.println("Thanks "+name+".  The range for difficulty "+difficulty+" will be from "+ Long.MIN_VALUE+ " to "+ Long.MAX_VALUE);
        }
    }
    /***********************************************************
     * 
     * 
     * description: Decides what to do with each number that it gets.  Also outputs messages in response.
     */
    private static void playGame()
    {
        long guess=getInput();//a temp value for the guess that error checking is done on
        while (!gameOver)
        {
            if(guess==number)
            {
                System.out.println("CONGRATS!!! YOU GUESSED MY NUMBER! It only took you "+guesses+" guesses.");
                gameOver=true;
                return;
            }
            if (difficulty==1)
            {
                if(guess>100 || guess<1)
                {
                    System.out.println("I'm not kind enough to look over simple mistakes "+name+", like guessing outside of the range I told you.  Try again");
                }
                else if (guess>number)
                {
                    if(guess>minGuess)
                    {
                        System.out.println("Come on "+name+", that's in the totally wrong direction. I already told you it was lower than that.");
                    }
                    else
                    {
                        if(guess<minGuess)
                        {
                            minGuess=guess;
                        }
                        System.out.println("Your guess was too high");
                    }
                }
                else if(guess<number)
                {
                    if(guess<maxGuess)
                    {
                        System.out.println("Come on "+name+", that's in the totally wrong direction. I already told you it was higher than that.");
                    }
                    else
                    {
                        if(guess>maxGuess)
                        {
                            maxGuess=guess;
                        }
                        System.out.println("Your guess was too low");
                    }
                }
            }
            else if (difficulty==2)
            {
                if(guess<Integer.MIN_VALUE || guess>Integer.MAX_VALUE)
                {
                    System.out.println("I'm not kind enough to look over simple mistakes "+name+", like guessing outside of the range I told you.");
                }
                else if (guess>number)
                {
                    if(guess>=minGuess)
                    {
                        System.out.println("Come on "+name+", that's in the totally wrong direction. I already told you it was lower than that.");
                    }
                    else
                    {
                        if(guess<minGuess)
                        {
                            minGuess=guess;
                        }
                        System.out.println("Your guess was too high");
                    }
                }
                else if(guess<number)
                {
                    if(guess<=maxGuess)
                    {
                        System.out.println("Come on "+name+", that's in the totally wrong direction. I already told you it was higher than that.");
                    }
                    else
                    {
                        if(guess>maxGuess)
                        {
                            maxGuess=guess;
                        }
                        System.out.println("Your guess was too low");
                    }
                }
            }
            else if (difficulty==3) {
                if (guess < Integer.MIN_VALUE || guess > Integer.MAX_VALUE) {
                    System.out.println("I'm not kind enough to look over simple mistakes " + name + ", like guessing outside of the range I told you.");
                } else if (guess > number) {
                    if (guess >= minGuess) {
                        System.out.println("Come on " + name + ", that's in the totally wrong direction. I already told you it was lower than that.");
                    } else {
                        if (guess < minGuess) {
                            minGuess = guess;
                        }
                        System.out.println("Your guess was too high");
                    }
                } else if (guess < number) {
                    if (guess <= maxGuess) {
                        System.out.println("Come on " + name + ", that's in the totally wrong direction. I already told you it was higher than that.");
                    } else {
                        if (guess > maxGuess) {
                            maxGuess = guess;
                        }
                        System.out.println("Your guess was too low");
                    }
                }
            }
            else if(difficulty==4) {
                if (guess < number) {
                    System.out.println("Your guess was too low");
                } else {
                    System.out.println("Your guess was too high");
                }
                number = rand.nextLong();
            }
        guess = getInput();
        }
    }
    /***********************************************************
     * 
     * @return long
     * description: Gets the input from the users guesses and error checks
     */
    private static long getInput()
    {
        scan.nextLine();  //needed to clear buffer before the method kicks off.
        System.out.print("Please enter your guess "+name+"\n>");
        while (true)
        {
            guesses++;
            if(scan.hasNextInt())
            {
                if (difficulty==1||difficulty==2)
                {
                    return scan.nextInt();
                }
            }
            else if(scan.hasNextLong())
            {
                if(difficulty==3||difficulty==4)
                {
                    return scan.nextLong();
                }
            }
            else
            {
                scan.nextLine();
            }
            System.out.println("Wow, that's not even a valid guess");
            System.out.print("Lets try this again "+ name+".  Enter your guess in number form\n>");
        }
    }
    /***********************************************************
     * 
     * @return boolean
     * description: Asks the player if they want to play the game again.  Updates game variables
     */
    private static boolean playAgain()
    {
        totalGuesses.add(guesses);
        totalDifficulty.add(difficulty);
        String answer;//a temp variable for the answer for error checking to be done on
        scan.nextLine();
        while (true)
        {
            System.out.print("Would you like to play again? (y/n)\n>");
            answer=scan.nextLine();
            if(answer.equalsIgnoreCase("y")||answer.equalsIgnoreCase("yes"))
            {
                return true;
            }
            else if(answer.equalsIgnoreCase("n")||answer.equalsIgnoreCase("no"))
            {
                return false;
            }
            System.out.println("Lets try this again "+name+".");
        }
    }
    /***********************************************************
     * 
     * 
     * description: Outputs messages based on the performance in the games
     */
    private static void endGame()
    {
        if (difficulty!=4)
        {
            System.out.println("Thanks for playing "+name+".  Maybe next time, you'll beat level " +(difficulty+1));
        }
        else
        {
            System.out.println("Thanks for playing "+name+".  Maybe next time you wont lose.");
        }
        int totalGuessesInt=0;
        for (int x=0;x<totalGuesses.size();x++)
        {
            totalGuessesInt+=totalGuesses.get(x);
        }
        System.out.println("Your total guesses were "+totalGuessesInt+".");
        int guess = 0;
        int gamesAtDifficulty=0;
        double optimal=0; //holds the optimal number of guesses
        double averageGuessNum=0;//will hold the average guesses for each difficulty
        double diff= 0;//a temporary variable for difficulty for use in the for loop and passing to methods
        for(int x =1;x<5;x++)
        {
            for(int i = 0;i<totalGuesses.size();i++)
            {
                if(totalDifficulty.get(i)==x)
                {
                    guess+=totalGuesses.get(i);
                    gamesAtDifficulty++;
                }
            }
            if(guess!=0)
            {
                averageGuessNum=Math.round(guess/gamesAtDifficulty*10)/10;
                optimal=getOptimumGuesses(x);
                diff=Math.round(averageGuessNum-optimal*10)/10;
                System.out.println(optimal);
                if(averageGuessNum<optimal-2)
                {
                    System.out.println("Wow "+name+" must have some great luck! Your average guess on level "+x+" was "+averageGuessNum+", "+ Math.abs(diff)+" less than the optimal number of guesses");
                }
                else if(averageGuessNum<optimal)
                {
                    System.out.println("Alright "+name+", not bad. On level "+x+" you're close to the optimum number of guesses of "+optimal+", at "+averageGuessNum);
                }
                else if (averageGuessNum<=optimal+1)
                {
                    System.out.println("I'm proud of you "+name+". For level"+x+" you had "+averageGuessNum+" guesses, the perfect amount of guesses");
                }
                else
                {
                    System.out.println("It must be hard not being the smartest person in the room "+name+". For level "+x+" you guessed "+averageGuessNum+" guesses.  That's "+Math.abs(diff)+" over the optimal amount");
                }
            }
            gamesAtDifficulty=0;
            guess=0;
        }

    }
    /***********************************************************
     *
     * @param diff the temporary difficulty
     * @return double
     * description: Gets the difficulty of past games and outputs the optimum amount of guesses for them
     */
    private static double getOptimumGuesses(int diff)
    {
        if(diff==1)
        {
            return (double)Math.round(Math.log(100-1.0)*10)/10;
        }
        else if(diff==2)
        {
            return (double)Math.round(Math.log(Integer.MAX_VALUE-Integer.MIN_VALUE)*10)/10;
        }
        else
        {
            return (double)Math.round(Math.log(Long.MAX_VALUE-Long.MIN_VALUE)*10)/10;
        }
    }
    /***********************************************************
     * 
     * 
     * description Sleeps for a couple milli seconds in order for sassy first message to be read
     */
    private static void sleep()
    {
        int timeToSleep=200;
        try {
            Thread.sleep(timeToSleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

/**
 *  ▄▄▄▄▄▄▄   ▄ ▄ ▄▄▄▄▄▄▄
 *  █ ▄▄▄ █ █ █▄▀ █ ▄▄▄ █
 *  █ ███ █ ▄▄▄▄█ █ ███ █
 *  █▄▄▄▄▄█ █▀█ ▄ █▄▄▄▄▄█
 *  ▄▄ ▄  ▄▄▀ ▄▄▄ ▄▄▄ ▄▄
 *    ██▄ ▄ █▀▀█ ▀  ▀▀  █
 *    ▀▄▄█▄▄▀▄ ▄▀▀█ ▄▀  █
 *  ▄▄▄▄▄▄▄ ██▄▄▄▀█ ▄▀▀█▀
 *  █ ▄▄▄ █  ▄▀ ▄▄██ █ █
 *  █ ███ █ ▀██▄ ██▄█▄▄▀█
 *  █▄▄▄▄▄█ █  ▀▀▄ ██  ▄
 */
