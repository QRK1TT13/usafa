/** PEX5.c
 * ===========================================================
 * Name: CS220, Spring 2019
 *
 * Purpose:  Code provided for PEX 5 - Debugging a word cloud
 * ===========================================================
 */
#include <stdio.h>
#include <ctype.h>
#include "CountedMap.h"
#include "WordCloud.h"
#include <string.h>

#define MAX_WORD_LENGTH 30
#define MAX_WORD_LENGTH_STRING "30"

/** -------------------------------------------------------------------
 * Removes non alphas from the input word and makes it all uppercase
 * @param word - word to convert
 *
  -------------------------------------------------------------------*/
void removePunctuationMakeUpperCase(STRING word)
{
	STRING src = word;
	STRING dst = word;

	while (*src)
	{
		if (!isalpha((unsigned char)*src))
		{
			/* Skip this character */
			src++;
		} else if (islower((unsigned char)*src))
		{
			/* Make it lowercase */
			*dst++ = toupper((unsigned char)*src);
			src++;
		} else if (src == dst)
		{
			/* Increment both pointers without copying */
			src++;
			dst++;
		} else
		{
			/* Copy character */
			*dst++ = *src++;
		}
	}

	*dst = 0;
}

/** -------------------------------------------------------------------
 * Counts the unique words in a file.
 * @param argFileName text file to count words in
 * @return number of total words in the file
  -------------------------------------------------------------------*/
int countWords(STRING argFileName)
{
	// open the file
	FILE *filePtr;

	filePtr = fopen(argFileName, "r");

	int wordCount = 0;

	if (filePtr)
	{
		// read in one word at at time and
		char charBuffer[MAX_WORD_LENGTH];

		while (fscanf(filePtr, "%"MAX_WORD_LENGTH_STRING"s", charBuffer) == 1)
		{
			// increment the counter
			wordCount++;
		}
	}

	fclose(filePtr);

	return wordCount;
}

/** -------------------------------------------------------------------
 * Reads a file of words to exclude from the word cloud.
 * @param argFileName - text file to get exclude words from
 * @return Map containing words to exclude
   -------------------------------------------------------------------*/
CountedMap* readExclusionFile(STRING argFileName)
{
	// get the count of words
	int wordCount = countWords(argFileName);

	// if there are no words then return NULL
	if (wordCount <= 0)
	{
		return NULL;
	} else
	{
		// open the file
		FILE *filePtr;

		filePtr = fopen(argFileName, "r");

		if (!filePtr)
			return NULL;

		// create a buffer
		char charBuffer[MAX_WORD_LENGTH];

		// initialize the CountedMap with the proper number of words
		CountedMap* returnMap = createMap(wordCount);

		// read in each word and add it to the counted map if it isn't already there
		while (fscanf(filePtr, "%"MAX_WORD_LENGTH_STRING"s", charBuffer) == 1)
		{
			removePunctuationMakeUpperCase(charBuffer);
			MapElement* currentElement = getValue(returnMap, charBuffer);
			if (currentElement == NULL)
			{
				addKeyValue(returnMap, charBuffer);
			}
		}

		// close the file and return
		fclose(filePtr);
		return returnMap;
	}

}

/** -------------------------------------------------------------------
 * Reads a file of words from story to build the word cloud.
 * @param argFileName - text file build word cloud from
 * @return Map containing words and counts from story
   -------------------------------------------------------------------*/
CountedMap* readStoryFile(STRING argFileName, CountedMap* exclusionList)
{
	// get the count of words
	int wordCount = countWords(argFileName);

	// if there are no words then return NULL
	if(wordCount <= 0)
	{
		return NULL;
	} else
	{
		// open the file
		FILE* filePtr;

		filePtr = fopen(argFileName, "r");

		if (!filePtr)
			return NULL;

		// create a buffer
		char charBuffer[MAX_WORD_LENGTH];

		// initialize the CountedMap with the proper number of words
		CountedMap* returnMap = createMap(wordCount);

		// read in each word and add it to the counted map if it isn't already there or update it's frequency
		while (fscanf(filePtr, "%"MAX_WORD_LENGTH_STRING"s", charBuffer) == 1)
		{
			removePunctuationMakeUpperCase(charBuffer);
			if (getValue(exclusionList, charBuffer) == NULL)
			{
				MapElement* currentElement = getValue(returnMap, charBuffer);
				if (currentElement == NULL)
				{
					addKeyValue(returnMap, charBuffer);
				} else
				{
					currentElement->countOfWord++;
				}
			}
		}

		// close the file and return
		fclose(filePtr);
		return returnMap;
	}

}

/** -------------------------------------------------------------------
 * Partitions the arrays into upper and lower portions around a pivot
 * @param argWordCounts - array of word frequencies
 * @param argWords - array of words
 * @param argLow - low index into arrays
 * @param argHigh - high index into arrays
 * @return pivot index
   -------------------------------------------------------------------*/
int partition(int* argWordCounts, STRING* argWords, int argLow, int argHigh)
{
	int midpoint = 0;
	int pivot = 0;

	int done = 0;

	// calc midpoint and pivot
	midpoint = argLow + (argHigh - argLow) / 2;
	pivot = argWordCounts[midpoint];

	while (!done)
	{
		// move the high pointer
		while (argWordCounts[argHigh] < pivot)
		{
			argHigh--;
		}

		// move the low pointer
		while (pivot < argWordCounts[argLow])
		{
			argLow++;
		}

		// check if the pointers have crossed
		if (argLow >= argHigh)
		{
			done = 1;
		} else
		{
			// swap frequency and word
			int tempInt;
			tempInt = argWordCounts[argLow];
			argWordCounts[argLow] = argWordCounts[argHigh];
			argWordCounts[argHigh] = tempInt;
            char tempString[50]; strcpy(tempString ,argWords[argLow]); strcpy(argWords[argLow], argWords[argHigh]); strcpy(argWords[argHigh],tempString);
			// move pointers
			argLow++;
			argHigh--;
		}
	}

	// return the location of the pivot point
	return argHigh;
}

/** -------------------------------------------------------------------
 * Quicksorts the arrays
 * @param argWordCounts - array of word frequencies
 * @param argWords - array of words
 * @param argLow - low index into arrays
 * @param argHigh - high index into arrays
   -------------------------------------------------------------------*/
void quickSortWords(int* argWordCounts, STRING* argWords, int argLow, int argHigh)
{
	// if the points haven't crossed
	if (argLow < argHigh)
	{
		// partition the arrays
		int pivot = partition(argWordCounts, argWords, argLow, argHigh);

		// sort the partitions
		quickSortWords(argWordCounts, argWords, argLow, pivot);
		quickSortWords(argWordCounts, argWords, pivot + 1, argHigh);
	}
}

/** -------------------------------------------------------------------
 * Prints the words and their frequencies
 * @param argWordCounts - array of word frequencies
 * @param argWords - array of words
   -------------------------------------------------------------------*/
void print(int* argWordCounts, STRING* argWords)
{
	// print out each word and it's frequency
	for (int index = 0; argWordCounts[index] > 0; index++)
	{
		printf("%5d %s\n", argWordCounts[index], argWords[index]);
	}
}

int main(int argc, char** argv)
{
	// check that an argument was provided
	if (argc <= 1)
	{
		printf("Please provide a story file to read, an optional exclusion word list, and an ");
		printf("optional filename to create.\n\n");
		printf("USAGE:  PEX5.exe storyFile [exclusionWordFile] [outputFileName]\n");
		return 0;
	}

	// create excluded words map and fill it in if the argument is provided
	CountedMap* excludedWords = NULL;



	excludedWords = readExclusionFile(argv[2]);


	// create and fill in the story word map
	CountedMap* storyWordMap = readStoryFile(argv[1], excludedWords);


	// Arrays to receeve words from
	int* wordCounts = NULL;
	STRING* words = NULL;

	// dump the map into the parallel arrays
	int numUniqueWords = getAllValuesInParallelArrays(storyWordMap, &wordCounts, &words);

	// if an output file is provided build the html word cloud

	buildWordCloud(wordCounts, words, argv[3], numUniqueWords);

	// sort the words by frequency, highest at the top
	quickSortWords(wordCounts, words, 0, numUniqueWords);

	// print the words
	print(wordCounts, words);

	// release memory
	deleteMap(excludedWords);
	deleteMap(storyWordMap);
	free(wordCounts);
	free(words);

 	return 1;
}