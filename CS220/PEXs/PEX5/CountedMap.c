/** CountedMap.c
 * ===========================================================
 * Name: CS220, Spring 2019
 *
 * Purpose:  Map ADT that uses the word as the key and the word
 *           and it's count as the data element.
 * ===========================================================*/
#include <string.h>
#include "CountedMap.h"

/** -------------------------------------------------------------------
 * Hashes the key -- the word
 * @param argPtrWord - word to hash into index value
 * @param argArraySize - size of the hash table
 * @return hash index location
   -------------------------------------------------------------------*/
int getHashIndex(STRING argPtrWord, int argArraySize)
{
	// use the sum of the ASCII values as the key for the word
	int sumOfChars = 0;
	int currChar = 0;

	// loop through the characters in the word and add them together
	while (argPtrWord[currChar] != '\0')
	{
		sumOfChars += (int)argPtrWord[currChar];
		currChar++;
	}

	return sumOfChars % argArraySize;
}

/** -------------------------------------------------------------------
 * creates a new element for the hash table
 * @param argPtrWord - word for the element
 * @return a MapElement to insert into a CountedMap
   -------------------------------------------------------------------*/
MapElement* createMapElement(STRING argPtrWord)
{
	// allocate a new MapElement
	MapElement* mapElement = malloc(sizeof(MapElement));
	mapElement->word= (char*)malloc(sizeof(char)*strlen(argPtrWord)+1);
	// assign initial values
	strcpy(mapElement->word ,argPtrWord);
	mapElement->countOfWord = 1;

	return mapElement;
}

/** -------------------------------------------------------------------
 * creates a new counted map
 * @param argSize - size to initilaize the hash table with
 * @return a CountedMap with a hashtable that has argSize buckets
   -------------------------------------------------------------------*/
CountedMap* createMap(int argSize)
{
	// allocate a CountedMap
	CountedMap* countedMap;
	countedMap = malloc(sizeof(CountedMap));
	
	// initialize values
	countedMap->tableSize = argSize;
	countedMap->hashTable = malloc(sizeof(MapElement*)*argSize);
	
	// ensure array is initialized to NULL
	for (int i = 0; i < argSize; i++)
		countedMap->hashTable[i] = NULL;

	return countedMap;
}

/** -------------------------------------------------------------------
 * Inserts or updates the CountedMap with a new (or repeated) word
 * @param argPtrCountedMap - CountedMap
 * @param argPtrWord - word to insert/update
 * @return 1 on success or 0 on failure
   -------------------------------------------------------------------*/
int addKeyValue(CountedMap *argPtrCountedMap, STRING argPtrWord)
{
	// check to ensure the map is valid, return otherwise
	if (argPtrCountedMap == NULL)
	{
		return 0;
	}

	// get the index of the key
	int hashIndex = getHashIndex(argPtrWord, argPtrCountedMap->tableSize);

	int currentIndex = hashIndex;

	// move through the hash table until you find a NULL cell
	while (argPtrCountedMap->hashTable[currentIndex] != NULL)
	{
		// increment the counter
		currentIndex++;

		// ensure index wraps around the table
		currentIndex = currentIndex % argPtrCountedMap->tableSize;

		// return false if the hash table is full
		if (hashIndex == currentIndex)
			return 0;
	}

	// allocate a new MapElement and assign initial values;
	argPtrCountedMap->hashTable[currentIndex] = createMapElement(argPtrWord);

	// return 1 on successful addition
	return 1;
}

/** -------------------------------------------------------------------
 * Gets the MapElement containing the frequency of the provided word
 * @param argPtrCountedMap - CountedMap
 * @param argPtrWord - word to get value fore
 * @return NULL if the word isn't found or the MapElement containing the
 * 			information
 --------------------------------------------------------------------*/
MapElement* getValue(CountedMap *argPtrCountedMap, STRING argPtrWord)
{
	// check to ensure the map is valid, return otherwise
	if (argPtrCountedMap == NULL)
	{
		return NULL;
	}

	// get the index of the key
	int hashIndex = getHashIndex(argPtrWord, argPtrCountedMap->tableSize);

	// move through the hash table until you find an empty index
	while (argPtrCountedMap->hashTable[hashIndex] != NULL)
	{
		// if the key matches then return that element
		if (strcmp(argPtrCountedMap->hashTable[hashIndex]->word, argPtrWord) == 0)
		{
			return argPtrCountedMap->hashTable[hashIndex];
		}
		
		//go to the next cell
		hashIndex++;
		
		//ensure index wraps around the hash table
		hashIndex = hashIndex % argPtrCountedMap->tableSize;
	}

	// return NULL if the key doesn't exist in the table
	return NULL;
}

/** -------------------------------------------------------------------
 * returns the words and their frequencies in a set of parallel arrays
 * @param argPtrCountedMap - CountedMap
 * @param outArgIntArray - out parameter containing the frequencies
 * @param outArgWordArray - out parameter containing the words
 * @return 1 on success or 0 on failure
 --------------------------------------------------------------------*/
int getAllValuesInParallelArrays(CountedMap *argPtrCountedMap, int **outArgIntArray, STRING**outArgWordArray)//double and triple pointer
{
	// check to ensure the map is valid, return otherwise
	if (argPtrCountedMap == NULL)
	{
		*outArgIntArray = NULL;
		*outArgWordArray = NULL;
		return 0;
	}

	// allocate memory for the out parameters
	*outArgIntArray = malloc(sizeof(int) * argPtrCountedMap->tableSize);
	*outArgWordArray = malloc(sizeof(char*) * argPtrCountedMap->tableSize);

	int currIndex = 0;

	// loop through the hashtable 
	for (int i = 0; i < argPtrCountedMap->tableSize; i++)
	{
		// copy values into the out parameters if they exist
		if (argPtrCountedMap->hashTable[i] != NULL)
		{
			(*outArgIntArray)[currIndex] = argPtrCountedMap->hashTable[i]->countOfWord;
			(*outArgWordArray)[currIndex] = argPtrCountedMap->hashTable[i]->word;
			currIndex++;
		
	
		}
	}
	(*outArgIntArray)[currIndex] = -1;
	(*outArgWordArray)[currIndex] = "\0";

	return currIndex;
}

/** -------------------------------------------------------------------
 * deletes the CountedMap by freeing the memory
 * @param argPtrCountedMap - CountedMap
 --------------------------------------------------------------------*/
void deleteMap(CountedMap *argPtrCountedMap)
{
	// free the MapElement array
	free(argPtrCountedMap->hashTable);

	// free the CountedMap
	free(argPtrCountedMap);
}