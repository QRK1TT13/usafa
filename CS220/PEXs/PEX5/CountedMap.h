/** CountedMap.h
 * ===========================================================
 * Name: CS220, Spring 2019
 *
 * Purpose:  Map that uses the word as the key and the word
 *           and it's count as the data element.
 * ===========================================================*/

#include <stdlib.h>

#ifndef COUNTEDMAP_H
#define COUNTEDMAP_H

#define STRING char*

typedef struct MapElement
{
	STRING word;
	int countOfWord;
} MapElement;


typedef struct CountedMap
{
	int tableSize;
	MapElement **hashTable;
} CountedMap;

int getHashIndex(STRING argPtrWord, int argArraySize);
CountedMap* createMap(int argSize);
int addKeyValue(CountedMap *argPtrCountedMap, STRING argPtrWord);
MapElement* getValue(CountedMap *argPtrCountedMap, STRING argPtrWord);
MapElement* createMapElement(STRING argPtrWord);
int getAllValuesInParallelArrays(CountedMap *argPtrCountedMap, int **outArgIntArray, STRING**outArgWordArray);

void deleteMap(CountedMap *argPtrCountedMap);

#endif

