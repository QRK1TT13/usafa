//
// Created by Hunter Manter on 2019-03-20.
//

#ifndef MYEXE_TERNARYTREE_H
#define MYEXE_TERNARYTREE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

typedef struct node {
    char data;
    struct node *left;
    struct node *right;
    struct node *down;
} node;

typedef struct {
    node *head;
    int size;
} ternaryTree;

ternaryTree *createTernaryTree();

bool isEmpty(ternaryTree *t);

void printTree(ternaryTree *t);

void printNode(node *node);

void insertArrayIntoTernaryTree(ternaryTree *t, char *in, int start, int stop);

void insertWordIntoTernaryTree(ternaryTree *t, char *word);

int findInTree(ternaryTree *t, node *onNode, char *word);

void deleteTree(ternaryTree *t);

void deleteNode(node *node);

void treeSearch(ternaryTree *t, char *word);

#endif //MYEXE_TERNARYTREE_H
