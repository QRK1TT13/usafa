//
// Created by Hunter Manter on 2019-03-20.
//

int main(int argc, char *argv[]) {
    char file[100];
    char checkFileName[100];
    strcpy(file, argv[1]);
    strcpy(checkFileName, argv[2]);

    printf("File is %s\n", file);
    FILE *fp = fopen(file, "r");
    // Check if file exists
    int i = 0;
    char in[178690][50];
    if (fp == NULL) {
        printf("Could not open file %s", file);
    }
    while (!feof(fp)) {
        fscanf(fp, "%s\n", in[i]);
        //printf("%s\n",in[i]);
        i++;
    }
    fclose(fp);

    ternaryTree *t = createTernaryTree();
    insertArrayIntoTernaryTree(t, (char *) in, 0, i - 1);
    deleteTree(t);


}