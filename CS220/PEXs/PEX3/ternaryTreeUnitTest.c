//
// Created by Hunter Manter on 2019-03-20.
//

#include "ternaryTree.h"
#include "pex3.h"

int main() {
    char file[100] = "../usafa_cs220_s19_student/PEXs/PEX3/test_dictionary.txt";
    printf("File is %s\n", file);
    FILE *fp = fopen(file, "r");
    // Check if file exists
    int i = 0;
    char in[8][50];
    if (fp == NULL) {
        printf("Could not open file %s", file);
    }
    while (!feof(fp)) {
        fscanf(fp, "%s\n", in[i]);
        //printf("%s\n",in[i]);
        i++;
    }
    fclose(fp);
    printf("Length of file is %d\n", i);

    ternaryTree *t = createTernaryTree();
    if (t->head == NULL) {
        //printf("its null\n");

    }
    insertArrayIntoTernaryTree(t, (char *) in, 0, i - 1);
    printf("print tree\n");
    printTree(t);
    printf("search for BAT\n");
    treeSearch(t, "BAT");
    printf("delete tree\n");
    deleteTree(t);
    return 0;
}

