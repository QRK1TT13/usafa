//
// Created by Hunter Manter on 2019-03-20.
//

#include "ternaryTree.h"


ternaryTree *createTernaryTree() {
    ternaryTree *new = malloc(sizeof(ternaryTree));
    new->head = NULL;
    new->size = 0;
    return new;
}

void insertArrayIntoTernaryTree(ternaryTree *t, char *in, int start, int stop) {
    if (start > stop) {
        return;
    } else {
        int mid = (stop - start) / 2 + start;
        char word[50];
        strcpy(word, (in + mid * 50));
        //printf("%d %s\n",mid,word);
        insertWordIntoTernaryTree(t, word);
        insertArrayIntoTernaryTree(t, in, start, mid - 1);
        insertArrayIntoTernaryTree(t, in, mid + 1, stop);
    }
}

void insertWordIntoTernaryTree(ternaryTree *t, char *word) {
    node *onNode;
    printf("try\n");
    if (t->head == NULL) {
        int onChar = 0;
        node *new;
        //onNode=*t->head;
        for (int i = 0; i < strlen(word); i++) {
            new = malloc(sizeof(node));
            new->down = NULL;
            new->right = NULL;
            new->left = NULL;
            new->data = word[onChar];
            if (i == 0) { t->head = new; }
            else {
                onNode->down = new;
            }
            onNode = new;
            printf("Char added: %c\n", new->data);
            onChar++;
        }
        new = malloc(sizeof(node));
        new->down = NULL;
        new->right = NULL;
        new->left = NULL;
        new->data = '\0';
        onNode->down = new;
        printf("Char added: %c\n", new->data);
    } else {
        onNode = t->head;
        int onChar = findInTree(t, onNode, word);
        printf("word: %s %d %d \n", word, (int) strlen(word), onChar);
        node *new;
        for (int i = 0; onChar < strlen(word); i++) {
            new = malloc(sizeof(node));
            new->down = NULL;
            new->right = NULL;
            new->left = NULL;
            new->data = word[onChar];
            if (word[onChar] < onNode->data) {
                onNode->left = new;
            }
            if (word[onChar] > onNode->data) {
                onNode->right = new;
            }
            if (word[onChar] == onNode->data) {
                onNode->down = new;
            }
            onNode = new;
            printf("Char added: %c\n", new->data);
            onChar++;
        }
        new = malloc(sizeof(node));
        new->down = NULL;
        new->right = NULL;
        new->left = NULL;
        new->data = '\0';
        onNode->down = new;
        printf("Char added: %c\n", new->data);
    }

    /*
    if(onChar<strlen(word)-1)
    {
        node *new = malloc(sizeof(node));
        new->down = NULL;
        new->right = NULL;
        new->left = NULL;
        new->data = word[onChar];
        if(t->head==NULL)
        {
            t->head = new;
        }
        else if(word[onChar]<onNode.data)
        {
            onNode.left = new;
        }
        else if(word[onChar]>onNode.data)
        {
            onNode.right = new;
        }
        onNode = *new;
        t->size ++;printf("n1%c\n",word[onChar]);
        onChar++;
        while(onChar<strlen(word))
        {
            new = malloc(sizeof(node));
            new->down = NULL;
            new->right = NULL;
            new->left = NULL;
            new->data = word[onChar];
            onNode.down = new;
            onNode = *new;
            t->size ++;printf("n2%c\n",word[onChar]);
            onChar++;
        }
    }
    node *new = malloc(sizeof(node));
    new->down = NULL;
    new->right = NULL;
    new->left = NULL;
    new->data = '\0';
    onNode.down = new;
    t->size ++;printf("n3\n");
    */
    printTree(t);
}

int findInTree(ternaryTree *t, node *onNode, char *word) {
    int onChar = 0;
    int wordLen = (int) strlen(word);
    //for all letters in the word
    while (onChar < wordLen) {
        if ((word[onChar] < onNode->data && onNode->left == NULL) ||
            (word[onChar] > onNode->data && onNode->right == NULL) ||
            (word[onChar] == onNode->data && onNode->down == NULL)) {
            break;
            //when no more of the word is present, break
        }
        if (word[onChar] < onNode->data) {
            onNode = onNode->left;
        }
        if (word[onChar] > onNode->data) {
            onNode = onNode->right;
        }
        if (word[onChar] == onNode->data) {
            onNode = onNode->down;
            onChar++;
        }
    }
    return onChar;
}

void deleteTree(ternaryTree *t) {
    deleteNode(t->head);
    free(t);
}

void deleteNode(node *node) {
    if (node == NULL) {
        return;
    }
    deleteNode(node->left);
    deleteNode(node->right);
    deleteNode(node->down);
    free(node);
}

void printTree(ternaryTree *t) {
    //printf("%c\n",t->head->data);
    printNode(t->head);
}

void printNode(node *node) {
    if (node == NULL) {
        //printf("node is null\n");
        return;
    }
    printNode(node->left);
    printNode(node->down);
    printNode(node->right);
    printf("Data: %c\n", node->data);
}

void treeSearch(ternaryTree *t, char *word) {
    int length = strlen(word);
    node *onNode = t->head;
    int onChar = 0;
    while (onChar <= length) {
        if (onNode == NULL) {
            printf("Wrong spelling\n");
            return;
        } else if (word[onChar] < onNode->data) {
            onNode = onNode->left;
        } else if (word[onChar] > onNode->data) {
            onNode = onNode->right;
        } else if (word[onChar] < onNode->data) {
            onNode = onNode->down;
            onChar++;
        }
    }
    if (onNode->down == '\0' || onNode->right == '\0' || onNode->left == '\0') {
        printf("Correct Spelling\n");
    }
}