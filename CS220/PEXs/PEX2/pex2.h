//
// Created by Hunter Manter on 2019-02-24.
//

#ifndef USAFA_CS220_S18_STUDENT_PEX2_H
#define USAFA_CS220_S18_STUDENT_PEX2_H

#include "listAsLinkedList.h"
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

void generateFood(doubleLinkedList *l, cord *food);

void drawScreen(doubleLinkedList *l, WINDOW *window, cord *food, int score);

void checkKeys(int *dir, bool *userEnd);

void checkLose(doubleLinkedList *l, bool *endGame);

void welcome(WINDOW *window);

void moveSnake(doubleLinkedList *l, int direction, cord *food, int *score);

bool checkCollision(doubleLinkedList *l, cord place);


#endif //USAFA_CS220_S18_STUDENT_PEX2_H
