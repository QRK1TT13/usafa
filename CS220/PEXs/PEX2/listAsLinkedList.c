//
// Created by Hunter Manter on 2019-02-24.
//

#include "listAsLinkedList.h"

/**
 * this function generates food in a new place
 * @return dobleLinkedList - the pointer to a new doubly linked list
 */
doubleLinkedList *createLinkedList() {
    doubleLinkedList *new = malloc(sizeof(doubleLinkedList));
    new->head = NULL;
    new->tail = NULL;
    new->size = 0;
    return new;
}

/**
 * this function deletes a doubly linked list by deleting the contents and then the head
 * @param l - the linked list that holds all the snake information
 */
void deleteLinkedList(doubleLinkedList *l) {
    while (l->size > 0) {
        deleteElementLinkedList(l, 0);
    }
    free(l);
}

/**
 * this function puts a new node at the beginning
 * @param l - the linked list that holds all the snake information
 * @param cords - cordinates that the new node should hold
 */
void appendElementLinkedList(doubleLinkedList *l, cord cords) {
    node *new = malloc(sizeof(node));
    cord *newcord = malloc(sizeof(cord));
    new->cords = newcord;
    new->cords->x = cords.x;
    new->cords->y = cords.y;
    if (lengthOfLinkedList(l) != 0) {
        new->prev = NULL;
        new->next = l->head;
        l->head->prev = new;
        l->head = new;
    } else {
        new->prev = NULL;
        new->next = NULL;
        l->tail = new;
        l->head = new;
    }
    l->size++;
}

/**
 * this function finds the length of the linked list
 * @param l - the linked list that holds all the snake information
 * @return int - the length of the list
 */
int lengthOfLinkedList(doubleLinkedList *l) {
    return l->size;
}


/**
 * this function prints the linked list to the terminal
 * @param l - the linked list that holds all the snake information
 */
void printLinkedList(doubleLinkedList *l) {
    if (lengthOfLinkedList(l) != 0) {
        node *on = l->head;
        printf("%d,%d\n", on->cords->x, on->cords->y);
        on = on->next;
        while (on != NULL) {
            printf("%d,%d\n", on->cords->x, on->cords->y);
            on = on->next;
        }
    } else {
        printf("List is empty\n");
    }
}


/**
 * this function generates food in a new place
 * @param l - the linked list that holds all the snake information
 * @param position - the position in the list to get the coordinates from
 * @return node - the information from the linked list at the position specified
 */
node getElementLinkedList(doubleLinkedList *l, int position) {
    if (lengthOfLinkedList(l) != 0) {
        if (position < lengthOfLinkedList(l) && position >= 0) {
            node *on = l->head;
            for (int i = 0; i < position; i++) {
                on = on->next;
            }
            return *on;
        } else {
            printf("Position is out of bounds\n");
        }
    } else {
        printf("List is empty\n");
    }
}

/**
 * this function changes the information at a position
 * @param l - the linked list that holds all the snake information
 * @param position - the position in the linked list to change
 * @param cords - the new coordinates to be put at position
 */
void changeElementLinkedList(doubleLinkedList *l, int position, cord cords) {
    if (lengthOfLinkedList(l) != 0) {
        if (position < lengthOfLinkedList(l) && position >= 0) {
            node *on = l->head;
            for (int i = 0; i < position; i++) {
                on = on->next;
            }
            on->cords->x = cords.x;
            on->cords->y = cords.y;
        } else {
            printf("Position is out of bounds\n");
        }
    } else {
        printf("List is empty\n");
    }
}

/**
 * this function deletes a node at position
 * @param l - the linked list that holds all the snake information
 * @param position - the position in the list to delete
 */
void deleteElementLinkedList(doubleLinkedList *l, int position) {
    if (lengthOfLinkedList(l) == 0) {
        printf("List is empty\n");
    } else if (position >= lengthOfLinkedList(l)) {
        printf("Position is out of bounds\n");
    } else if (lengthOfLinkedList(l) == 1) {
        free(l->head->cords);
        free(l->head);
        l->head = NULL;
        l->tail = NULL;
        l->size--;
    } else if (position == 0) {
        node *tmp = l->head;
        l->head = l->head->next;
        l->head->prev = NULL;
        free(tmp->cords);
        free(tmp);
        l->size--;
    } else if (position == lengthOfLinkedList(l) - 1) {
        node *tmp = l->tail;
        l->tail = l->tail->prev;
        l->tail->next = NULL;
        free(tmp->cords);
        free(tmp);
        l->size--;
    } else {
        node *on = l->head;
        for (int i = 0; i < position; i++) {
            on = on->next;
        }
        node *tmp = on->prev;
        tmp->next = on->next;
        tmp = on->next;
        tmp->prev = on->prev;
        free(on->cords);
        free(on);
        l->size--;
    }
}

/**
 * this function inserts a new node at a postition
 * @param l - the linked list that holds all the snake information
 * @param position - the new position to insert the data at
 * @param cords - the data to put in the new node
 */
void insertElementLinkedList(doubleLinkedList *l, int position, cord cords) {
    if (lengthOfLinkedList(l) == 0) {
        appendElementLinkedList(l, cords);
    } else if (position >= lengthOfLinkedList(l)) {
        printf("Position is out of bounds\n");
    } else {
        node *new = malloc(sizeof(node));
        cord *newcord = malloc(sizeof(cord));
        new->cords = newcord;
        new->cords->x = cords.x;
        new->cords->y = cords.y;
        node *on = l->head;
        for (int i = 0; i < position; i++) {
            on = on->next;
        }
        if (lengthOfLinkedList(l) - 1 == position) {
            new->prev = l->tail;
            new->next = NULL;
            l->tail = new;
            on->next = new;
        } else {

            new->next = on->next;
            new->prev = on;
            on->next = new;
            on = new->next;
            on->prev = new;
        }
        l->size++;
    }
}

/**
 * this function generates food in a new place
 * @param l - the linked list that holds all the snake information
 * @param cords - the data to search for in the list
 * @return int - the position where the data is in the list
 */
int findElementLinkedList(doubleLinkedList *l, cord cords) {
    node *on = l->head;
    int at = -1;
    int found = 0;
    while (on != NULL) {
        at++;
        if (on->cords->x == cords.x && on->cords->y == cords.y) {
            found = 1;
            break;
        }
        on = on->next;
    }
    if (found) {
        return at;
    } else {
        return -1;
    }
}