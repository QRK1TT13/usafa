//
// Created by Hunter Manter on 2019-02-24.
//

#ifndef USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
#define USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H

#include <stdlib.h>
#include <stdio.h>

typedef struct {
    int x;
    int y;
} cord;

typedef struct {
    struct node *next;
    struct node *prev;
    cord *cords;
} node;

typedef struct {
    node *head;
    node *tail;
    int size;
} doubleLinkedList;

doubleLinkedList *createLinkedList();

void deleteLinkedList(doubleLinkedList *l);

void appendElementLinkedList(doubleLinkedList *l, cord cords);

int lengthOfLinkedList(doubleLinkedList *l);

void printLinkedList(doubleLinkedList *l);

node getElementLinkedList(doubleLinkedList *l, int position);

void changeElementLinkedList(doubleLinkedList *l, int position, cord cords);

void deleteElementLinkedList(doubleLinkedList *l, int position);

void insertElementLinkedList(doubleLinkedList *l, int position, cord cords);

int findElementLinkedList(doubleLinkedList *l, cord cords);

#endif //USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
