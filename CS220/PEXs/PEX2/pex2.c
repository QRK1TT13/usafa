//
// Created by Hunter Manter on 2019-02-24.
//
#define HEIGHT LINES/2
#define WIDTH COLS/2
#define RATE 100
#define OFFSETX ((COLS - WIDTH) / 2)
#define OFFSETY ((LINES - HEIGHT) / 2)
#define ORIGINX LINES/4
#define ORIGINY COLS/4

#include "pex2.h"

int main() {
    srand((unsigned int) time(NULL));
    WINDOW *window;
    initscr();
    noecho();
    cbreak();
    curs_set(0);
    timeout(RATE);
    keypad(stdscr, TRUE);
    window = newwin(HEIGHT, WIDTH, OFFSETY, OFFSETX);
    welcome(window);

    int score;
    bool endGame = false;
    bool userEnd = false;

    doubleLinkedList *l = createLinkedList();
    cord head;
    head.y = HEIGHT / 2;
    head.x = WIDTH / 2;
    appendElementLinkedList(l, head);

    cord *food = &head;
    generateFood(l, food);


    int dir = -1;
    while (dir == -1) {
        checkKeys(&dir, &userEnd);
    }

    wrefresh(window);
    refresh();

    while (!endGame && !userEnd) {
        checkKeys(&dir, &userEnd);
        moveSnake(l, dir, food, &score);
        drawScreen(l, window, food, score);
        mvwaddch(window, HEIGHT + 1, WIDTH + 1, ' ');
        refresh();
        wrefresh(window);
        wclear(window);
        checkLose(l, &endGame);
    }
    mvprintw(LINES / 2, (COLS - 27) / 2, "Congrats! Your score was %d", score);
    wrefresh(window);
    refresh();
    sleep(5);
    endwin();
    return 0;
}

/**
 * this function generates food in a new place
 * @param l - the linked list that holds all the snake information
 * @param food - a pointer to the cordinates the food is at
 */
void generateFood(doubleLinkedList *l, cord *food) {
    cord tmp;
    tmp.x = (int) random() % (WIDTH - 2) + 1;
    tmp.y = (int) random() % (HEIGHT - 2) + 1;
    while (checkCollision(l, tmp)) {
        tmp.x = (int) random() % WIDTH;
        tmp.y = (int) random() % HEIGHT;
    }
    *food = tmp;
}

/**
 * this function draws the screen
 * @param l - the linked list that holds all the snake information
 * @param window - this is a pointer to the window that the game is played in
 * @param food - a pointer to the cordinates the food is at
 * @param score - a pointer to the score so that it can be updated
 */
void drawScreen(doubleLinkedList *l, WINDOW *window, cord *food, int score) {
    box(window, 0, 0);
    mvprintw(1, COLS / 2, "Score: %d", score);
    mvwaddch(window, food->y, food->x, 'f');//food

    cord *seg = getElementLinkedList(l, 0).cords;

    for (int i = 1; i < lengthOfLinkedList(l); i++) {
        seg = getElementLinkedList(l, i).cords;
        mvwaddch(window, seg->y, seg->x, 'o');
    }
    seg = getElementLinkedList(l, 0).cords;
    mvwaddch(window, seg->y, seg->x, '@'); //head
}

/**
 * this function gets the user input for the keys pressed and updates direction or pauses the game
 * @param dir - the currecnt direction of the snake
 * @param userEnd - a pointer to update if the game should end or not
 */
void checkKeys(int *dir, bool *userEnd) {
    int ch = *dir;
    int cha = getch();
    switch (cha) {
        case (KEY_UP):
            ch = 0;
            break;
        case (KEY_RIGHT):
            ch = 1;
            break;
        case (KEY_DOWN):
            ch = 2;
            break;
        case (KEY_LEFT):
            ch = 3;
            break;
        case ('q'):
            *userEnd = true;
            break;
        case ('p'):
            while (getch() != 'r') { sleep(1); }
            break;
    }
    if (ch != *dir && (ch + 2) % 4 != *dir) {
        *dir = ch;
    }
}

/**
 * this function checks if the player has lost by hitting itself or the wall
 * @param l - the linked list that holds all the snake information
 * @param endGame - a pointer to update if the game should end or not
 */

void checkLose(doubleLinkedList *l, bool *endGame) {
    bool tmp = false;
    cord *head = getElementLinkedList(l, 0).cords;
    for (int i = 1; i < lengthOfLinkedList(l); i++) {
        if (head->x == getElementLinkedList(l, i).cords->x && head->y == getElementLinkedList(l, i).cords->y) {
            *endGame = true;
        }
    }
    if (head->x <= 0) { *endGame = true; }
    if (head->y <= 0) { *endGame = true; }
    if (head->y >= HEIGHT - 1) { *endGame = true; }
    if (head->x >= WIDTH - 1) { *endGame = true; }
    endGame = &tmp;
}

/**
 * this function displays the welcome screen
 * @param window - a reference to the window that the game is played in to write to it
 */

void welcome(WINDOW *window) {
    wclear(window);
    mvprintw(LINES / 2, (COLS - 16) / 2, "%s", "Welcome to snake");
    refresh();
    sleep(1);
    wclear(window);
    box(window, 0, 0);
    wrefresh(window);
    mvprintw(LINES / 2, (COLS - 27) / 2, "%s", "Press an arrow key to start");
    mvprintw(LINES / 2 + 1, (COLS - 9) / 2, "%s", "q to quit");
    //sleep(2);
    refresh();
    wrefresh(window);
}

/**
 * this function moves the snake using the current direction
 * @param l - the linked list that holds all the snake information
 * @param direction - the directin that the snake is going
 * @param food - a pointer to the cordinates the food is at
 * @param score - the current score of the game, to be updated when snake hits food
 */
void moveSnake(doubleLinkedList *l, int direction, cord *food, int *score) {

    cord last = *getElementLinkedList(l, 0).cords;
    cord tmp = *getElementLinkedList(l, 0).cords;
    if (direction == 0)//up
    {
        tmp.y = tmp.y - 1;
    } else if (direction == 1)//right
    {
        tmp.x = tmp.x + 1;
    } else if (direction == 2)//down
    {
        tmp.y = tmp.y + 1;
    } else if (direction == 3)//left
    {
        tmp.x = tmp.x - 1;
    }
    changeElementLinkedList(l, 0, tmp);
    insertElementLinkedList(l, 0, last);
    if (l->head->cords->y == food->y && l->head->cords->x == food->x) {
        *score = *score + 1;
        generateFood(l, food);
    } else {
        deleteElementLinkedList(l, lengthOfLinkedList(l) - 1);
    }
}

/**
 * this function checks if there has been a collision
 * @param l - the linked list that holds all the snake information
 * @param place - the place on the board to check if the snake hit
 */
bool checkCollision(doubleLinkedList *l, cord place) {
    for (int i = 0; i < lengthOfLinkedList(l); i++) {
        if (getElementLinkedList(l, i).cords->x == place.x && getElementLinkedList(l, i).cords->y == place.y) {
            return true;
        }
    }
    return false;
}