/** queueAsArray.c
* ===========================================================
* Name: Hunter Manter
* Section: xxx
* Project: Lab09 - Queue ADT implemented with C array
* ===========================================================
*/
#include <stdio.h>
#include <stdlib.h>
#include "queueAsArray.h"


/** Task 0) - write the implementations for circular queue functions
 * 0) implement functions below
 * 1) test each function in Lab09_queue.c prior to completing
 * the DMV application.
 */

/** enqueue() - adds an element to the back of the queue
 * @param queue - a ptr to the queue structure
 * @param element - the item to add to the queue
 * @return 1 on success, -1 on failure
 */
int enqueue(QueueAsArray* queue, int element) {
    if(queueIsFull(queue))
    {
        return -1;
    }
    queue->number[queue->front]=element;
    queue->front++;
    return 1;
}


/** dequeue() - removes an element from the front of the queue
 * @param queue - a ptr to the queue structure
 * @return - the top of the queue on success, -1 on failure
 */
int dequeue(QueueAsArray* queue) {
    printf("%d\n",queuePeek(queue));
    if(queueIsEmpty(queue))
    {
        return -1;
    }
    queue->number[queue->back]=0;
    queue->back++;
    return 1;
}


/** queueIsEmpty() - determines if the queue is empty
 * @param queue - a ptr to the queue structure
 * @return - true if the queue is empty or false
 */
bool queueIsEmpty(QueueAsArray* queue) {
    return queue->front==queue->back;
}


/** queueIsFull() - determines if the queue is full
 * @param queue - a ptr to the queue structure
 * @return - true if the queue is full or false
 */
bool queueIsFull(QueueAsArray* queue) {
    if((queue->front+1)%QUEUE_MAX_SIZE==queue->back)
    {return true;}
    return false;
}


/** queueInit() - initializes the queue structure
 * @param queue - a ptr to the queue structure
 */
void queueInit(QueueAsArray* queue) {
    queue->front=0;
    queue->back=0;
}


/** queuePeek() - returns the item on the front of the
 * queue but doesn't remove it
 * @param queue - a ptr to the queue structure
 * @return - the item at the front of the queue or -1 on failure
 */
int queuePeek(QueueAsArray* queue) {
    return queue->number[queue->back];
}


/** queueSize() - determines the size of the queue
 * @param queue - a ptr to the queue structure
 * @return - number of items in the queue
 */
int queueSize(QueueAsArray* queue) {
    return (queue->front-queue->back)%QUEUE_MAX_SIZE;

}


/** queuePrint() - outputs the queue to the console
 * @param queue - ptr to the queue structure
 */
void queuePrint(QueueAsArray* queue) {
    for(int i =queue->back;i%QUEUE_MAX_SIZE!=queue->front%QUEUE_MAX_SIZE;i++)
    {
        printf("%d\n",queue->number[i]);
    }
}