//
// Created by C21Hunter.Manter on 9/4/2018.
//
#include <stdio.h>
#include <math.h>
#include "lab09functs.h"

int main()
{
    printf("Enter cylinder radius followed by height:\n");
    double radius;
    double height;
    scanf("%lf %lf",&radius, &height );
    printf("The volume is %lf.\n", volumeCylinder(radius, height));
    printf("Enter box width followed by height followed by depth:\n");
    double width;
    double heightB;
    double depth;
    scanf("%lf %lf %lf", &width, &heightB, &depth);
    printf("The volume is %lf.\n",volumeBox(width, heightB, depth));
    printf("Enter degrees: \n");
    double deg;
    scanf("%lf", &deg);
    printf("%lf degrees is %lf radians.\n", deg, degToRad(deg));

    return 0;
}