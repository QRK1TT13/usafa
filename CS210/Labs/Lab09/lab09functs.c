//
// Created by C21Hunter.Manter on 9/4/2018.
//
#include "lab09functs.h"
#include <math.h>
double volumeCylinder(double radius, double height)
{
    return M_PI*radius*radius*height;
}
double volumeBox(double depth, double width, double height)
{
    return depth*width*height;
}
double degToRad(double degrees)
{
    return degrees * M_PI /180;
}
