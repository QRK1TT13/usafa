//
// Created by C21Hunter.Manter on 9/4/2018.
//

#ifndef MYEXE_LAB09FUNCTS_H
#define MYEXE_LAB09FUNCTS_H
double volumeCylinder(double radius, double height);
double volumeBox(double depth, double width, double height);
double degToRad(double degrees);

#endif //MYEXE_LAB09FUNCTS_H
