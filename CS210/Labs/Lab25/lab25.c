//
// Created by C21Hunter.Manter on 10/22/2018.
//
#include "lab25.h"

int getNumLines(char filename[])
{
    char c;
    int count = 0;
    // Open the file
    FILE* fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL)
    {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;
}
void readFile(char filename[], USAFBaseData** baseStruct, int lines)
{
    FILE* fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL)
    {
        printf("Could not open file %s", filename);
        exit(1);
    }
    fscanf(fp,"%*[^\n] %*c");
    for(int i = 0;i<lines;i++)
    {
        fscanf(fp,"%[^,], %d,%d,%[^,],%[^\n]%*c",baseStruct[i]->baseName,&baseStruct[i]->bldgsOwned,&baseStruct[i]->structsOwned,baseStruct[i]->city,baseStruct[i]->state);
    }
    fclose(fp);
}
void printData(USAFBaseData** bases, int numBases)
{
    for(int i = 0;i<numBases;i++)
    {
        printf("%s,%d,%d,%s,%s\n\n",bases[i]->baseName,bases[i]->bldgsOwned,bases[i]->structsOwned,bases[i]->city,bases[i]->state);
    }
}
void getPoints(Point* one,  Point* two)
{
    printf("Enter x y\n");
    scanf("%d %d",&one->x,&one->y);
    printf("Enter x y\n");
    scanf("%d %d",&two->x,&two->y);
}
double getDistance(Point one,  Point two)
{
    return sqrt(pow((double)one.x-two.x,2.0)+pow((double)one.y-two.y,2.0));
}
int main()
{
    //Point* one = malloc(sizeof(Point));
    //Point* two = malloc(sizeof(Point));
    //getPoints(one,two);
    //printf("Distance: %f\n",getDistance(*one,*two));
    printf("Hi");
    int num = getNumLines("lab25Data.csv");
    printf("%d\n",num);
    USAFBaseData** baseDataPtr=malloc(num*sizeof(USAFBaseData));
    for (int i = 0;i<num;i++)
    {
        baseDataPtr[i]=malloc(sizeof(USAFBaseData));
    }
    printf("Hi");
    readFile("lab25Data.csv",baseDataPtr,num);
    printf("Hi");
    printData(baseDataPtr,num);
    return 1;
}