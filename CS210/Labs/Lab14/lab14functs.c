#include "lab14functs.h"

int findMaxIndex(int array[], int numPlayers)
{
    int max=0;
    int index = 0;
    for(int i = 0; i<numPlayers;i++)
    {
        if(array[i]>max)
        {
            max = array[i];
            index=i;
        }
    }
    return index;
}
int numPlayersOverX(int array[], int numPlayers, int num)
{
    int counter=0;
    for (int i = 0;i<numPlayers;i++)
    {
        if(array[i]>num)
        {
            counter++;
        }
    }
    return counter;
}
int getMaxYardsPerCarryIndex(int attempts[],int yards[], int numPlayers, int num)
{
    double maxy=0;
    int maxi=0;
    for(int i = 0;i<numPlayers;i++)
    {
        if(attempts[i]>num)
        {
            if(maxy<(double)yards[i]/attempts[i])
            {
                maxy=(double)yards[i]/attempts[i];
                maxi=i;
            }
        }
    }
    return maxi;

}
double getMaxYardsPerCarry(int attempts[], int yards[], int numPlayers, int num)
{
    double maxy=0;
    for(int i = 0;i<numPlayers;i++)
    {
        if(attempts[i]>num)
        {
            if(maxy<(double)yards[i]/attempts[i])
            {
                maxy=(double)yards[i]/attempts[i];
            }
        }
    }
    return maxy ;
}