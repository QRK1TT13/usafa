//
// Created by C21Hunter.Manter on 9/25/2018.
//

#ifndef MYEXE_LAB14FUNCTS_H
#define MYEXE_LAB14FUNCTS_H

#include <stdio.h>
#include "lab14fbFuncts.h"

int findMaxIndex(int array[], int numPlayers);
int numPlayersOverX(int array[], int numPlayers, int num);
int getMaxYardsPerCarryIndex(int attempts[],int yards[], int numPlayers, int num);
double getMaxYardsPerCarry(int attempts[], int yards[], int numPlayers, int num);
#endif //MYEXE_LAB14FUNCTS_H
