#include "lab14functs.h"
#include "lab14fbFuncts.h"

int main()
{
    int attempts[MAXPLAYERS], yards[MAXPLAYERS], touchdowns[MAXPLAYERS];
    int numPlayers =readData(attempts,yards,touchdowns);
    printf("%d players data read.\n",numPlayers);
    printf("The player ID %d is %s and he scored %d touchdowns in 2017.\n",0,getPlayerName(0),touchdowns[0]);
    printf("The player ID %d is %s and he rushed for %d yards in 2017.\n",122,getPlayerName(122),yards[122]);

    int maxYards = findMaxIndex(yards,numPlayers);
    int maxTouchdowns = findMaxIndex(touchdowns,numPlayers);
    int touchdownsOver = numPlayersOverX(touchdowns,numPlayers,10);
    int over1000 = numPlayersOverX(yards,numPlayers, 1000);
    int overAttempt = numPlayersOverX(attempts, numPlayers,100);
    int maxyardpercarry = getMaxYardsPerCarryIndex(attempts,yards,numPlayers,100);
    int maxYardPerCarry2=getMaxYardsPerCarryIndex(attempts,yards,numPlayers,50);
    int maxYardPerCarry3=getMaxYardsPerCarryIndex(attempts,yards,numPlayers,10);

    printf("The player with the most yards in 2017 is %s and he had %d.\n", getPlayerName(maxYards), yards[maxYards]);
    printf("The player with the most TDs in 2017 is %s and he had %d.\n", getPlayerName(maxTouchdowns),touchdowns[maxTouchdowns]);
    printf("The number of players with more than 10 TDs in 2017 is %d.\n", touchdownsOver);
    printf("The number of players with more than 1000 yards in 2017 is %d.\n",over1000);
    printf("The number of players with more than 100 attempts in 2017 is %d.\n",overAttempt);
    printf("The player with the most yards per carry (>100 attempts) is %s and he had %.1lf.\n", getPlayerName(maxyardpercarry),getMaxYardsPerCarry(attempts,yards,numPlayers,100));
    printf("The player with the most yards per carry (>50 attempts) is %s and he had %.1lf.\n", getPlayerName(maxYardPerCarry2),getMaxYardsPerCarry(attempts,yards,numPlayers,50));
    printf("The player with the most yards per carry (>10 attempts) is %s and he had %.1lf.\n",getPlayerName(maxYardPerCarry3),getMaxYardsPerCarry(attempts,yards,numPlayers,10));

    return 0;
}