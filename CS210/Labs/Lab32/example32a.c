//
// Created by John.Cox on 10/30/2018.
//

#include <stdio.h>

void print_str(char str[]);
int how_many_bits(int N);

int main() {

    // Example 1
    printf("\nExample 1: print a string recursively\n\n");

    // print a string recursively
    print_str("The quick brown fox jumps over the lazy dog.\n");


    // Example 2
    printf("\nExample 2: count how many bits required to for N items\n\n");

    // count how many bits required to for N items
    for (int i = 2; i <= 16; ++i) {
        printf("%2d items takes %d bits\n", i,how_many_bits(i));

    }

    for (int i = 1; i <= 20; ++i) {
        printf("%d / %d = %d\n", i, 10, (i+9) / 10);
    }

    return 0;
}

void print_str(char str[]) {

    // Base case
    if (*str == '\0') {
        return;
    }

    // Print 1 character
    printf("%c", *str);

    // Recursive case
    print_str(str + 1); // return optional since void
}

int how_many_bits(int N) {

    // Base case
    if (N == 1) {
        return 0;
    }

    // Recursive case - fun fact (numer + (denom - 1)) / denom
    // can be used to round up rather than truncating int division
    return 1 + how_many_bits((N + 1) / 2);
}