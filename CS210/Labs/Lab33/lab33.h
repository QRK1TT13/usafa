/** lab33.h
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Project: Lab 33
* Documentation: DOC STATEMENT
* ===========================================================  */

#ifndef MYEXE_LAB33_H
#define MYEXE_LAB33_H

#include <stdio.h>
#include <string.h>
#define DIRNAME "../Labs/Lab33/password/"

/** ----------------------------------------------------------
 * @fn int factorial(int N)
 * @brief Recursively calculates the factorial of N
 * @param N, the input parameter
 * @return N!, the value of the factorial of N
 * ----------------------------------------------------------
 */
int factorial(int N);
int tail_factorial(int input, int accum);
void decode_password(char *pass);

#endif //MYEXE_LAB33_H
