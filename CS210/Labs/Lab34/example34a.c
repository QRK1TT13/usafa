//
// Created by John.Cox on 11/3/2018.
//

#include <stdio.h>

void print_bits(char[], uint8_t);

int main() {

    uint8_t x = 10;
    uint8_t y = 15;
    uint8_t z = 240;

    // Bit Ops Examples
    print_bits("    x", x);
    print_bits("    y", y);
    print_bits("x & y", x&y);

    printf("\n");

    print_bits("    x", x);
    print_bits("    y", y);
    print_bits("x | y", x|y);

    printf("\n");

    print_bits("    x", x);
    print_bits("    y", y);
    print_bits("x ^ y", x^y);

    printf("\n");

    print_bits("     y", y);
    print_bits("    ~y",~y);
    print_bits("y + ~y", y + ~y);

    printf("\n");

    // Bit Shift Examples
    print_bits("     x", x);
    print_bits("x << 2", x<<2);
    print_bits("x >> 2", x>>2);

    printf("\n");

    print_bits("     z", z);
    print_bits("z << 2", z<<2);
    print_bits("z >> 2", z>>2);

    return 0;
}

void print_bits(char var_name[], uint8_t x) {

    // Print the variable name
    printf("%s = ", var_name);

    // Print each bit, starting with left most bit
    for (int i = sizeof(x) * 8 - 1; i >=0; --i) {
        printf("%c", (x >> i) & 1 ? '1':'0');

    }

    // Print decimal value of the variable
    printf(" (%u)\n", x);
}