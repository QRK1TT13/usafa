//
// Created by C21Hunter.Manter on 9/12/2018.
//

#ifndef MYEXE_LAB12_H
#define MYEXE_LAB12_H
#include <math.h>
#include <stdio.h>
int isArmstrong(int num);
int findGCD(int one, int two);
void printRange(int start, int finish);
#endif //MYEXE_LAB12_H
