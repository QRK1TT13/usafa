//
// Created by C21Hunter.Manter on 9/12/2018.
//
#include "lab12.h"
int main()
{
int arm;
    scanf("%d",&arm );
    printf("%d\n", isArmstrong(arm));
    int one, two;
    scanf("%d%d", &one, &two);
    printf("%d\n", findGCD(one, two));
    int start, finish;
    scanf("%d%d", &start, &finish);
    printRange(start,finish);
    return 0;
}
int isArmstrong( int num)
{
    int ones = num%10;
    int tens = num%100/10;
    int hund = num /100;
    return (num==pow(ones,3)+pow(tens, 3)+pow(hund,3));
}
int findGCD(int one, int two)
{
    for (int i = one; i>0;i--)
    {
        if(one%i==0&&two%i==0)
        {
            return i;
        }
    }
    return -1;
}
void printRange(int start, int finish)
{
    for(int i=start;i<=finish;i++)
    {
        printf("%d\n",i);
    }
}