//
// Created by C21Hunter.Manter on 10/17/2018.
//

/** ----------------------------------------------------------
 * @fn int getNumRecs(char dataFile[])
 * @brief Reads the number of records from the data file
 * @param dataFile is a string that indicates the path to and filename of the datafile
 * @return number of records in the file or -1 on error
 * ----------------------------------------------------------
 */
#include "lab23functs.h"
int getNumRecs(char dataFile[])
{
    FILE* inFile = fopen(dataFile,"r");
    if(inFile==NULL)
    {
        printf("Bad\n");
    }
    int result=0;
    fscanf(inFile,"%d",&result);
    fclose(inFile);
    return result;
}

/** ----------------------------------------------------------
 * @fn void getDataText(CadetInfoStructType cadetRecords[], int numRecs, char dataFile[])
 * @brief Reads CadetInfoStructType  records from a text file
 * @param cadetRecords is the array of cadet records
 * @param numRecs is the number of records in the file
 * @param dataFile is a string that indicates the path to and filename of the datafile
 * ----------------------------------------------------------
 */
void getDataText(CadetInfoStructType cadetRecords[], int numRecs, char dataFile[])
{
    FILE* inFile = fopen(dataFile,"r");
    if(inFile==NULL)
    {
        printf("Bad\n");
    }
    fseek(inFile,sizeof(int), SEEK_SET);
    char string[50];
    char string1[50];
    for(int i=0;i<numRecs;i++)
    {
        fscanf(inFile,"%s %s",string,string1);
        strcat(string," ");
        strcat(string,string1);
        strcpy(cadetRecords[i].name,string);
        fscanf(inFile,"%d",&cadetRecords[i].age);
        fscanf(inFile,"%d",&cadetRecords[i].squad);
        fscanf(inFile,"%d",&cadetRecords[i].year);
    }

    fclose(inFile);
}