//
// Created by C21Hunter.Manter on 10/17/2018.
//
#include "lab23functs.h"
int main()
{
    printf("Reading number of records in the data file.\n");
    printf("%d records in the data file.\n",getNumRecs("lab23data.txt"));
    printf("The first cadet is:\n");
    CadetInfoStructType *space= (CadetInfoStructType*)malloc(getNumRecs("lab23data.txt")*sizeof(CadetInfoStructType));
    getDataText(space,getNumRecs("lab23data.txt"),"lab23data.txt");
    printf("Cadet name = %s\n",space[0].name);
    printf("Cadet age = %d\n",space[0].age);
    printf("Cadet squad = %d\n",space[0].squad);
    printf("Cadet year = %d\n",space[0].year);
    printf("The last cadet is:\n");
    printf("Cadet name = %s\n",space[87].name);
    printf("Cadet age = %d\n",space[87].age);
    printf("Cadet squad = %d\n",space[87].squad);
    printf("Cadet year = %d\n",space[87].year);


    free(space);
    return 0;
}