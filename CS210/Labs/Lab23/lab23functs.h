//
// Created by C21Hunter.Manter on 10/17/2018.
//

#ifndef MYEXE_LAB23FUNCTS_H
#define MYEXE_LAB23FUNCTS_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct CadetInfoStruct{
    char name[50];
    int age;
    int squad;
    int year;
} CadetInfoStructType;

int getNumRecs(char dataFile[]);
void getDataText(CadetInfoStructType cadetRecords[], int numRecs, char dataFile[]);
#endif //MYEXE_LAB23FUNCTS_H
