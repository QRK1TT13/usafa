//
// Created by C21Hunter.Manter on 9/6/2018.
//
#include "lab10functs.h"
#include <stdbool.h>
#include <ctype.h>
#include <stdio.h>
int main()
{
    printf("Enter a single character to pass to isFirstHalfAlpha():\n");
    char cha = 'A';
    scanf(" %c", &cha);
    if(isFirstHalfAlpha(cha))
    {
        printf("A %c is in the first half of the alphabet.\n",cha);
    }
    else
    {
        printf("A %c is NOT in the first half of the alphabet.\n",cha);
    }

    printf("Enter a single character to pass to typeOfChar():\n");
    cha = 'A';
    scanf(" %c", &cha);
    if(typeOfChar(cha)==0)
    {
        printf("A %c is a vowel.\n",cha);
    }
    else if (typeOfChar(cha)==1)
    {
        printf("A %c is a consonant.\n",cha);
    }
    else
    {
        printf("A %c is neither a vowel nor a consonant.\n",cha);
    }

    printf("Enter a single character to pass to isMagicChar():\n");
    cha = 'A';
    scanf(" %c", &cha);
    if(isMagicChar(cha))
    {
        printf("A %c is the magic character @.\n",cha);
    }
    else
    {
        printf("A %c is NOT the magic character @.\n",cha);
    }
    return 0;
}