//
// Created by C21Hunter.Manter on 9/6/2018.
//
#include <stdbool.h>

#ifndef MYEXE_LAB10FUNCTS_H
#define MYEXE_LAB10FUNCTS_H
#define MAGIC_CHAR '@'

    bool isFirstHalfAlpha(unsigned char cha);
    int typeOfChar(unsigned char cha);
    bool isMagicChar(char cha);
#endif //MYEXE_LAB10FUNCTS_H
