//
// Created by C21Hunter.Manter on 9/6/2018.
//
#include "lab10functs.h"
#include <stdbool.h>
#include <ctype.h>
#include <stdio.h>
bool isMagicChar(char cha)
{
    return (cha==MAGIC_CHAR) ? true:false;
}
int typeOfChar(unsigned char cha)
{
    cha = tolower(cha);
    switch(cha)
    {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            return 0;
        case 'b':
        case 'c':
        case 'd':
        case 'f':
        case 'g':
        case 'h':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'v':
        case 'w':
        case 'x':
        case 'z':
            return 1;
        default:
            return -1;

    }
}
bool isFirstHalfAlpha(unsigned char cha)
{
    if(!isalpha(cha)){return 0;}
    return (tolower(cha)-109<=0) ? true:false ;
}