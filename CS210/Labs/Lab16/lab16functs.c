#include "lab16functs.h"

void captureOhms(double resVals[])
{
    for (int i = 0;i<NUM_RES;i++)
    {
        scanf("%lf", &resVals[i]);
    }
    printf("\n");
}
void calculateCurrent(double circVolt, double resVals[], double *currentVal)
{
    double sum =0;
    for (int i = 0;i<NUM_RES;i++)
    {
        sum+=resVals[i];
    }
    *currentVal=circVolt/sum;
}
void voltageDrop(double vDrop[], double resVals[], double currentVal)
{
    for (int i = 0;i<NUM_RES;i++)
    {
        vDrop[i]=currentVal*resVals[i];
    }
}
void printDrop(double vDrop[])
{
    for (int i = 0;i<NUM_RES;i++)
    {
        printf("%d) %.1lf V\n",i+1,vDrop[i]);
    }
}