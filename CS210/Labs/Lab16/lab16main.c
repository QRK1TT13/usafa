#include "lab16functs.h"
int main()
{
    double resVals[NUM_RES];
    double circVolt = 12;
    double vDrop[NUM_RES];
    double currentVal;

    captureOhms(resVals);
    calculateCurrent(circVolt,resVals,&currentVal);
    voltageDrop(vDrop,resVals,currentVal);
    printDrop(vDrop);

    return 0;
}