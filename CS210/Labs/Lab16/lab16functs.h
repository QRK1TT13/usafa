//
// Created by C21Hunter.Manter on 9/25/2018.
//

#ifndef MYEXE_LAB16FUNCTS_H
#define MYEXE_LAB16FUNCTS_H
#define NUM_RES 5
#include <stdio.h>

void captureOhms(double resVals[]);
void calculateCurrent(double circVolt, double resVals[], double *currentVal);
void voltageDrop(double vDrop[], double resVals[],double currentVal);
void printDrop(double vDrop[]);

#endif //MYEXE_LAB16FUNCTS_H
