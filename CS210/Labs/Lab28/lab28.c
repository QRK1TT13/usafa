/** lab28.c
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Project: Progamming Assessment 3 Practice
* Documentation: DOC STATEMENT
* ===========================================================  */
#include "lab28.h"
int main(void)
{
    // 1) Define a structure type, named BAHRecord, with appropriate members
    //   to store data from the O1_BAH_2018.txt file. Use the fields names:
    //   state, city, withDep, and withoutDep.

    // 2) Dynamically allocate memory to store the BAH data in the text file.
    printf("hi");
    int lines = getNumLines("/Users/hunter/Documents/JetBrains/CLion/usafa_cs210_f18_student/Labs/Lab28/O1_BAH_2018.txt");
    BAHRecord* baseDataPtr=malloc(lines*sizeof(BAHRecord));


    // 3) Create a user-defined function called readFile, which
    //   requires the filename, memory passed by reference, and
    //   the number of records to be passed as parameters (in that order).
    //   It returns the actual number of records read.
   printf("hi\n%d\n",lines);
   int read = readFile("/Users/hunter/Documents/JetBrains/CLion/usafa_cs210_f18_student/Labs/Lab28/O1_BAH_2018.txt",baseDataPtr,lines);
    printf("read: %d\n",read);
    // 4) Create a user-defined function called greatestRate to determine
    //   the city, state, and amount of the highest with-dependent rate.
    //   This function accepts the stored data, the number of records, and
    //   a pointer to a char array to capture the city and state (in that order).  
    //   It returns the greatest rate.

    char city[50];
    int most =greatestRate(baseDataPtr,lines,city);
    printf("%d",most);
    
    // 5) Create a user-defined function called leastRate to determine
    //   the city, state, and amount of the least without-dependent rate.
    //   This function accepts the stored data, the number of records, andl
    //   a pointer to a char array to capture the city and state (in that order).  
    //   It returns the least rate.
   char city2[50];
   int least = leastRate(baseDataPtr,lines,city2);
   printf("%d",least);
    // 6) Create a user-defined function called numStates to determine
    //   the number of states represented in the file.  This function accepts the
    //   stored data and the number of records (in that order).  It returns the 
    //   number of states.
    //   Assume that the entries are sorted alphabetically by state.
   int states = numStates(baseDataPtr,lines);
   printf("\n%d",states);

    // 7) Create a user-defined function called highRates to determine
    //   the number of cities with BAH rates (with or without dependent)
    //   above $1,000.  This function accepts the stored data and the number
    //   of records (in that order).  It returns the number of cities.
    int highCities = highRates(baseDataPtr,lines);
    printf("%d\n",highCities);

    // 8) Create a user-defined function called longestCity to determine
    //   the city with the longest name.  This function accepts the stored data 
    //   the number of records, and a pointer to a character array to capture the city.
    //   It returns a pointer to the character array storing the city name as a string.
    char city4[50];
    char city5[50];
    strcpy(city5,longestCity(baseDataPtr,lines, city4));

    // 9) Create a user-defined function called updateState to update the abbreviation
    //   for Colorado from "CO" to "Colorado" for each city/state pair.  Once these
    //   updates are complete, then write all the data to a file called "outputFile.txt".
    //   This function accepts the stored data and the number of records (in that order). 
    //   It does not have a return value.
   updateState(baseDataPtr,lines);

   return 0;
}


int getNumLines(char filename[])
{
   char c;
   int count = 0;
   // Open the file
   FILE* fp = fopen(filename, "r");

   // Check if file exists
   if (fp == NULL)
   {
      printf("Could not open file %s", filename);
      return 0;
   }

   // Extract characters from file and store in character c
   for (c = getc(fp); c != EOF; c = getc(fp))
      if (c == '\n') // Increment count if this character is newline
         count = count + 1;

   // Close the file
   fclose(fp);

   return count;
}
int readFile(char filename[], BAHRecord* baseStruct, int lines)
{
   FILE* fp = fopen(filename, "r");

   // Check if file exists
   if (fp == NULL)
   {
      printf("Could not open file %s", filename);
      exit(1);
   }
   int read=0;
   printf("Lines that should be read %d\n",lines);
   for(int i = 0;i<lines;i++)
   {
      fscanf(fp,"%[^,], %[^,],%lf,%lf,%*[^\n]",baseStruct[i].state,baseStruct[i].city,&baseStruct[i].withDep,&baseStruct[i].withoutDep);
      read++;
   }
   printf("sample : %s %s %lf %lf",baseStruct[300].state,baseStruct[300].city,baseStruct[300].withDep,baseStruct[300].withoutDep);
   fclose(fp);
   return read;
}
int greatestRate(BAHRecord* baseDataPtr,int lines,char city[])
{
   printf("test");
   int max = 0;
   for (int i =0;i<lines;i++)
   {
      if(baseDataPtr[i].withDep>baseDataPtr[max].withDep)
      {
         max=i;
      }
   }
   strcpy(city,baseDataPtr[max].city);
   strcat(city," ");
   strcat(city,baseDataPtr[max].state);
   return (int)baseDataPtr[max].withDep;
}
int leastRate(BAHRecord* baseDataPtr,int lines,char city[])
{
   int min = 0;
   for (int i =0;i<lines;i++)
   {
      if(baseDataPtr[i].withoutDep<baseDataPtr[min].withoutDep)
      {
         min=i;
      }
   }
   strcpy(city,baseDataPtr[min].city);
   strcat(city,baseDataPtr[min].state);
   return (int)baseDataPtr[min].withoutDep;
}
int numStates(BAHRecord* baseDataPtr,int lines)
{
   int num=1;
   char state[50];
   strcpy(state,baseDataPtr[0].state);
   for (int i =0;i<lines;i++)
   {
      if(strcmp(state,baseDataPtr[i].state)!=0)
      {
         strcpy(state,baseDataPtr[i].state);
         num++;
      }
   }
   return num;
}
int highRates(BAHRecord* baseDataPtr,int lines)
{
   int num =  0;
   for (int i =0;i<lines;i++)
   {
      if(baseDataPtr[i].withoutDep>1000||baseDataPtr[i].withDep>1000){num++;}
   }
   return num;
}
char* longestCity(BAHRecord* baseDataPtr,int lines,char city4[])
{
   int max = (int)strlen(baseDataPtr[0].city);
   strcpy(city4,baseDataPtr[0].city);
   int j;
   for (int i = 0;i<lines;i++)
   {
      if(strlen(baseDataPtr[i].city)>max)
      {
         j=i;
         max=(int)strlen(baseDataPtr[i].city);
         strcpy(city4,baseDataPtr[i].city);
      }
   }
   return baseDataPtr[j].city;
}
void updateState(BAHRecord* baseDatePtr, int lines)
{
   //FILE* inFile = fopen("outputFile.txt","rw");
   //if(inFile==NULL)
 //  {
  //    printf("Bad\n");
 //     exit(1);
  // }
   for(int i = 0;i<lines;i++)
   {
      if(strcmp(baseDatePtr[i].state,"AL")==0){
         strcpy(baseDatePtr[i].state, "Alabama");

      }else if(strcmp(baseDatePtr[i].state,"AK")==0){
         strcpy(baseDatePtr[i].state, "Alaska");

      }else if(strcmp(baseDatePtr[i].state,"AZ")==0){
         strcpy(baseDatePtr[i].state, "Arizona");

      }else if(strcmp(baseDatePtr[i].state,"AR")==0){
         strcpy(baseDatePtr[i].state, "Arkansas");

      }else if(strcmp(baseDatePtr[i].state,"CA")==0){
         strcpy(baseDatePtr[i].state, "California");

      }else if(strcmp(baseDatePtr[i].state,"CO")==0){
         strcpy(baseDatePtr[i].state, "Colorado");

      }else if(strcmp(baseDatePtr[i].state,"CT")==0){
         strcpy(baseDatePtr[i].state, "Connecticut");

      }else if(strcmp(baseDatePtr[i].state,"DE")==0){
         strcpy(baseDatePtr[i].state, "Delaware");

      }else if(strcmp(baseDatePtr[i].state,"DC")==0){
         strcpy(baseDatePtr[i].state, "District Of Columbia");

      }else if(strcmp(baseDatePtr[i].state,"FL")==0){
         strcpy(baseDatePtr[i].state, "Florida");

      }else if(strcmp(baseDatePtr[i].state,"GA")==0){
         strcpy(baseDatePtr[i].state, "Georgia");

      }else if(strcmp(baseDatePtr[i].state,"HI")==0){
         strcpy(baseDatePtr[i].state, "Hawaii");

      }else if(strcmp(baseDatePtr[i].state,"ID")==0){
         strcpy(baseDatePtr[i].state, "Idaho");

      }else if(strcmp(baseDatePtr[i].state,"IL")==0){
         strcpy(baseDatePtr[i].state, "Illinois");

      }else if(strcmp(baseDatePtr[i].state,"IN")==0){
         strcpy(baseDatePtr[i].state, "Indiana");

      }else if(strcmp(baseDatePtr[i].state,"IA")==0){
         strcpy(baseDatePtr[i].state, "Iowa");

      }else if(strcmp(baseDatePtr[i].state,"KS")==0){
         strcpy(baseDatePtr[i].state, "Kansas");

      }else if(strcmp(baseDatePtr[i].state,"KY")==0){
         strcpy(baseDatePtr[i].state, "Kentucky");

      }else if(strcmp(baseDatePtr[i].state,"LA")==0){
         strcpy(baseDatePtr[i].state, "Louisiana");

      }else if(strcmp(baseDatePtr[i].state,"ME")==0){
         strcpy(baseDatePtr[i].state, "Maine");

      }else if(strcmp(baseDatePtr[i].state,"MD")==0){
         strcpy(baseDatePtr[i].state, "Maryland");

      }else if(strcmp(baseDatePtr[i].state,"MA")==0){
         strcpy(baseDatePtr[i].state, "Massachusetts");

      }else if(strcmp(baseDatePtr[i].state,"MI")==0){
         strcpy(baseDatePtr[i].state, "Michigan");

      }else if(strcmp(baseDatePtr[i].state,"MN")==0){
         strcpy(baseDatePtr[i].state, "Minnesota");

      }else if(strcmp(baseDatePtr[i].state,"MS")==0){
         strcpy(baseDatePtr[i].state, "Mississippi");

      }else if(strcmp(baseDatePtr[i].state,"MO")==0){
         strcpy(baseDatePtr[i].state, "Missouri");

      }else if(strcmp(baseDatePtr[i].state,"MT")==0){
         strcpy(baseDatePtr[i].state, "Montana");

      }else if(strcmp(baseDatePtr[i].state,"NE")==0){
         strcpy(baseDatePtr[i].state, "Nebraska");

      }else if(strcmp(baseDatePtr[i].state,"NV")==0){
         strcpy(baseDatePtr[i].state, "Nevada");

      }else if(strcmp(baseDatePtr[i].state,"NH")==0){
         strcpy(baseDatePtr[i].state, "New Hampshire");

      }else if(strcmp(baseDatePtr[i].state,"NJ")==0){
         strcpy(baseDatePtr[i].state, "New Jersey");

      }else if(strcmp(baseDatePtr[i].state,"NM")==0){
         strcpy(baseDatePtr[i].state, "New Mexico");

      }else if(strcmp(baseDatePtr[i].state,"NY")==0){
         strcpy(baseDatePtr[i].state, "New York");

      }else if(strcmp(baseDatePtr[i].state,"NC")==0){
         strcpy(baseDatePtr[i].state, "North Carolina");

      }else if(strcmp(baseDatePtr[i].state,"ND")==0){
         strcpy(baseDatePtr[i].state, "North Dakota");

      }else if(strcmp(baseDatePtr[i].state,"OH")==0){
         strcpy(baseDatePtr[i].state, "Ohio");

      }else if(strcmp(baseDatePtr[i].state,"OK")==0){
         strcpy(baseDatePtr[i].state, "Oklahoma");

      }else if(strcmp(baseDatePtr[i].state,"OR")==0){
         strcpy(baseDatePtr[i].state, "Oregon");

      }else if(strcmp(baseDatePtr[i].state,"PA")==0){
         strcpy(baseDatePtr[i].state, "Pennsylvania");

      }else if(strcmp(baseDatePtr[i].state,"RI")==0){
         strcpy(baseDatePtr[i].state, "Rhode Island");

      }else if(strcmp(baseDatePtr[i].state,"SC")==0){
         strcpy(baseDatePtr[i].state, "South Carolina");

      }else if(strcmp(baseDatePtr[i].state,"SD")==0){
         strcpy(baseDatePtr[i].state, "South Dakota");

      }else if(strcmp(baseDatePtr[i].state,"TN")==0){
         strcpy(baseDatePtr[i].state, "Tennessee");

      }else if(strcmp(baseDatePtr[i].state,"TX")==0){
         strcpy(baseDatePtr[i].state, "Texas");

      }else if(strcmp(baseDatePtr[i].state,"UT")==0){
         strcpy(baseDatePtr[i].state, "Utah");

      }else if(strcmp(baseDatePtr[i].state,"VT")==0){
         strcpy(baseDatePtr[i].state, "Vermont");

      }else if(strcmp(baseDatePtr[i].state,"VA")==0){
         strcpy(baseDatePtr[i].state, "Virginia");

      }else if(strcmp(baseDatePtr[i].state,"WA")==0){
         strcpy(baseDatePtr[i].state, "Washington");

      }else if(strcmp(baseDatePtr[i].state,"WV")==0){
         strcpy(baseDatePtr[i].state, "West Virginia");

      }else if(strcmp(baseDatePtr[i].state,"WI")==0){
         strcpy(baseDatePtr[i].state, "Wisconsin");

      }else if(strcmp(baseDatePtr[i].state,"WY")==0) {
         strcpy(baseDatePtr[i].state, "Wyoming");
      }
   }
   //fwrite(&baseDatePtr,sizeof(BAHRecord),lines,inFile);
   //fclose(inFile);
}