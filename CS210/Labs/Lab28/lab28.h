/** lab28.h
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Project: Programming Assessment 3 Practice
* Documentation: DOC STATEMENT
* ===========================================================  */

#ifndef MYEXE_LAB28_H
#define MYEXE_LAB28_H

typedef struct BAHRecord_struct
{
    char state[50];
    char city[50];
    double withDep;
    double withoutDep;
}BAHRecord;
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int getNumLines(char filename[]);
int readFile(char filename[], BAHRecord* baseStruct, int lines);
int greatestRate(BAHRecord* baseDataPtr,int lines,char city[]);
int leastRate(BAHRecord* baseDataPtr,int lines,char city[]);
int numStates(BAHRecord* baseDataPtr,int lines);
int highRates(BAHRecord* baseDataPtr,int lines);
char* longestCity(BAHRecord* baseDataPtr,int lines,char city4[]);
void updateState(BAHRecord* baseDatePtr, int lines);

#endif //MYEXE_LAB28_H
