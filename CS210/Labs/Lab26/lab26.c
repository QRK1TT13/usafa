#include "lab26.h"
int getNumLines(char filename[])
{
    char c;
    int count = 0;
    // Open the file
    FILE* fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL)
    {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;
}
void readFile(char filename[], vector* vectorPtr, int records)
{
    FILE* fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL)
    {
        printf("Could not open file %s", filename);
        exit(1);
    }
    for(int i = 0;i<records;i++)
    {
        fscanf(fp,"%d%*c",vector_at(vectorPtr, i));
    }
    fclose(fp);
}


int main(void)
{
    int lines =getNumLines("/Users/hunter/Documents/JetBrains/CLion/usafa_cs210_f18_student/Labs/Lab26/lab26Data.csv");
    vector v;
    vector_create(&v,lines);
    readFile("/Users/hunter/Documents/JetBrains/CLion/usafa_cs210_f18_student/Labs/Lab26/lab26Data.csv",&v,lines);
   // With the functions provided in the Vector ADT,
   //   create a correctly sized vector to read-in all
   //   data from the lab26Data.csv file
   
   // Utilize your readFile() function from Lab 25
   //   to populate the vector -- you will need to make changes
   //   for this function to perform correctly
   

   // With the functions provided in the Vector ADT,
   //   print the number of vector elements
    printf("%d\n",vector_size(&v));
   // With the functions provided in the Vector ADT,
   //   insert a 12 at index 139
    vector_insert(&v,139,12);

   // With the functions provided in the Vector ADT,
   //   print the number of vector elements
    printf("%d\n",vector_size(&v));
    
   // With the functions provided in the Vector ADT,
   //   print the element at index 999
   unsigned int h = 999;
    printf("%d\n",*vector_at(&v,h));
    
   // With the functions provided in the Vector ADT,
   //   erase the element at index 999
    vector_erase(&v,999);

   // With the functions provided in the Vector ADT,
   //   print the element at index 999
    printf("%d\n",*vector_at(&v,h));

   // With the functions provided in the Vector ADT,
   //   print the number of vector elements
    printf("%d\n",vector_size(&v));

    // Using the Vector ADT provided function
    //   vector_push_back to insert the integer 222
   vector_push_back(&v,222);

   // With the functions provided in the Vector ADT,
   //   print the number of vector elementss
    printf("%d\n",vector_size(&v));

   // With the functions provided in the Vector ADT,
   //   destroy the vector
   vector_destroy(&v);
}