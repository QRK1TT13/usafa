#ifndef MYEXE_LAB26_H
#define MYEXE_LAB26_H
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "vector.h"
int getNumLines(char filename[]);
void readFile(char filename[], vector* vectorPtr, int records);

#endif //MYEXE_LAB26_H