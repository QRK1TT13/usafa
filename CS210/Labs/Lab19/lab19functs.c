//
// Created by C21Hunter.Manter on 10/2/2018.
//
#include "lab19functs.h"
void replStr(char str[], char aChar, char bChar)
{
    int length = strlen(str);
    for (int i =0;i<length;i++)
    {
        if (str[i]==aChar)
        {
            str[i]=bChar;
        }
    }
}
void replMultiChar(char str[], char searchChars[], char rChar )
{
    int length = strlen(str);
    int length2 = strlen(searchChars);
    for (int i =0;i<length;i++)
    {
        for(int j = 0; j<length2;j++)
        {
            if (str[i]==searchChars[j])
            {
                str[i]=rChar;
            }
        }
    }
}
int findLocations(char str[], int locations[], char searchChar)
{
    int length = strlen(str);
    int counter=0;
    for (int i =0;i<length;i++)
    {
        if (str[i]==searchChar)
        {
            locations[counter]=i;
            counter++;
        }
    }
    return counter;
}
int countSeqStr(char str[], char searchStr[])
{
    char ch = '\0';while ((ch = getchar()) != '\n' && ch != EOF);
    int length = strlen(str);
    int length2 = strlen(searchStr)-1;
    int point=0;
    int counter=0;
    for (int i =0;i<length-length2;i++)
    {
        for (int j=0;j<length2&&point==1;j++)
        {

            if(str[i+j]==searchStr[j])
            {
                if(j==length2-1)
                {
                    if(point==1)
                    {
                        counter++;
                    }
                }
            }
            else{point=0;}
        }
        point=1;
    }
    return counter-1;
}
