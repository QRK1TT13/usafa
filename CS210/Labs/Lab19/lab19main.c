//
// Created by C21Hunter.Manter on 10/2/2018.
//
#include "lab19functs.h"
int main()
{

    printf("Function - 1\n");
    printf("Enter a string (<= 80 chars): ");
    char data[MAX_STR_LEN];
    fgets(data,80,stdin);
    printf("\nEnter a search character followed by replace character: ");
    char char1;
    char char2;
    scanf(" %c %c",&char1,&char2);
    replStr(data,char1,char2);
    printf("\nNew string (between arrows): -->%s<--\n\n",data);
    char ch = '\0';while ((ch = getchar()) != '\n' && ch != EOF);

    printf("Function - 2\n");
    printf("Enter a string (<= 80 chars): ");
    char data2[MAX_STR_LEN];
    fgets(data2,80,stdin);
    printf("\nEnter a search string (<= 80 chars): ");
    char char3[MAX_STR_LEN];
    fgets(char3,80,stdin);
    printf("Enter a replace character: ");
    char char4;
    scanf(" %c",&char4);
    replMultiChar(data2,char3,char4);
    printf("\nNew string (between arrows): -->%s<--\n\n",data2);
    ch = '\0';while ((ch = getchar()) != '\n' && ch != EOF);

    printf("Function - 3\n");
    printf("Enter a string (<= 80 chars): ");
    char data3[MAX_STR_LEN];
    fgets(data3,80,stdin);
    printf("\nEnter a character whose locations you wish to find: ");
    char char5;
    scanf(" %c",&char5);
    int locations[MAX_STR_LEN];
    int count =findLocations(data3,locations,char5);
    printf("\nThere are %d occurrences of %c.\n",count,char5);
    for (int i =0;i<count;i++)
    {
        printf("%c found at location %d.\n",char5,locations[i]);
    }
    ch = '\0';while ((ch = getchar()) != '\n' && ch != EOF);

    printf("Function - Challenge\n");
    printf("Enter a string (<= 80 chars): ");
    char data4[MAX_STR_LEN];
    fgets(data4,80,stdin);
    printf("\nEnter a search string (<= 80 chars): ");
    char data5[MAX_STR_LEN];
    fgets(data5,80,stdin);
    int counted = countSeqStr(data4,data5);
    printf("There are %d occurrences of %s.",counted,data5);
    return 0;
}