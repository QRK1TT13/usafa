//
// Created by C21Hunter.Manter on 10/2/2018.
//

#ifndef MYEXE_LAB19FUNCTS_H
#define MYEXE_LAB19FUNCTS_H
#define MAX_STR_LEN 81
#include <string.h>
#include <stdio.h>
void replStr(char str[], char aChar, char bChar);
void replMultiChar(char str[], char searchChars[], char rChar );
int findLocations(char str[], int locations[], char searchChar);
int countSeqStr(char str[], char searchStr[]);


#endif //MYEXE_LAB19FUNCTS_H
