#include "lab13.h"

int main()
{
    printf("Please enter your class year:\n");
    printf("Please enter your favorite integer:\n");
    int a,b,e,f,h=0;
    long g =1;
    scanf("%d %d", &a,&b);
    swapPassByValue(a,b);
    printf("aNum = %d, bNum = %d\n", a,b);

    swapPassByReference(&a,&b);
    printf("aNum = %d, bNum = %d\n", a,b);

    scanf("%d %d", &e,&f);
    int sum=pointerSum(&a,&b);
    printf("The sum of %d and %d = %d\n", a,b, sum);

    scanf("%d", &h);
    findFactorial(h,&g);
    printf("The factorial of % is %ld\n", h,g);
    return 0;
}
void swapPassByValue(int a, int b)
{
    int temp = a;
    a=b;
    b=temp;
}
void swapPassByReference(int *a, int *b)
{
    int temp = *a;
    *a=*b;
    *b=temp;
}
int pointerSum(int *a, int *b)
{
    return *a+*b;
}
void findFactorial(int a, long *b)
{
    for(int i = 1;i<=a;i++)
    {
        *b*=i;
    }
}