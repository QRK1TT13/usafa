//
// Created by C21Hunter.Manter on 9/25/2018.
//

#ifndef MYEXE_LAB13_H
#define MYEXE_LAB13_H

#include <stdio.h>
void swapPassByValue(int a, int b);
void swapPassByReference(int *a, int *b);
int pointerSum(int *a, int *b);
void findFactorial(int a, long *b);
#endif //MYEXE_LAB13_H
