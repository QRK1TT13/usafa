/** lab36.h
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Documentation: YOUR DOC STATEMENT
* ===========================================================  */

#ifndef MYEXE_LAB36_H
#define MYEXE_LAB36_H

#include <stdio.h>
#include <strings.h>
int sum_squares(int N);
int is_even(int x);
int power_two(int N);
int reverse_bytes(unsigned int val);
int is_palindrome(char message[], int len);
int is_pal(char message[],int start ,int end);
void printAllStrings(char chars[],int len);

#endif //MYEXE_LAB36_H
