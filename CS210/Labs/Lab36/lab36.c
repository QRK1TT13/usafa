/** lab36.c
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Documentation: YOUR DOC STATEMENT
* ===========================================================  */

#include "lab36.h"

int main() {
    // Exercise 1
    printf("\nExercise 1: sum_squares()\n\n");
    printf("%d",sum_squares(4));
    
    
    // Exercise 2
    printf("\nExercise 2: is_even()\n\n");
    printf("%d",is_even(11));
    
    
    // Exercise 3
    printf("\nExercise 3: power_two()\n\n");
    printf("%d",power_two(0));
    
    
    // Exercise 4
    printf("\nExercise 4: reverse_bytes()\n\n");
    
    
    
    // Exercise 5
    printf("\nExercise 5: is_palindrome()\n\n");

    printf("\nExercise 6: allstrings()\n\n");
    char chars[4]={'a','b','c'};
   printAllStrings(chars,3);
    return 0;
}

int sum_squares(int x)
{
    if(x==1){return 1;}
    else
    {
        return x*x+sum_squares(x-1);
    }
}
int is_even(int x)
{
    return ~x&1;
}
int power_two(int N)
{
    return 1<<N;
}
int reverse_bytes(unsigned int val)
{
    unsigned int fin=0x00000000,help;
    for(int i = 0;i<4;i++)
    {
        help = val>>(i*8)&0xFF;
        fin|=help<<(24-i*8);
    }
    return fin;
}
int is_palindrome(char message[], int len)
{
    return is_pal(message,0,len-1)>=len/2;
}
int is_pal(char message[],int start ,int end)
{
    if(end-start<=1)
    {
        return 1;
    }
    return (message[start]==message[end])+is_pal(message,start+1,end-1);
}
void printAllStrings(char chars[],int len)
{
    if(len==1)
    {
        printf("%c\n",chars[0]);

    }else{
    for(int i = 0;i<len;i++)
    {
        printf("%c",chars[i]);
        printAllStrings(chars+1,len-1);

    }}
}
