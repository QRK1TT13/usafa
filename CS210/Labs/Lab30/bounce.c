//
// Created by Troy Weingart on 11/4/18.
//
/* Before yo begin you must add the following to your cmake
 *
 * After your add_executable() call

        # For curses.h
        target_link_libraries(myExe ${CURSES_LIBRARIES})
 *
 *
 * After you set compiler flags

        # Settings for use of curses.h
        find_package(curses REQUIRED)
        include_directories(${CURSES_INCLUDE_DIR})
        set(CMAKE_CXX_FLAGS -lcurses)
 *
 * Note:  For color support you need to change cygwin terminal
 * preferences (Terminal->Type->xterm-256).  A restart of the
 * terminal is required to enable.
 */
#include <unistd.h>
#include <stdlib.h>
#include "bounce.h"

int main() {

    // curses setup code must happen first
    setlocale(LC_ALL, ""); // for use of unicode characters
    initscr(); // Initialize the window
    start_color(); // enable color support
    use_default_colors(); // set default colors
    noecho(); // Don't echo any keypresses
    curs_set(FALSE); // Don't display a cursor
    cbreak();    // Line buffering disabled. pass on everything
    timeout(100); // Amount of time to wait for keyboard input
    keypad(stdscr, TRUE); // allows use of arrow keys

    // setup colors, colors come in foreground/background pairs
    // this code initializes pairs 0 to 255 and colors 0 to 255
    for (int i = 0; i < 256; i++) {
        init_pair(i, i, -1); // pair #, color, terminal background
    }

    // Write a intro message then delay and continue
    mvprintw(LINES/2,COLS/2-20,"Hit the 'x' key to exit...");
    refresh(); // Update the screen (without this the message wouldn't appear)
    sleep(1);

    // set up start position of the block (moves with arrow keys)
    int blockX = COLS / 2.0;
    int blockY = LINES / 2.0;

    // allocate an array of BallType structs to hold ball info
    int numBalls = rand() % 10 + 20;
    BallType *ballsPtr = malloc(numBalls * sizeof(BallType));

    // initialize each ball struct
    for (int i = 0; i < numBalls; i++) {
        ballsPtr[i].x = rand() % (COLS-2) + 1;
        ballsPtr[i].y = rand() % (LINES-2) + 1;
        ballsPtr[i].color = rand() % 256;
        ballsPtr[i].dirX = rand() % 4 - 2;
        ballsPtr[i].dirY = rand() % 4 - 2;
    }


    // initialize variable to hold keyboard input
    int ch = '\0';

    // start the animation loop
    while (ch != 'x') {
        // get character input, note: this call will timeout based on value
        // set in the timeout() call above. Without timeout it will block
        // and cause your code to halt until a until key is hit
        ch = getch();

        // erase terminal window
        clear();

        // draw a border with all default values around the terminal window
        border(0, 0, 0, 0, 0, 0, 0, 0);

        // draw balls
        for (int i = 0; i < numBalls; i++) {
            // set the color to the ball color
            attron(COLOR_PAIR(ballsPtr[i].color));
            // use the unicode smiley face symbol and write it to screen
            mvprintw(ballsPtr[i].y, ballsPtr[i].x, "\u263A");
        }

        // draw block that moves with keyboard input, the unicode for the
        // block is used
        mvprintw(blockY, blockX, "\u2588");

        // move balls (
        for (int i = 0; i < numBalls; i++) {
            ballsPtr[i].x += ballsPtr[i].dirX;
            ballsPtr[i].y += ballsPtr[i].dirY;
        }

        // process keys (for block in center to move), arrow keys will not
        // work with you keypad() call above, KEY_XXX are defined as part of
        // curses
        switch (ch) {
            case KEY_UP: {
                blockY--;
                break;
            }
            case KEY_DOWN: {
                blockY++;
                break;
            }
            case KEY_LEFT: {
                blockX--;
                break;
            }
            case KEY_RIGHT: {
                blockX++;
                break;
            }
        }

        // check for collision
        for (int i = 0; i < numBalls; i++) {
            if (ballsPtr[i].x >= COLS - 2 || ballsPtr[i].x <= 1) {
                ballsPtr[i].dirX *= -1;
            }
            if (ballsPtr[i].y >= LINES - 2 || ballsPtr[i].y <= 1) {
                ballsPtr[i].dirY *= -1;
            }
        }

        // update window and delay before erase
        refresh();  // update the terminal window with all drawn
        usleep(DELAY); // sleep a short time to allow balls to be seen before redrawn

    }

    endwin(); // Restore normal terminal behavior
}