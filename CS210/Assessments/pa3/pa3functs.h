//
// Name:  Hunter Manter
//

#ifndef MYEXE_PA3FUNCTS_H
#define MYEXE_PA3FUNCTS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct{
    char operAirline[50];
    char geoRegion[30];
    char acType[30];
    char acBody[30];
    char acManu[30];
    char acMod[30];
    char acVer[30];
    int landCount;
    int weight;
}AirCraftType;
int getNumLines(char filename[]);
int readFile(char filename[], AirCraftType* aircraft, int lines);
int findIndexMostLandings(AirCraftType* aircraft,int lines);
AirCraftType* findPtrMostLandings(AirCraftType* aircraft, int lines);
int countType(AirCraftType* aircraft, int lines, char* type);
void updateRegion(AirCraftType* aircraft, int lines);
int countLetter(AirCraftType* aircraft, int lines, char letter);
int uniqueAirlines(AirCraftType* aircraft, int lines);


#endif //MYEXE_PA3FUNCTS_H