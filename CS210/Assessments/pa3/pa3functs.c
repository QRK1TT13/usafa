//
// Name:Hunter Manter
//
#include "pa3functs.h"

int getNumLines(char filename[])
{
    char c;
    int count = 0;
    // Open the file
    FILE* fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL)
    {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;
}

int readFile(char filename[], AirCraftType* aircraft, int lines)
{
    FILE* fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL)
    {
        printf("Could not open file %s", filename);
        return 0;
    }
    int read=0;
    printf("Lines that should be read %d\n",lines);
    for(int i = 0;i<lines;i++)
    {
        fscanf(fp,"%[^,], %[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%d,%d\n",aircraft[i].operAirline,aircraft[i].geoRegion,aircraft[i].acType,aircraft[i].acBody,aircraft[i].acManu,aircraft[i].acMod,aircraft[i].acVer,&aircraft[i].landCount,&aircraft[i].weight);
        read++;
    }
    printf("sample : %s, %s,%s,%s,%s,%s,%s,%d,%d,\n",aircraft[10].operAirline,aircraft[10].geoRegion,aircraft[10].acType,aircraft[10].acBody,aircraft[10].acManu,aircraft[10].acMod,aircraft[10].acVer,aircraft[10].landCount,aircraft[10].weight);
    fclose(fp);
    return read;
}

int findIndexMostLandings(AirCraftType* aircraft,int lines)
{
    int index=0;
    int max = aircraft[0].landCount;
    for(int i =0;i<lines;i++)
    {
        if(aircraft[i].landCount>max)
        {
            index=i;
            max=aircraft[i].landCount;
        }
    }
    return index;
}

AirCraftType* findPtrMostLandings(AirCraftType* aircraft, int lines)
{
    int index=findIndexMostLandings(aircraft,lines);
    AirCraftType* mostLandings = &aircraft[index];
    return mostLandings;
}

int countType(AirCraftType* aircraft, int lines, char* type)
{
    int count=0;
    for(int i = 0; i<lines;i++)
    {
        if(strcmp(aircraft[i].acType,type)==0)
        {
            count++;
        }
    }
    return count;
}

void updateRegion(AirCraftType* aircraft, int lines)
{
    for(int i = 0; i<lines;i++)
    {
        if(strcmp(aircraft[i].geoRegion,"Mexico")==0)
        {
            strcpy(aircraft[i].geoRegion,"MX");
        }
    }
}

int countLetter(AirCraftType* aircraft, int lines, char letter)
{
    char letter2 = toupper(letter);
    int count=0;
    for(int i =0;i<lines;i++)
    {
        for(int j =0;j<strlen(aircraft[i].operAirline);j++)
        {
            if(aircraft[i].operAirline[j]==letter||aircraft[i].operAirline[j]==letter2)
            {
                count++;
            }
        }
    }
    return count;
}

int uniqueAirlines(AirCraftType* aircraft, int lines)
{
    int num=1;
    char state[lines][200];
    int index=1;
    int bunk=0;
    strcpy(state[0],aircraft[0].operAirline);
    for (int i =0;i<lines;i++)
    {
        bunk=0;
        for(int j = 0;j<index;j++)
        {
            if(strcmp(state[j],aircraft[i].operAirline)==0)
            {
                bunk=1;
            }
        }
        if(bunk==0)
        {
            num++;
            strcpy(state[index],aircraft[i].operAirline);
            index++;
        }
    }
    return num;
}