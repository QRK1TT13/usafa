//
// Name: Hunter Manter
//
#include "pa3functs.h"
int main() {
    char file[100]="/Users/hunter/Documents/JetBrains/CLion/usafa_cs210_f18_student/Assessments/pa3/dataFile.csv";
    int lines = getNumLines(file);
    printf("Lines: %d\n",lines);
    AirCraftType* aircraft = malloc(lines*sizeof(AirCraftType));
    readFile(file,aircraft,lines);
    int mostLandingsIndex = findIndexMostLandings(aircraft,lines);
    printf("Index of most landings: %d\n",mostLandingsIndex);
    AirCraftType* mostLandings = findPtrMostLandings(aircraft,lines);
    printf("Most Landings : %s, %s,%s,%s,%s,%s,%s,%d,%d,\n",mostLandings->operAirline,mostLandings->geoRegion,mostLandings->acType,mostLandings->acBody,mostLandings->acManu,mostLandings->acMod,mostLandings->acVer,mostLandings->landCount,mostLandings->weight);
    int typeCount = countType(aircraft,lines,"Passenger");
    printf("Num of type: %d\n",typeCount);
    char letter = 's';
    int cntLetter = countLetter(aircraft,lines,letter);
    printf("num of letter: %d\n",cntLetter);
    int uniq=uniqueAirlines(aircraft,lines);
    printf("uniqe: %d\n",uniq);

    return 0;

}