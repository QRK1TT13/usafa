/** pa4functs.c
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* ===========================================================  */

#include "pa4functs.h"

int nth_odd(int N)
{
    if(N==1)
    {
        return 1;
    }
    else
        {
        return 2+nth_odd(N-1);
        }
}
int almost_fibonacci(int N)
{
    if(N==1)
    {
        return 3;
    }
    else if(N==2)
    {
        return  2;
    }
    else{return almost_fibonacci(N-1)+almost_fibonacci(N-2);}
}
int alternating_cubes(int N)
{

    if(N==1)
    {
        return -1;
    }
    else if(N%2==0)
    {

        return (N*N*N) + alternating_cubes(N-1);
    }
    else if(N%2==1)
        {
        return -N*N*N + alternating_cubes(N-1);
        }
        else{printf("%d\n",N);return -1;}
}
int is_negative(int x)
{
    return x>>23&1;
}
int halve(int x)
{
    //print_bits(&x, sizeof(x));
    int last =0;
    unsigned int v = 1;
    for(int i = 1;i<32;i++)
    {

        if(x&v){last=i;}
        v=v<<1;
    }
    int fin = x>>1;
    //print_bits(&fin, sizeof(x));
    return fin;
}
int set_bit(int val, int N, int bitVal)
{
    unsigned int fin=val;
    if(bitVal==1&&(bitVal&1<<(N))==0){fin= fin|(1<<(N));}
    else if(bitVal==0&&(bitVal&1<<(N)))
        {fin=fin^(1<<N);}
    return fin;
}
int quick_hash(char message[])
{
    unsigned int fin=0;
    for(int i = 0;i<strlen(message);i++)
    {
        fin=fin+message[i];
        fin = fin+(fin<<10);
        fin = fin^(fin>>6);
    }
    fin = fin + (fin<<3);
    fin = fin ^(fin >>11);
    fin = fin +(fin<<15);



    return fin;
}
void print_bits(void* ptr, int num_bytes) {
    // Cast the pointer as an unsigned byte
    uint8_t* byte = ptr;

    // For each byte, (bytes are little endian ordered)
    for (int i = num_bytes - 1; i >= 0; --i) {

        // For each bit, (inside the byte, bits are big endian ordered)
        for (int j = 7; j >= 0; --j) {

            // Print a character 1 or 0, given the bit value
            printf("%c", (byte[i] >> j) & 1 ? '1' : '0');
        }

        // Separate bytes
        printf(" ");
    }

    // End with a new line
    printf("\n");
}