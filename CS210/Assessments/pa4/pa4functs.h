/** pa4functs.h
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* ===========================================================  */

#ifndef MYEXE_PA4FUNCTS_H
#define MYEXE_PA4FUNCTS_H

#include <stdint.h>
#include <stdio.h>
#include <strings.h>
int nth_odd(int N);
int almost_fibonacci(int N);
int alternating_cubes(int N);
int is_negative(int x);
int halve(int x);
void print_bits(void* ptr, int num_bytes);
int set_bit(int val, int N, int bitVal);
int quick_hash(char message[]);
#endif //MYEXE_PA4FUNCTS_H
