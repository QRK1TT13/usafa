/** pa4main.c
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* ===========================================================  */

#include "pa4functs.h"

int main() {
    unsigned int h=0x10FF6748;
    print_bits(&h, sizeof(h));
    set_bit(h,3,0);
    print_bits(&h, sizeof(h));
    
    
    return 0;
}