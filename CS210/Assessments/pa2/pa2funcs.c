#include "pa2funcs.h"

void reverseDigits(int num)
{
    int tmp;
    do
        {
            tmp=num%10;
            printf("%d",tmp);
            num/=10;
        }
        while(num>0);
        printf("\n");
}
int performOperation(char c, int num, int num2)
{
    switch (c)
    {
        case '/':
            return num/num2;
        case '*':
            return num*num2;
        case '+':
            return num+num2;
        case '-':
            return num-num2;
        default:
            return 0;
    }
}
int perfectAI(char data[])
{
    if (data[0]=='r')
    {
        return 1;
    }
    else if (data[0]=='p')
    {
        return 2;
    }
    else if (data[0]=='s')
    {
        return 0;
    }
    else
    {
        return -1;
    }
}
void getMinMax(int data[], int num, int *min, int *max)
{
    *min = data[0];
    *max = data[0];
    for (int i=0;i<num;i++)
    {
        if(data[i]>*max)
        {
            *max=data[i];
        }
        if(data[i]<*min)
        {
            *min=data[i];
        }
    }
}
int checkPerfect(int num)
{
    int div[num];
    for(int i =0;i<num;i++){div[i]=0;}
    int j=0;
    for (int i =1; i<num;i++)
    {
        if(num%i==0)
        {
            div[j]=i;
            j++;
        }
    }
    int sum=0;
    for(int i =0;i<num;i++)
    {
        if(div[i]>0 && div[i]<num)
        {
            sum+=div[i];
        }
    }
    if(sum==num){return 1;}
    else{return 0;}
}
int perfectNumbers(int start, int finish, int array[])
{
    int j =0;
    for (int i=start;i<=finish;i++)
    {
        if(checkPerfect(i))
        {
            array[j]=i;
            j++;
        }
    }
    return j;
}
void fill2D(int data[SIZE][SIZE])
{
    for(int i =0;i<SIZE;i++)
    {
        for(int j = 0;j<SIZE;j++)
        {
            data[i][j]=rand()*999+1;
        }
    }
}
int count2D(int array[SIZE][SIZE])
{
    int total=0;
    for(int i =0;i<SIZE;i++)
    {
        for(int j = 0;j<SIZE;j++)
        {
            if(array[i][j]%3==0 && array[i][j]%5==0)
            {
                total++;
            }
        }
    }
    return total;
}

