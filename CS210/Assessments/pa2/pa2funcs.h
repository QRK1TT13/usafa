#ifndef MYEXE_PA2FUNCS_H
#define MYEXE_PA2FUNCS_H
#define SIZE 100
#include <stdio.h>
#include <stdlib.h>
void reverseDigits(int num);
int performOperation(char c, int num, int num2);
int perfectAI(char data[]);
void getMinMax(int data[], int num, int *min, int *max);
int checkPerfect(int num);
int perfectNumbers(int start, int finish, int array[]);
void fill2D(int data[SIZE][SIZE]);
int count2D(int array[SIZE][SIZE]);

#endif //MYEXE_PA2FUNCS_H
