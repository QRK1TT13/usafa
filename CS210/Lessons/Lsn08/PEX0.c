//
// Created by C21Hunter.Manter on 8/29/2018.
//
/** PEX0.c
* ==============================================================
* Name: Name, Date
* Section:
* Project: PEX 0 - ASCII Art
* Purpose: Understand how to use C standard library functions,
*          and user-defined functions.
* Documentation Statement:
* ==============================================================
* Instructions:
 * Complete and submit PEX0 as outlined in the writeup.
*/
#include <stdio.h>
#include <math.h>

void drawTree();
void drawCat();
double coursePercentage(double, double, double);
int main() {
    int choose;
    scanf("%d", &choose);
    if(choose==1)
    {
        drawTree();
    }
    else{
        drawCat();
    }
    double home;
    double mid;
    double final;
    scanf("%lf", &home);
    scanf("%lf", &mid);
    scanf("%lf", &final);
    double grade = coursePercentage(home, mid,final);
    printf("%lf\n", grade);
    return 0;
}
double coursePercentage(double homework, double midterm, double final)
{
    double total =100*(homework/80*.2+midterm/40*.3+final/70*.5);
    return total;
}
void drawTree()
{
    printf("   *\n");
    printf("  ***\n");
    printf(" *****\n");
    printf("*******\n");
    printf("  ***\n");
}
void drawCat()
{
    printf("/\\   /\\\n");
    printf("  o o\n");
    printf(" =   =\n");
    printf("  ---\n");
}