//
// Created by C21Hunter.Manter on 10/4/2018.
//
/*
 * Documentation:
 * None
 */
#include "pex2.h"
int main()
{
    printf("hamming = %d\n", hammingDistance("AAA", "CC")); // result= -1
    printf("hamming = %d\n", hammingDistance("ACCT", "ACCG")); //result= 1
    printf("hamming = %d\n", hammingDistance("ACGGT", "CCGTT")); //result= 2
    printf("simularity = %0.3f\n", similarityScore("CCC", "CCC")); // result= 1.0
    printf("simularity = %0.3f\n", similarityScore("ACCT", "ACCG")); // result= 0.75
    printf("simularity = %0.3f\n", similarityScore("ACGGT", "CCGTT")); // result= 0.6
    printf("simularity = %0.3f\n", similarityScore("CCGCCGCCGA", "CCTCCTCCTA")); // result= 0.7
    printf("# matches = %d\n", countMatches("CCGCCGCCGA", "TTT", 0.6)); // result= 0
    printf("# matches = %d\n", countMatches("CCGCCGCCGA", "CCG", 0.2)); // result= 8
    printf("# matches = %d\n", countMatches(MOUSEDNA, "CGCTT", 0.5)); // result= 36
    printf("# matches = %d\n", countMatches(HUMANDNA, "CGC", 0.3)); // result= 215
    printf("best match = %0.3f\n", findBestMatch("CCGCCGCCGA", "TTT")); // result= 0.0
    printf("best match = %0.3f\n", findBestMatch("CTGCCACCAA", "CCGC")); // result= 0.75
    printf("best match = %0.3f\n", findBestMatch(MOUSEDNA, "CGCTT")); // result= 0.8
    printf("best match = %0.3f\n", findBestMatch(HUMANDNA, "CGC")); // result= 1.0
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAGGCCGGTT")); //result= 1
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAGGCCGGG")); //result= 2
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTAATTCTTTT")); //result= 3
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAG")); //result= 0
    return 0;
}
int hammingDistance(char* str1, char* str2)
{
    //we can only compare same length strings
    if(strlen(str1)!=strlen(str2))
    {
        return -1;
    }
    int count =0;
    //goes through each eltter in both strings and compares them
    for ( int i =0; i<strlen(str1);i++)
    {
        if(str1[i]!=str2[i])
        {
            count++;
        }
    }
    return count;
}
float similarityScore(char* seq1, char* seq2)
{
    //(sequenceLength - hammingDistance) / sequenceLength
    return (strlen(seq1)-hammingDistance(seq1,seq2))/(double)strlen(seq1);
}
int countMatches(char* genome, char* seq, float minScore)
{
    int result=0;
    //gets length of both strings
    int length = strlen(genome);
    int length2 = strlen(seq);
    //place to store scores
    double counter[length];
    double stat=0;
    //goes through each character of big string
    for(int i=0; i<length;i++)
    {
        stat=0;
        //goes through each letter of smalller string
        for (int j=0;j<length2;j++)
        {
            if(i+j<length)
            {
                //checks if characters are the same in both strings while the biger is offset by character we are on (i)
                if(genome[i+j]==seq[j])
                {
                    stat+=1/(double)length2;
                }
            }
        }
        counter[i]=stat;
    }
    //counts how many scores are above the limit
    for (int i=0;i<length;i++)
    {
        if(counter[i]>minScore)
        {
            result++;
        }
    }
    return result;
}
float findBestMatch(char* genome, char* seq)
{
    float result=0;
    //gets length of both strings
    int length = strlen(genome);
    int length2 = strlen(seq);
    //place to store scores
    double counter[length];
    double stat=0;
    //goes through each character of big string
    for(int i=0; i<length;i++)
    {
        stat=0;
        //goes through each letter of smalller string
        for (int j=0;j<length2;j++)
        {
            if(i+j<length)
            {
                if(genome[i+j]==seq[j])
                {
                    //checks if characters are the same in both strings while the biger is offset by character we are on (i)
                    stat+=1/(double)length2;
                }
            }
        }
        counter[i]=stat;
    }
    result=counter[1];
    //counts how many scores are above the limit
    for (int i=0;i<length;i++)
    {
        if(counter[i]>result)
        {
            result=counter[i];
        }
    }
    return result;
}
int findBestGenome(char* genome1, char* genome2, char* genome3, char* unknownSeq)
{
    //gets scores of each genome and unknownSEQ
    float g1 = findBestMatch(genome1, unknownSeq);
    float g2 = findBestMatch(genome2, unknownSeq);
    float g3 = findBestMatch(genome3, unknownSeq);
    //returns which number is the highest
    if(g1>g2 &&g1>g3){return 1;}
    else if(g2>g1 &&g2>g3){return 2;}
    else if(g3>g2 &&g3>g1){return 3;}
    else {return 0;}
}