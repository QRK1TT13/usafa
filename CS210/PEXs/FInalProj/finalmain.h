//
// Created by Hunter Manter on 11/5/18.
//Documentation: referenced https://www.thonky.com/qr-code-tutorial/ to understand what to do. There is no code on website so it was just learning the process

#ifndef MYEXE_FINALMAIN_H
#define MYEXE_FINALMAIN_H

#include <stdio.h>
#include <strings.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
    char input[25];//138 bits
    char modeIndicator[5];
    char encodedData[152];//152 allowed in 1-Q
    int encodedLength;
    char charCountIndicator[9];
    char totalData[300];
    int array[21][21];
    int mask;
} qr;

void getInput(qr *qr);

int getAlphNumVal(char let);

void dec2bin(char *charsInBin, int c);

void addBuffer(char *charsInBinary, int bufferAmt);

void addBufferEnd(char *charsInBinary, int bufferAmt);

void lengthOfEnco(qr *qr);

void makeMultEight(qr *qr);

void encode(qr *qr);

void addTerminator(qr *qr);

void combine(qr *qr);

void addPadBytes(qr *qr);

void printCode(qr *qr);

void addErrorCorrectionCodewords(qr *qr);

void placeEncodedData(qr *qr);

void addMask(qr *qr);

void coefficientsMessagePolynomial(qr *qr, unsigned int *coef);

void coefToAlph(unsigned int *messageCoef, unsigned int *messageAlph);

void alphToCoef(unsigned int *generatorAlph, unsigned int *generatorCoef);

unsigned int bin2Dec(char *str);

unsigned int alphToNum(unsigned int num);

unsigned int numToAlph(unsigned int alph);

void unsignedMod(unsigned int *generatorAlph);

void flipBit(int a, int b, qr *qr);

#endif //MYEXE_FINALMAIN_H
