//
// Created by Hunter Manter on 11/5/18.
//

#include "finalmain.h"


//To use the program, just follow the prompts.  There is a max length of 20 and only the numbers provided for the mask will create a recognizable qr code

int main() {
    //NOTE: Only using version 1-L and AlphaNum mode because of the incredible complexities
    qr qr = {};
    //get input
    getInput(&qr);
    //Get length of expected encoded string for use later
    lengthOfEnco(&qr);
    //encode the string
    encode(&qr);
    //Combine mode count indicator, length indicator and data into single stream
    combine(&qr);
    //add terminator
    addTerminator(&qr);
    //add more zeros
    makeMultEight(&qr);
    //add pad bytes
    addPadBytes(&qr);
    //add errorcorrection code words
    addErrorCorrectionCodewords(&qr);
    //go block by block and get encoded and put block by block into the qr code array
    placeEncodedData(&qr);
    //add mask and other version bits
    addMask(&qr);
    //write array to terminal
    printCode(&qr);
    return 0;
}
