//
// Created by Hunter Manter on 11/7/18.
//
#include "finalmain.h"

/** ----------------------------------------------------------
 * @fn void getInput(qr *qr)
 * @brief Gets input from user on the data the qr dcode should have and which mask to use
 * @param qr is a pointer to the struct qr
 * @return void, stores info in qr structt
 * ----------------------------------------------------------
 */
void getInput(qr *qr) {
    //get string through input
    printf("Please enter what you want to encode: \n");
    scanf("%20[^\n]", qr->input);
    printf("Please enter which mask you would like to use (0-7): \n");
    scanf("%d", &qr->mask);
    //strcpy(qr->input,"HELLO WORLD");
    //printf("%d: %s\n",qr->mask,qr->input);
    strcpy(qr->modeIndicator, "0010");
}

/** ----------------------------------------------------------
 * @fn void lengthOfEnco(qr *qr)
 * @brief Figures out the expected length of the encoded binary
 * @param qr is a pointer to the struct qr
 * @return void, stores info in qr structt
 * ----------------------------------------------------------
 */
void lengthOfEnco(qr *qr) {
    int length = strlen(qr->input);
    int numPairs = length / 2;
    int lengthEnc = numPairs * 11;
    if (length % 2 == 1) {
        lengthEnc += 6;
    }
    qr->encodedLength = lengthEnc;

    dec2bin(qr->charCountIndicator, length);
    addBuffer(qr->charCountIndicator, 9);

}

/** ----------------------------------------------------------
 * @fn void encode(qr *qr)
 * @brief Parent functuon for encoding the data
 * @param qr is a pointer to the struct qr
 * @return void, stores info in qr structt
 * ----------------------------------------------------------
 */
void encode(qr *qr) {
    int numPairs = qr->encodedLength / 11;
    int odd = 0;
    if (qr->encodedLength % 11 != 11) {
        odd = 1;
    }
    int pairVal;
    int outputaAt = 0;
    char charsInBin[12];
    for (int i = 0; i < numPairs * 2 + odd; i = i + 2) {
        pairVal = 0;
        pairVal += getAlphNumVal(qr->input[i]);
        memset(charsInBin, 0, sizeof(charsInBin));
        if (i < numPairs * 2) {
            pairVal *= 45;
            pairVal += getAlphNumVal(qr->input[i + 1]);
            dec2bin(charsInBin, pairVal);
            addBuffer(charsInBin, 11);
        } else {
            dec2bin(charsInBin, pairVal);
            addBuffer(charsInBin, 6);
        }
        for (int i = outputaAt; i < 11 + outputaAt; i++) {
            qr->encodedData[i] = charsInBin[i - outputaAt];
        }
        outputaAt += 11;
    }
}

/** ----------------------------------------------------------
 * @fn int getAlphNumVal(char let)
 * @brief Findds decimal value for ascii value
 * @param let is a char to be converted
 * @return int, the decimal value that the ascii character is equal to
 * ----------------------------------------------------------
 */
int getAlphNumVal(char let) {
    switch (let) {
        case '0':
            return 0;
        case '1':
            return 1;
        case '2':
            return 2;
        case '3':
            return 3;
        case '4':
            return 4;
        case '5':
            return 5;
        case '6':
            return 6;
        case '7':
            return 7;
        case '8':
            return 8;
        case '9':
            return 9;
        case 'A':
            return 10;
        case 'B':
            return 11;
        case 'C':
            return 12;
        case 'D':
            return 13;
        case 'E':
            return 14;
        case 'F':
            return 15;
        case 'G':
            return 16;
        case 'H':
            return 17;
        case 'I':
            return 18;
        case 'J':
            return 19;
        case 'K':
            return 20;
        case 'L':
            return 21;
        case 'M':
            return 22;
        case 'N':
            return 23;
        case 'O':
            return 24;
        case 'P':
            return 25;
        case 'Q':
            return 26;
        case 'R':
            return 27;
        case 'S':
            return 28;
        case 'T':
            return 29;
        case 'U':
            return 30;
        case 'V':
            return 31;
        case 'W':
            return 32;
        case 'X':
            return 33;
        case 'Y':
            return 34;
        case 'Z':
            return 35;
        case ' ':
            return 36;
        case '$':
            return 37;
        case '%':
            return 38;
        case '*':
            return 39;
        case '+':
            return 40;
        case '-':
            return 41;
        case '.':
            return 42;
        case '/':
            return 43;
        case ':':
            return 44;
        case 'a':
            return 10;
        case 'b':
            return 11;
        case 'c':
            return 12;
        case 'd':
            return 13;
        case 'e':
            return 14;
        case 'f':
            return 15;
        case 'g':
            return 16;
        case 'h':
            return 17;
        case 'i':
            return 18;
        case 'j':
            return 19;
        case 'k':
            return 20;
        case 'l':
            return 21;
        case 'm':
            return 22;
        case 'n':
            return 23;
        case 'o':
            return 24;
        case 'p':
            return 25;
        case 'q':
            return 26;
        case 'r':
            return 27;
        case 's':
            return 28;
        case 't':
            return 29;
        case 'u':
            return 30;
        case 'v':
            return 31;
        case 'w':
            return 32;
        case 'x':
            return 33;
        case 'y':
            return 34;
        case 'z':
            return 35;
        default:
            return -1;
    }
}

/** ----------------------------------------------------------
 * @fn void dec2bin(char *charsInBin, int c)
 * @brief Returns binary value of decimal
 * @param charsInBin is a pointer to a string to store result in
 * @param c is the decimal to convert
 * @return void, stores the binary in the passed string array
 * ----------------------------------------------------------
 */
void dec2bin(char *charsInBin, int c) {
    int d[20];
    int i = 0;
    while (c > 0) {
        d[i] = c % 2;
        i++;
        c = c / 2;
    }
    int g = 0;
    for (int j = i - 1; j >= 0; j--) {
        charsInBin[j] = '0' + d[g];
        g++;
    }
}

/** ----------------------------------------------------------
 * @fn unsigned int bin2Dec(char *str)
 * @brief Finds the decimal representation of the string
 * @param str is the binary string to find the decimal value of
 * @return unsigned int, the decimal value of the binary
 * ----------------------------------------------------------
 */
unsigned int bin2Dec(char *str) {
    unsigned int val = 0;

    for (int i = 0; i < 8; i++) {
        val = 2 * val + (*str++ - '0');
    }
    return val;
}

/** ----------------------------------------------------------
 * @fn void addBuffer(char *charsInBinary, int bufferAmt)
 * @brief adds a buffer of 0s to the front of the passed binary
 * @param *charsInBinary, the binary to be padded and returned to
 * @param bufferAmt, the ammount of total bits the binary should have
 * @return void, stores the binary back into charsInBinary
 * ----------------------------------------------------------
 */
void addBuffer(char *charsInBinary, int bufferAmt) {
    if (strlen(charsInBinary) == bufferAmt) {

    } else if (strlen(charsInBinary) < bufferAmt) {
        char tempstr[bufferAmt];
        int diff = bufferAmt - (int) strlen(charsInBinary);
        for (int i = 0; i < diff; i++) {
            tempstr[i] = '0';
        }
        for (int i = 0; i < bufferAmt - diff; i++) {
            tempstr[i + diff] = charsInBinary[i];
        }
        for (int i = 0; i < bufferAmt; i++) {
            charsInBinary[i] = tempstr[i];
        }

    } else {
        printf("Error in addBuffer.  String is longer than what it should be.\n");
    }
}

/** ----------------------------------------------------------
 * @fn void addBufferEnd(char *charsInBinary, int bufferAmt)
 * @brief adds a buffer of 0s to the end of the passed binary
 * @param *charsInBinary, the binary to be padded and returned to
 * @param bufferAmt, the ammount of total bits the binary should have
 * @return void, stores the binary back into charsInBinary
 * ----------------------------------------------------------
 */
void addBufferEnd(char *charsInBinary, int bufferAmt) {
    if (strlen(charsInBinary) == bufferAmt) {

    } else if (strlen(charsInBinary) < bufferAmt) {
        char tempstr[bufferAmt];
        int diff = bufferAmt - (int) strlen(charsInBinary);
        int currLen = (int) strlen(charsInBinary);
        for (int i = 0; i < bufferAmt - diff; i++) {
            tempstr[i] = charsInBinary[i];
        }
        for (int i = currLen; i < currLen + diff; i++) {
            tempstr[i] = '0';
        }
        for (int i = 0; i < bufferAmt; i++) {
            charsInBinary[i] = tempstr[i];
        }

    } else {
        printf("Error in addBuffer.  String is longer than what it should be.\n");
    }
}

/** ----------------------------------------------------------
 * @fn void combine(qr *qr)
 * @brief adds all admin data and encodded data to form almost final binary string
 * @param qr, the struct where all admin and encoded data is
 * @return void, stores the data back into the struct under a different value
 * ----------------------------------------------------------
 */
void combine(qr *qr) {
    for (int i = 0; i < 4; i++) { qr->totalData[i] = qr->modeIndicator[i]; }
    for (int i = 4; i < 13; i++) { qr->totalData[i] = qr->charCountIndicator[i - 4]; }
    for (int i = 0; i < strlen(qr->encodedData); i++) { qr->totalData[i + 13] = qr->encodedData[i]; }
}

/** ----------------------------------------------------------
 * @fn void addTerminator(qr *qr)
 * @brief adds a terminator string of 0000 to the end of the total binary
 * @param qr, the struct where the binary is stored
 * @return void, stores the data back into the struct under a different value
 * ----------------------------------------------------------
 */
void addTerminator(qr *qr) {
    int difference = 19 * 8 - (int) strlen(qr->totalData);
    if (difference < 0) {
        printf("Error in addTerminal: Impossible string length");
    }
    difference = (difference > 4) ? 4 : difference;
    addBufferEnd(qr->totalData, (int) strlen(qr->totalData) + difference);
}

/** ----------------------------------------------------------
 * @fn void makeMultEight(qr *qr)
 * @brief makes the binary divisible by 8 by adding 0s
 * @param qr, the struct where the binary is stored
 * @return void, stores the data back into the struct under a different value
 * ----------------------------------------------------------
 */
void makeMultEight(qr *qr) {
    if (strlen(qr->totalData) % 8 == 0) {

    } else {
        int diff = 8 - strlen(qr->totalData) % 8;
        addBufferEnd(qr->totalData, (int) strlen(qr->totalData) + diff);
    }
}

/** ----------------------------------------------------------
 * @fn void addPadBytes(qr *qr)
 * @brief makes the binary longer according to the qr standards by adding set bytes
 * @param qr, the struct where the binary is stored
 * @return void, stores the data back into the struct under a different value
 * ----------------------------------------------------------
 */
void addPadBytes(qr *qr) {
    if ((int) strlen(qr->totalData) > 128) {
        //printf("Error in addPadBytes: length of data is exceded allowable");
    } else {
        char byteOne[] = "11101100";
        char byteTwo[] = "00010001";
        int itter = (128 - (int) strlen(qr->totalData)) / 8;
        int temp = 0;
        for (int i = 0; i < itter; i++) {
            if (temp == 0) {
                sprintf(qr->totalData, "%s%s", qr->totalData, byteOne);
                temp = 1;
            } else {
                sprintf(qr->totalData, "%s%s", qr->totalData, byteTwo);
                temp = 0;
            }
        }
    }

}

/** ----------------------------------------------------------
 * @fn void printCode(qr *qr)
 * @brief prints qr code to the screen
 * @param qr, the struct where the binary is stored
 * @return void, prints to the screen
 * ----------------------------------------------------------
 */
void printCode(qr *qr) {
    for (int i = 0; i < 21; i++) {
        for (int j = 0; j < 21; j++) {
            if (qr->array[i][j] == 1) {
                printf("\342\226\210\342\226\210");
            } else if (qr->array[i][j] == 0) {
                printf("  ");
            }
        }
        printf("\n");
    }
}

/** ----------------------------------------------------------
 * @fn void addErrorCorrectionCodewords(qr *qr)
 * @brief generates reed-solomon erro correction codes
 * @param qr, the struct where the binary is stored
 * @return void, adds data to the end of the binary
 * ----------------------------------------------------------
 */
void addErrorCorrectionCodewords(qr *qr) {
    unsigned int messageCoef[16];
    coefficientsMessagePolynomial(qr, messageCoef);
    unsigned int messageExp[16] = {15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1,
                                   0}; //Exponents of the x in the polynomials
    unsigned int messageAlph[16];

    unsigned int xor[16];
    unsigned int xoralph[16];

    unsigned int generatorAlph[11] = {0, 251, 67, 46, 61, 118, 70, 64, 94, 32,
                                      45};  //Exponents of the x in the polynomials
    unsigned int generatorAlphTmp[11];
    unsigned int generatorExp[11] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
    unsigned int generatorCoef[11];

    int lost = 15;

    coefToAlph(messageCoef, messageAlph);//source, dest
    alphToCoef(generatorAlph, generatorCoef);//source, dest


    //multiply the message polynomial by x^10 to make sure that the lead term doesn't become too big (its 10 because thats how many error correction codewords)
    for (int i = 0; i < 16; i++) { messageExp[i] = messageExp[i] + 10; }
    //DO the same thing for the generator poly so that the exponents are the same but with 15
    for (int i = 0; i < 11; i++) { generatorExp[i] = generatorExp[i] + 15; }
    //Division will take 16 steps to complete and remainder will have 10 terms which will be the codewords
    //1a
    // Multiply the Generator Polynomial by the Lead Term of the Message Polynomial (add lead alpha of message to all generator alphas)
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + messageAlph[0]; }
    //make sure no alpha values are over 255 so perform modulo 255 on all
    unsignedMod(generatorAlphTmp);
    //convert to integer notation
    alphToCoef(generatorAlphTmp, generatorCoef);
    //1b
    //XOR result with message polynomial, since this is the first division step, XOR result from 1a with the message polynomial
    for (int i = 0; i < 11; i++) { xor[i] = generatorCoef[i] ^ messageCoef[i]; }
    for (int i = 11; i < 16; i++) { xor[i] = messageCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }
    lost--;//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //2a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //2b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }
    lost--;//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //3a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //3b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }
    lost--;//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //4a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //4b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }
    lost--;//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //5a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //5b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }
    lost--;//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //6a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //6b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);

    //7a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //7b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //8a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //8b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //9a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //9b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);



    //10a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //10b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);

    //11a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //11b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //12a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //12b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //13a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //13b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //14a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //14b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //15a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //15b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);


    //16a
    //Multiply the generator poly by the lead term of the XOR result from previous step
    for (int i = 0; i < 11; i++) { generatorAlphTmp[i] = generatorAlph[i] + xoralph[0]; }
    unsignedMod(generatorAlphTmp);
    alphToCoef(generatorAlphTmp, generatorCoef);
    //16b
    //use result from step 1b to perform next XOR
    for (int i = 0; i < 11; i++) { xor[i] = xor[i] ^ generatorCoef[i]; }
    for (int i = 0; i < lost; i++) { xor[i] = xor[i + 1]; }
    for (int i = lost; i < 16; i++) { xor[i] = 0; }//removes the leading 0 term
    coefToAlph(xor, xoralph);

    char codeword[9];
    for (int i = 0; i < 10; i++) {
        dec2bin(codeword, xor[i]);
        addBuffer(codeword, 8);
        strcat(qr->totalData, codeword);
    }

}

/** ----------------------------------------------------------
 * @fn void placeEncodedData(qr *qr)
 * @brief put the encoded binary into the qr code structure
 * @param qr, the struct where the binary is stored
 * @return void, adds the data into the struct's array
 * ----------------------------------------------------------
 */
void placeEncodedData(qr *qr) {
    int onBit = 0;
    //right column 3 blocks
    for (int i = 0; i < 24; i++)//up placement
    {
        if (i % 2 == 0) {
            qr->array[20 - (i / 2)][20] = qr->totalData[onBit] - '0';
        } else {
            qr->array[20 - i / 2][19] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }//good

    for (int i = 0; i < 24; i++)//down placement
    {
        if (i % 2 == 0) {
            qr->array[9 + (i / 2)][18] = qr->totalData[onBit] - '0';
        } else {
            qr->array[9 + i / 2][17] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }//good

    for (int i = 0; i < 24; i++)//up
    {
        if (i % 2 == 0) {
            qr->array[20 - (i / 2)][16] = qr->totalData[onBit] - '0';
        } else {
            qr->array[20 - i / 2][15] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }//good
    for (int i = 0; i < 24; i++)//down
    {
        if (i % 2 == 0) {
            qr->array[9 + (i / 2)][14] = qr->totalData[onBit] - '0';
        } else {
            qr->array[9 + i / 2][13] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }//first bit bad
    for (int i = 0; i < 26; i++)//up big
    {
        if (i == 15) { i = i + 2; }
        if (i % 2 == 0) {
            qr->array[20 - (i / 2)][12] = qr->totalData[onBit] - '0';
        } else {
            qr->array[20 - i / 2][11] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }
    for (int i = 0; i < 12; i++) {
        if (i % 2 == 0) {
            qr->array[6 - (i / 2)][12] = qr->totalData[onBit] - '0';
        } else {
            qr->array[6 - i / 2][11] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }

    for (int i = 0; i < 12; i++) {
        if (i % 2 == 0) {
            qr->array[(i / 2)][10] = qr->totalData[onBit] - '0';
        } else {
            qr->array[i / 2][9] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }
    for (int i = 0; i < 28; i++) {
        if (i % 2 == 0) {
            qr->array[7 + (i / 2)][10] = qr->totalData[onBit] - '0';
        } else {
            qr->array[7 + i / 2][9] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }
    for (int i = 0; i < 8; i++)//up placement
    {
        if (i % 2 == 0) {
            qr->array[12 - (i / 2)][8] = qr->totalData[onBit] - '0';
        } else {
            qr->array[12 - i / 2][7] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }
    for (int i = 0; i < 8; i++)//Down but no in left zone
    {
        if (i % 2 == 0) {
            qr->array[9 + (i / 2)][5] = qr->totalData[onBit] - '0';
        } else {
            qr->array[9 + i / 2][4] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }
    for (int i = 0; i < 8; i++)//up placement
    {
        if (i % 2 == 0) {
            qr->array[12 - (i / 2)][3] = qr->totalData[onBit] - '0';
        } else {
            qr->array[12 - i / 2][2] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }
    for (int i = 0; i < 8; i++)//Down
    {
        if (i % 2 == 0) {
            qr->array[9 + (i / 2)][1] = qr->totalData[onBit] - '0';
        } else {
            qr->array[9 + i / 2][0] = qr->totalData[onBit] - '0';
        }
        onBit++;
    }
}

/** ----------------------------------------------------------
 * @fn void addMask(qr *qr)
 * @brief generate mask and place in array
 * @param qr, the struct where the binary is stored
 * @return void, adds the data into the struct's array
 * ----------------------------------------------------------
 */
void addMask(qr *qr) {
    //acceptable area is 9,0 - 12,5
    //acceptable area is 0,9 - 5,12
    //acceptable area is 9,7 - 12,8
    //acceptable area is 7,9 - 8.12
    //acceptable area is 9,9 20,20

    if (qr->mask == 0) {
        for(int i = 9;i<13;i++)
        {
            for(int j = 0;j<6;j++)
            {
                if((i + j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 0;i<6;i++)
        {
            for(int j = 9;j<13;j++)
            {
                if((i + j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<13;i++)
        {
            for(int j = 7;j<9;j++)
            {
                if((i + j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 7;i<9;i++)
        {
            for(int j = 9;j<12;j++)
            {
                if((i + j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<21;i++)
        {
            for(int j = 9;j<21;j++)
            {
                if((i + j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
    } else if (qr->mask == 1) {
        for(int i = 9;i<13;i++)
        {
            for(int j = 0;j<6;j++)
            {
                if((j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 0;i<6;i++)
        {
            for(int j = 9;j<13;j++)
            {
                if((j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<13;i++)
        {
            for(int j = 7;j<9;j++)
            {
                if((j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 7;i<9;i++)
        {
            for(int j = 9;j<12;j++)
            {
                if((j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<21;i++)
        {
            for(int j = 9;j<21;j++)
            {
                if((j) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
    } else if (qr->mask == 2) {
        for(int i = 9;i<13;i++)
        {
            for(int j = 0;j<6;j++)
            {
                if((i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 0;i<6;i++)
        {
            for(int j = 9;j<13;j++)
            {
                if((i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<13;i++)
        {
            for(int j = 7;j<9;j++)
            {
                if((i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 7;i<9;i++)
        {
            for(int j = 9;j<12;j++)
            {
                if((i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<21;i++)
        {
            for(int j = 9;j<21;j++)
            {
                if((i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
    } else if (qr->mask == 3) {
        for(int i = 9;i<13;i++)
        {
            for(int j = 0;j<6;j++)
            {
                if((j + i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 0;i<6;i++)
        {
            for(int j = 9;j<13;j++)
            {
                if((j + i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<13;i++)
        {
            for(int j = 7;j<9;j++)
            {
                if((j + i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 7;i<9;i++)
        {
            for(int j = 9;j<12;j++)
            {
                if((j + i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<21;i++)
        {
            for(int j = 9;j<21;j++)
            {
                if((j + i) % 3 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
    } else if (qr->mask == 4) {
        for(int i = 9;i<13;i++)
        {
            for(int j = 0;j<6;j++)
            {
                if((j / 2 + i / 3) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 0;i<6;i++)
        {
            for(int j = 9;j<13;j++)
            {
                if((j / 2 + i / 3) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<13;i++)
        {
            for(int j = 7;j<9;j++)
            {
                if((j / 2 + i / 3) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 7;i<9;i++)
        {
            for(int j = 9;j<12;j++)
            {
                if((j / 2 + i / 3) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<21;i++)
        {
            for(int j = 9;j<21;j++)
            {
                if((j / 2 + i / 3) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
    } else if (qr->mask == 5) {//((row * column) mod 2) + ((row * column) mod 3) == 0
        for(int i = 9;i<13;i++)
        {
            for(int j = 0;j<6;j++)
            {
                if(((i * j) % 2) + ((i * j) % 3) == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 0;i<6;i++)
        {
            for(int j = 9;j<13;j++)
            {
                if(((i * j) % 2) + ((i * j) % 3) == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<13;i++)
        {
            for(int j = 7;j<9;j++)
            {
                if(((i * j) % 2) + ((i * j) % 3) == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 7;i<9;i++)
        {
            for(int j = 9;j<12;j++)
            {
                if(((i * j) % 2) + ((i * j) % 3) == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<21;i++)
        {
            for(int j = 9;j<21;j++)
            {
                if(((i * j) % 2) + ((i * j) % 3) == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
    } else if (qr->mask == 6) {//( ((row * column) mod 2) + ((row * column) mod 3) ) mod 2 == 0
        for(int i = 9;i<13;i++)
        {
            for(int j = 0;j<6;j++)
            {
                if((((i * j) % 2) + ((i * j) % 3)) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 0;i<6;i++)
        {
            for(int j = 9;j<13;j++)
            {
                if((((i * j) % 2) + ((i * j) % 3)) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<13;i++)
        {
            for(int j = 7;j<9;j++)
            {
                if((((i * j) % 2) + ((i * j) % 3)) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 7;i<9;i++)
        {
            for(int j = 9;j<12;j++)
            {
                if((((i * j) % 2) + ((i * j) % 3)) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
        for(int i = 9;i<21;i++)
        {
            for(int j = 9;j<21;j++)
            {
                if((((i * j) % 2) + ((i * j) % 3)) % 2 == 0)
                {
                    flipBit(i,j,qr);
                }
            }
        }
    } else if (qr->mask == 7) {//( ((row + column) mod 2) + ((row * column) mod 3) ) mod 2 == 0
        for (int i = 0; i < 21; i++) {
            for (int j = 0; j < 21; j++) {
                if ((((i * j) % 2) + ((i * j) % 3)) % 2 == 0) {
                    if (qr->array[j][i] == 1) {
                        qr->array[j][i] = 0;
                    } else if (qr->array[j][i] == 0) {
                        qr->array[j][i] = 1;
                    }
                }
            }
        }
    }

    for (int i = 0; i < 9; i++) { for (int j = 0; j < 9; j++) { qr->array[i][j] = 0; }}
    for (int i = 13; i < 21; i++) { for (int j = 0; j < 8; j++) { qr->array[i][j] = 0; }}
    for (int i = 0; i < 8; i++) { for (int j = 13; j < 21; j++) { qr->array[i][j] = 0; }}

    //timing bits
    qr->array[6][8] = 1;
    qr->array[6][10] = 1;
    qr->array[6][12] = 1;
    qr->array[6][9] = 0;
    qr->array[6][11] = 0;


    qr->array[8][6] = 1;
    qr->array[10][6] = 1;
    qr->array[12][6] = 1;
    qr->array[9][6] = 0;
    qr->array[11][6] = 0;

    //alighning bits
    qr->array[0][0] = 1;
    qr->array[0][1] = 1;
    qr->array[0][2] = 1;
    qr->array[0][3] = 1;
    qr->array[0][4] = 1;
    qr->array[0][5] = 1;
    qr->array[0][6] = 1;

    qr->array[14][0] = 1;
    qr->array[14][1] = 1;
    qr->array[14][2] = 1;
    qr->array[14][3] = 1;
    qr->array[14][4] = 1;
    qr->array[14][5] = 1;
    qr->array[14][6] = 1;

    qr->array[0][14] = 1;
    qr->array[0][15] = 1;
    qr->array[0][16] = 1;
    qr->array[0][17] = 1;
    qr->array[0][18] = 1;
    qr->array[0][19] = 1;
    qr->array[0][20] = 1;

    qr->array[6][0] = 1;
    qr->array[6][1] = 1;
    qr->array[6][2] = 1;
    qr->array[6][3] = 1;
    qr->array[6][4] = 1;
    qr->array[6][5] = 1;
    qr->array[6][6] = 1;

    qr->array[20][0] = 1;
    qr->array[20][1] = 1;
    qr->array[20][2] = 1;
    qr->array[20][3] = 1;
    qr->array[20][4] = 1;
    qr->array[20][5] = 1;
    qr->array[20][6] = 1;

    qr->array[6][14] = 1;
    qr->array[6][15] = 1;
    qr->array[6][16] = 1;
    qr->array[6][17] = 1;
    qr->array[6][18] = 1;
    qr->array[6][19] = 1;
    qr->array[6][20] = 1;


    qr->array[0][0] = 1;
    qr->array[1][0] = 1;
    qr->array[2][0] = 1;
    qr->array[3][0] = 1;
    qr->array[4][0] = 1;
    qr->array[5][0] = 1;
    qr->array[6][0] = 1;

    qr->array[0][14] = 1;
    qr->array[1][14] = 1;
    qr->array[2][14] = 1;
    qr->array[3][14] = 1;
    qr->array[4][14] = 1;
    qr->array[5][14] = 1;
    qr->array[6][14] = 1;

    qr->array[0][20] = 1;
    qr->array[1][20] = 1;
    qr->array[2][20] = 1;
    qr->array[3][20] = 1;
    qr->array[4][20] = 1;
    qr->array[5][20] = 1;
    qr->array[6][20] = 1;

    qr->array[0][6] = 1;
    qr->array[1][6] = 1;
    qr->array[2][6] = 1;
    qr->array[3][6] = 1;
    qr->array[4][6] = 1;
    qr->array[5][6] = 1;
    qr->array[6][6] = 1;

    qr->array[14][0] = 1;
    qr->array[15][0] = 1;
    qr->array[16][0] = 1;
    qr->array[17][0] = 1;
    qr->array[18][0] = 1;
    qr->array[19][0] = 1;
    qr->array[20][0] = 1;

    qr->array[14][6] = 1;
    qr->array[15][6] = 1;
    qr->array[16][6] = 1;
    qr->array[17][6] = 1;
    qr->array[18][6] = 1;
    qr->array[19][6] = 1;
    qr->array[20][6] = 1;


    qr->array[2][2] = 1;
    qr->array[2][3] = 1;
    qr->array[2][4] = 1;
    qr->array[3][2] = 1;
    qr->array[3][3] = 1;
    qr->array[3][4] = 1;
    qr->array[4][2] = 1;
    qr->array[4][3] = 1;
    qr->array[4][4] = 1;

    qr->array[16][2] = 1;
    qr->array[16][3] = 1;
    qr->array[16][4] = 1;
    qr->array[17][2] = 1;
    qr->array[17][3] = 1;
    qr->array[17][4] = 1;
    qr->array[18][2] = 1;
    qr->array[18][3] = 1;
    qr->array[18][4] = 1;

    qr->array[2][16] = 1;
    qr->array[2][17] = 1;
    qr->array[2][18] = 1;
    qr->array[3][16] = 1;
    qr->array[3][17] = 1;
    qr->array[3][18] = 1;
    qr->array[4][16] = 1;
    qr->array[4][17] = 1;
    qr->array[4][18] = 1;


    qr->array[13][8] = 1;

    //1010100
    //00010010
    if (qr->mask == 0) {
        qr->array[20][8] = 1;//0
        qr->array[19][8] = 0;
        qr->array[18][8] = 1;
        qr->array[17][8] = 0;
        qr->array[16][8] = 1;
        qr->array[15][8] = 0;
        qr->array[14][8] = 0;//6

        qr->array[8][0] = 1;//0
        qr->array[8][1] = 0;
        qr->array[8][2] = 1;
        qr->array[8][3] = 0;
        qr->array[8][4] = 1;
        qr->array[8][5] = 0;
        qr->array[8][7] = 0;//6

        qr->array[0][8] = 0;//14
        qr->array[1][8] = 1;
        qr->array[2][8] = 0;
        qr->array[3][8] = 0;
        qr->array[4][8] = 1;
        qr->array[5][8] = 0;
        qr->array[7][8] = 0;
        qr->array[8][8] = 0;//7

        qr->array[8][13] = 0;//7
        qr->array[8][14] = 0;
        qr->array[8][15] = 0;
        qr->array[8][16] = 1;
        qr->array[8][17] = 0;
        qr->array[8][18] = 0;
        qr->array[8][19] = 1;
        qr->array[8][20] = 0;//14
    } else if (qr->mask == 1)//1010001
        // 00100101
    {
        qr->array[20][8] = 1;//0
        qr->array[19][8] = 0;
        qr->array[18][8] = 1;
        qr->array[17][8] = 0;
        qr->array[16][8] = 0;
        qr->array[15][8] = 0;
        qr->array[14][8] = 1;//6

        qr->array[8][0] = 1;//0
        qr->array[8][1] = 0;
        qr->array[8][2] = 1;
        qr->array[8][3] = 0;
        qr->array[8][4] = 0;
        qr->array[8][5] = 0;
        qr->array[8][7] = 1;//6

        qr->array[0][8] = 1;//14
        qr->array[1][8] = 0;
        qr->array[2][8] = 1;
        qr->array[3][8] = 0;
        qr->array[4][8] = 0;
        qr->array[5][8] = 1;
        qr->array[7][8] = 0;
        qr->array[8][8] = 0;//7

        qr->array[8][13] = 0;//7
        qr->array[8][14] = 0;
        qr->array[8][15] = 1;
        qr->array[8][16] = 0;
        qr->array[8][17] = 0;
        qr->array[8][18] = 1;
        qr->array[8][19] = 0;
        qr->array[8][20] = 1;//14
    } else if (qr->mask == 2)//1011110
        // 01111100
    {
        qr->array[20][8] = 1;//0
        qr->array[19][8] = 0;
        qr->array[18][8] = 1;
        qr->array[17][8] = 1;
        qr->array[16][8] = 1;
        qr->array[15][8] = 1;
        qr->array[14][8] = 0;//6

        qr->array[8][0] = 1;//0
        qr->array[8][1] = 0;
        qr->array[8][2] = 1;
        qr->array[8][3] = 1;
        qr->array[8][4] = 1;
        qr->array[8][5] = 1;
        qr->array[8][7] = 0;//6

        qr->array[0][8] = 0;//14
        qr->array[1][8] = 0;
        qr->array[2][8] = 1;
        qr->array[3][8] = 1;
        qr->array[4][8] = 1;
        qr->array[5][8] = 1;
        qr->array[7][8] = 1;
        qr->array[8][8] = 0;//7

        qr->array[8][13] = 0;//7
        qr->array[8][14] = 1;
        qr->array[8][15] = 1;
        qr->array[8][16] = 1;
        qr->array[8][17] = 1;
        qr->array[8][18] = 1;
        qr->array[8][19] = 0;
        qr->array[8][20] = 0;//14
    } else if (qr->mask == 3)//1011011
        // 01001011
    {
        qr->array[20][8] = 1;//0
        qr->array[19][8] = 0;
        qr->array[18][8] = 1;
        qr->array[17][8] = 1;
        qr->array[16][8] = 0;
        qr->array[15][8] = 1;
        qr->array[14][8] = 1;//6

        qr->array[8][0] = 1;//0
        qr->array[8][1] = 0;
        qr->array[8][2] = 1;
        qr->array[8][3] = 1;
        qr->array[8][4] = 0;
        qr->array[8][5] = 1;
        qr->array[8][7] = 1;//6

        qr->array[0][8] = 1;//14
        qr->array[1][8] = 1;
        qr->array[2][8] = 0;
        qr->array[3][8] = 1;
        qr->array[4][8] = 0;
        qr->array[5][8] = 0;
        qr->array[7][8] = 1;
        qr->array[8][8] = 0;//7

        qr->array[8][13] = 0;//7
        qr->array[8][14] = 1;
        qr->array[8][15] = 0;
        qr->array[8][16] = 0;
        qr->array[8][17] = 1;
        qr->array[8][18] = 0;
        qr->array[8][19] = 1;
        qr->array[8][20] = 1;//14
    } else if (qr->mask == 4)//1000101
        // 11111001
    {
        qr->array[20][8] = 1;//0
        qr->array[19][8] = 0;
        qr->array[18][8] = 0;
        qr->array[17][8] = 0;
        qr->array[16][8] = 1;
        qr->array[15][8] = 0;
        qr->array[14][8] = 1;//6

        qr->array[8][0] = 1;//0
        qr->array[8][1] = 0;
        qr->array[8][2] = 0;
        qr->array[8][3] = 0;
        qr->array[8][4] = 1;
        qr->array[8][5] = 0;
        qr->array[8][7] = 1;//6

        qr->array[0][8] = 1;//14
        qr->array[1][8] = 0;
        qr->array[2][8] = 0;
        qr->array[3][8] = 1;
        qr->array[4][8] = 1;
        qr->array[5][8] = 1;
        qr->array[7][8] = 1;
        qr->array[8][8] = 1;//7

        qr->array[8][13] = 1;//7
        qr->array[8][14] = 1;
        qr->array[8][15] = 1;
        qr->array[8][16] = 1;
        qr->array[8][17] = 1;
        qr->array[8][18] = 0;
        qr->array[8][19] = 0;
        qr->array[8][20] = 1;//14
    } else if (qr->mask == 5)//1000000
        // 11001110
    {
        qr->array[20][8] = 1;//0
        qr->array[19][8] = 0;
        qr->array[18][8] = 0;
        qr->array[17][8] = 0;
        qr->array[16][8] = 0;
        qr->array[15][8] = 0;
        qr->array[14][8] = 0;//6

        qr->array[8][0] = 1;//0
        qr->array[8][1] = 0;
        qr->array[8][2] = 0;
        qr->array[8][3] = 0;
        qr->array[8][4] = 0;
        qr->array[8][5] = 0;
        qr->array[8][7] = 0;//6

        qr->array[0][8] = 0;//14
        qr->array[1][8] = 1;
        qr->array[2][8] = 1;
        qr->array[3][8] = 1;
        qr->array[4][8] = 0;
        qr->array[5][8] = 0;
        qr->array[7][8] = 1;
        qr->array[8][8] = 1;//7

        qr->array[8][13] = 1;//7
        qr->array[8][14] = 1;
        qr->array[8][15] = 0;
        qr->array[8][16] = 0;
        qr->array[8][17] = 1;
        qr->array[8][18] = 1;
        qr->array[8][19] = 1;
        qr->array[8][20] = 0;//14
    } else if (qr->mask == 6)//1001111
        // 10010111
    {
        qr->array[20][8] = 1;//0
        qr->array[19][8] = 0;
        qr->array[18][8] = 0;
        qr->array[17][8] = 1;
        qr->array[16][8] = 1;
        qr->array[15][8] = 1;
        qr->array[14][8] = 1;//6

        qr->array[8][0] = 1;//0
        qr->array[8][1] = 0;
        qr->array[8][2] = 0;
        qr->array[8][3] = 1;
        qr->array[8][4] = 1;
        qr->array[8][5] = 1;
        qr->array[8][7] = 1;//6

        qr->array[0][8] = 1;//14
        qr->array[1][8] = 1;
        qr->array[2][8] = 1;
        qr->array[3][8] = 0;
        qr->array[4][8] = 1;
        qr->array[5][8] = 0;
        qr->array[7][8] = 0;
        qr->array[8][8] = 1;//7

        qr->array[8][13] = 1;//7
        qr->array[8][14] = 0;
        qr->array[8][15] = 0;
        qr->array[8][16] = 1;
        qr->array[8][17] = 0;
        qr->array[8][18] = 1;
        qr->array[8][19] = 1;
        qr->array[8][20] = 1;//14
    } else if (qr->mask == 7)//1001010
        // 10100000
    {
        qr->array[20][8] = 1;//0
        qr->array[19][8] = 0;
        qr->array[18][8] = 0;
        qr->array[17][8] = 1;
        qr->array[16][8] = 0;
        qr->array[15][8] = 1;
        qr->array[14][8] = 0;//6

        qr->array[8][0] = 1;//0
        qr->array[8][1] = 0;
        qr->array[8][2] = 0;
        qr->array[8][3] = 1;
        qr->array[8][4] = 0;
        qr->array[8][5] = 1;
        qr->array[8][7] = 0;//6

        qr->array[0][8] = 0;//14
        qr->array[1][8] = 0;
        qr->array[2][8] = 0;
        qr->array[3][8] = 0;
        qr->array[4][8] = 0;
        qr->array[5][8] = 1;
        qr->array[7][8] = 0;
        qr->array[8][8] = 1;//7

        qr->array[8][13] = 1;//7
        qr->array[8][14] = 0;
        qr->array[8][15] = 1;
        qr->array[8][16] = 0;
        qr->array[8][17] = 0;
        qr->array[8][18] = 0;
        qr->array[8][19] = 0;
        qr->array[8][20] = 0;//14
    }
}

/** ----------------------------------------------------------
 * @fn coefficientsMessagePolynomial(qr *qr, unsigned int *coef)
 * @brief generate the generator polynomial for the qr code
 * @param qr, the struct where the binary is stored
 * @param *coef, an array to store the coeficients
 * @return void, adds the data into the array
 * ----------------------------------------------------------
 */
void coefficientsMessagePolynomial(qr *qr, unsigned int *coef) {
    char sub[8];
    for (int i = 0; i < 128; i = i + 8) {
        strncpy(sub, qr->totalData + i, 8);
        coef[i / 8] = bin2Dec(sub);
    }
}

/** ----------------------------------------------------------
 * @fn void coefToAlph(unsigned int *messageCoef, unsigned int *messageAlph)
 * @brief convert log to the antilog
 * @param *messageCoef, the array of decimals to convert
 * @param *messageAlph, the array of logs to store the data
 * @return void, adds the data into the  array
 * ----------------------------------------------------------
 */
void coefToAlph(unsigned int *messageCoef, unsigned int *messageAlph) {
    int length = 16;
    //printf("Message Coef: ");
    //for(int i = 0;i<length;i++){printf("%d, ",messageCoef[i]);}printf("\n");
    for (int i = 0; i < length; i++) {
        //printf("%d (%d)  ",i,messageCoef[i]);
        messageAlph[i] = numToAlph(messageCoef[i]);
    }
    //printf("\n\n");
    //exit(91);
}

/** ----------------------------------------------------------
 * @fn coefficientsMessagePolynomial(qr *qr, unsigned int *coef)
 * @brief convert decimal to the log of it
 * @param *generatorAlph, the array of logs to convert
 * @param *generatorCoef, the array of  anti logs to store the data
 * @return void, adds the data into the  array
 * ----------------------------------------------------------
 */
void alphToCoef(unsigned int *generatorAlph, unsigned int *generatorCoef) {
    int length = 11;
    for (int i = 0; i < length; i++) {
        generatorCoef[i] = alphToNum(generatorAlph[i]);
    }
}

/** ----------------------------------------------------------
 * @fn unsigned int alphToNum(unsigned int num)
 * @brief convert an log into a decimal
 * @param num, the log to change
 * @return unsigned int, the decimal representation to change the log to
 * ----------------------------------------------------------
 */
unsigned int alphToNum(unsigned int num) {
    switch (num) {
        case 0:
            return 1;
        case 1:
            return 2;
        case 2:
            return 4;
        case 3:
            return 8;
        case 4:
            return 16;
        case 5:
            return 32;
        case 6:
            return 64;
        case 7:
            return 128;
        case 8:
            return 29;
        case 9:
            return 58;
        case 10:
            return 116;
        case 11:
            return 232;
        case 12:
            return 205;
        case 13:
            return 135;
        case 14:
            return 19;
        case 15:
            return 38;
        case 16:
            return 76;
        case 17:
            return 152;
        case 18:
            return 45;
        case 19:
            return 90;
        case 20:
            return 180;
        case 21:
            return 117;
        case 22:
            return 234;
        case 23:
            return 201;
        case 24:
            return 143;
        case 25:
            return 3;
        case 26:
            return 6;
        case 27:
            return 12;
        case 28:
            return 24;
        case 29:
            return 48;
        case 30:
            return 96;
        case 31:
            return 192;
        case 32:
            return 157;
        case 33:
            return 39;
        case 34:
            return 78;
        case 35:
            return 156;
        case 36:
            return 37;
        case 37:
            return 74;
        case 38:
            return 148;
        case 39:
            return 53;
        case 40:
            return 106;
        case 41:
            return 212;
        case 42:
            return 181;
        case 43:
            return 119;
        case 44:
            return 238;
        case 45:
            return 193;
        case 46:
            return 159;
        case 47:
            return 35;
        case 48:
            return 70;
        case 49:
            return 140;
        case 50:
            return 5;
        case 51:
            return 10;
        case 52:
            return 20;
        case 53:
            return 40;
        case 54:
            return 80;
        case 55:
            return 160;
        case 56:
            return 93;
        case 57:
            return 186;
        case 58:
            return 105;
        case 59:
            return 210;
        case 60:
            return 185;
        case 61:
            return 111;
        case 62:
            return 222;
        case 63:
            return 161;
        case 64:
            return 95;
        case 65:
            return 190;
        case 66:
            return 97;
        case 67:
            return 194;
        case 68:
            return 153;
        case 69:
            return 47;
        case 70:
            return 94;
        case 71:
            return 188;
        case 72:
            return 101;
        case 73:
            return 202;
        case 74:
            return 137;
        case 75:
            return 15;
        case 76:
            return 30;
        case 77:
            return 60;
        case 78:
            return 120;
        case 79:
            return 240;
        case 80:
            return 253;
        case 81:
            return 231;
        case 82:
            return 211;
        case 83:
            return 187;
        case 84:
            return 107;
        case 85:
            return 214;
        case 86:
            return 177;
        case 87:
            return 127;
        case 88:
            return 254;
        case 89:
            return 225;
        case 90:
            return 223;
        case 91:
            return 163;
        case 92:
            return 91;
        case 93:
            return 182;
        case 94:
            return 113;
        case 95:
            return 226;
        case 96:
            return 217;
        case 97:
            return 175;
        case 98:
            return 67;
        case 99:
            return 134;
        case 100:
            return 17;
        case 101:
            return 34;
        case 102:
            return 68;
        case 103:
            return 136;
        case 104:
            return 13;
        case 105:
            return 26;
        case 106:
            return 52;
        case 107:
            return 104;
        case 108:
            return 208;
        case 109:
            return 189;
        case 110:
            return 103;
        case 111:
            return 206;
        case 112:
            return 129;
        case 113:
            return 31;
        case 114:
            return 62;
        case 115:
            return 124;
        case 116:
            return 248;
        case 117:
            return 237;
        case 118:
            return 199;
        case 119:
            return 147;
        case 120:
            return 59;
        case 121:
            return 118;
        case 122:
            return 236;
        case 123:
            return 197;
        case 124:
            return 151;
        case 125:
            return 51;
        case 126:
            return 102;
        case 127:
            return 204;
        case 128:
            return 133;
        case 129:
            return 23;
        case 130:
            return 46;
        case 131:
            return 92;
        case 132:
            return 184;
        case 133:
            return 109;
        case 134:
            return 218;
        case 135:
            return 169;
        case 136:
            return 79;
        case 137:
            return 158;
        case 138:
            return 33;
        case 139:
            return 66;
        case 140:
            return 132;
        case 141:
            return 21;
        case 142:
            return 42;
        case 143:
            return 84;
        case 144:
            return 168;
        case 145:
            return 77;
        case 146:
            return 154;
        case 147:
            return 41;
        case 148:
            return 82;
        case 149:
            return 164;
        case 150:
            return 85;
        case 151:
            return 170;
        case 152:
            return 73;
        case 153:
            return 146;
        case 154:
            return 57;
        case 155:
            return 114;
        case 156:
            return 228;
        case 157:
            return 213;
        case 158:
            return 183;
        case 159:
            return 115;
        case 160:
            return 230;
        case 161:
            return 209;
        case 162:
            return 191;
        case 163:
            return 99;
        case 164:
            return 198;
        case 165:
            return 145;
        case 166:
            return 63;
        case 167:
            return 126;
        case 168:
            return 252;
        case 169:
            return 229;
        case 170:
            return 215;
        case 171:
            return 179;
        case 172:
            return 123;
        case 173:
            return 246;
        case 174:
            return 241;
        case 175:
            return 255;
        case 176:
            return 227;
        case 177:
            return 219;
        case 178:
            return 171;
        case 179:
            return 75;
        case 180:
            return 150;
        case 181:
            return 49;
        case 182:
            return 98;
        case 183:
            return 196;
        case 184:
            return 149;
        case 185:
            return 55;
        case 186:
            return 110;
        case 187:
            return 220;
        case 188:
            return 165;
        case 189:
            return 87;
        case 190:
            return 174;
        case 191:
            return 65;
        case 192:
            return 130;
        case 193:
            return 25;
        case 194:
            return 50;
        case 195:
            return 100;
        case 196:
            return 200;
        case 197:
            return 141;
        case 198:
            return 7;
        case 199:
            return 14;
        case 200:
            return 28;
        case 201:
            return 56;
        case 202:
            return 112;
        case 203:
            return 224;
        case 204:
            return 221;
        case 205:
            return 167;
        case 206:
            return 83;
        case 207:
            return 166;
        case 208:
            return 81;
        case 209:
            return 162;
        case 210:
            return 89;
        case 211:
            return 178;
        case 212:
            return 121;
        case 213:
            return 242;
        case 214:
            return 249;
        case 215:
            return 239;
        case 216:
            return 195;
        case 217:
            return 155;
        case 218:
            return 43;
        case 219:
            return 86;
        case 220:
            return 172;
        case 221:
            return 69;
        case 222:
            return 138;
        case 223:
            return 9;
        case 224:
            return 18;
        case 225:
            return 36;
        case 226:
            return 72;
        case 227:
            return 144;
        case 228:
            return 61;
        case 229:
            return 122;
        case 230:
            return 244;
        case 231:
            return 245;
        case 232:
            return 247;
        case 233:
            return 243;
        case 234:
            return 251;
        case 235:
            return 235;
        case 236:
            return 203;
        case 237:
            return 139;
        case 238:
            return 11;
        case 239:
            return 22;
        case 240:
            return 44;
        case 241:
            return 88;
        case 242:
            return 176;
        case 243:
            return 125;
        case 244:
            return 250;
        case 245:
            return 233;
        case 246:
            return 207;
        case 247:
            return 131;
        case 248:
            return 27;
        case 249:
            return 54;
        case 250:
            return 108;
        case 251:
            return 216;
        case 252:
            return 173;
        case 253:
            return 71;
        case 254:
            return 142;
        case 255:
            return 1;
        default:
            //printf("Error in alphToNum");  No longer an error because we need 0 to go to 0
            return 0;
    }
}

/** ----------------------------------------------------------
 * @fn unsigned int numToAlph(unsigned int alph)
 * @brief convert a decimal to a log
 * @param alph, the decimal to change
 * @return unsigned int, the log representation to change the decimal to
 * ----------------------------------------------------------
 */
unsigned int numToAlph(unsigned int alph) {
    switch (alph) {
        case 1:
            return 0;
        case 2:
            return 1;
        case 3:
            return 25;
        case 4:
            return 2;
        case 5:
            return 50;
        case 6:
            return 26;
        case 7:
            return 198;
        case 8:
            return 3;
        case 9:
            return 223;
        case 10:
            return 51;
        case 11:
            return 238;
        case 12:
            return 27;
        case 13:
            return 104;
        case 14:
            return 199;
        case 15:
            return 75;
        case 16:
            return 4;
        case 17:
            return 100;
        case 18:
            return 224;
        case 19:
            return 14;
        case 20:
            return 52;
        case 21:
            return 141;
        case 22:
            return 239;
        case 23:
            return 129;
        case 24:
            return 28;
        case 25:
            return 193;
        case 26:
            return 105;
        case 27:
            return 248;
        case 28:
            return 200;
        case 29:
            return 8;
        case 30:
            return 76;
        case 31:
            return 113;
        case 32:
            return 5;
        case 33:
            return 138;
        case 34:
            return 101;
        case 35:
            return 47;
        case 36:
            return 225;
        case 37:
            return 36;
        case 38:
            return 15;
        case 39:
            return 33;
        case 40:
            return 53;
        case 41:
            return 147;
        case 42:
            return 142;
        case 43:
            return 218;
        case 44:
            return 240;
        case 45:
            return 18;
        case 46:
            return 130;
        case 47:
            return 69;
        case 48:
            return 29;
        case 49:
            return 181;
        case 50:
            return 194;
        case 51:
            return 125;
        case 52:
            return 106;
        case 53:
            return 39;
        case 54:
            return 249;
        case 55:
            return 185;
        case 56:
            return 201;
        case 57:
            return 154;
        case 58:
            return 9;
        case 59:
            return 120;
        case 60:
            return 77;
        case 61:
            return 228;
        case 62:
            return 114;
        case 63:
            return 166;
        case 64:
            return 6;
        case 65:
            return 191;
        case 66:
            return 139;
        case 67:
            return 98;
        case 68:
            return 102;
        case 69:
            return 221;
        case 70:
            return 48;
        case 71:
            return 253;
        case 72:
            return 226;
        case 73:
            return 152;
        case 74:
            return 37;
        case 75:
            return 179;
        case 76:
            return 16;
        case 77:
            return 145;
        case 78:
            return 34;
        case 79:
            return 136;
        case 80:
            return 54;
        case 81:
            return 208;
        case 82:
            return 148;
        case 83:
            return 206;
        case 84:
            return 143;
        case 85:
            return 150;
        case 86:
            return 219;
        case 87:
            return 189;
        case 88:
            return 241;
        case 89:
            return 210;
        case 90:
            return 19;
        case 91:
            return 92;
        case 92:
            return 131;
        case 93:
            return 56;
        case 94:
            return 70;
        case 95:
            return 64;
        case 96:
            return 30;
        case 97:
            return 66;
        case 98:
            return 182;
        case 99:
            return 163;
        case 100:
            return 195;
        case 101:
            return 72;
        case 102:
            return 126;
        case 103:
            return 110;
        case 104:
            return 107;
        case 105:
            return 58;
        case 106:
            return 40;
        case 107:
            return 84;
        case 108:
            return 250;
        case 109:
            return 133;
        case 110:
            return 186;
        case 111:
            return 61;
        case 112:
            return 202;
        case 113:
            return 94;
        case 114:
            return 155;
        case 115:
            return 159;
        case 116:
            return 10;
        case 117:
            return 21;
        case 118:
            return 121;
        case 119:
            return 43;
        case 120:
            return 78;
        case 121:
            return 212;
        case 122:
            return 229;
        case 123:
            return 172;
        case 124:
            return 115;
        case 125:
            return 243;
        case 126:
            return 167;
        case 127:
            return 87;
        case 128:
            return 7;
        case 129:
            return 112;
        case 130:
            return 192;
        case 131:
            return 247;
        case 132:
            return 140;
        case 133:
            return 128;
        case 134:
            return 99;
        case 135:
            return 13;
        case 136:
            return 103;
        case 137:
            return 74;
        case 138:
            return 222;
        case 139:
            return 237;
        case 140:
            return 49;
        case 141:
            return 197;
        case 142:
            return 254;
        case 143:
            return 24;
        case 144:
            return 227;
        case 145:
            return 165;
        case 146:
            return 153;
        case 147:
            return 119;
        case 148:
            return 38;
        case 149:
            return 184;
        case 150:
            return 180;
        case 151:
            return 124;
        case 152:
            return 17;
        case 153:
            return 68;
        case 154:
            return 146;
        case 155:
            return 217;
        case 156:
            return 35;
        case 157:
            return 32;
        case 158:
            return 137;
        case 159:
            return 46;
        case 160:
            return 55;
        case 161:
            return 63;
        case 162:
            return 209;
        case 163:
            return 91;
        case 164:
            return 149;
        case 165:
            return 188;
        case 166:
            return 207;
        case 167:
            return 205;
        case 168:
            return 144;
        case 169:
            return 135;
        case 170:
            return 151;
        case 171:
            return 178;
        case 172:
            return 220;
        case 173:
            return 252;
        case 174:
            return 190;
        case 175:
            return 97;
        case 176:
            return 242;
        case 177:
            return 86;
        case 178:
            return 211;
        case 179:
            return 171;
        case 180:
            return 20;
        case 181:
            return 42;
        case 182:
            return 93;
        case 183:
            return 158;
        case 184:
            return 132;
        case 185:
            return 60;
        case 186:
            return 57;
        case 187:
            return 83;
        case 188:
            return 71;
        case 189:
            return 109;
        case 190:
            return 65;
        case 191:
            return 162;
        case 192:
            return 31;
        case 193:
            return 45;
        case 194:
            return 67;
        case 195:
            return 216;
        case 196:
            return 183;
        case 197:
            return 123;
        case 198:
            return 164;
        case 199:
            return 118;
        case 200:
            return 196;
        case 201:
            return 23;
        case 202:
            return 73;
        case 203:
            return 236;
        case 204:
            return 127;
        case 205:
            return 12;
        case 206:
            return 111;
        case 207:
            return 246;
        case 208:
            return 108;
        case 209:
            return 161;
        case 210:
            return 59;
        case 211:
            return 82;
        case 212:
            return 41;
        case 213:
            return 157;
        case 214:
            return 85;
        case 215:
            return 170;
        case 216:
            return 251;
        case 217:
            return 96;
        case 218:
            return 134;
        case 219:
            return 177;
        case 220:
            return 187;
        case 221:
            return 204;
        case 222:
            return 62;
        case 223:
            return 90;
        case 224:
            return 203;
        case 225:
            return 89;
        case 226:
            return 95;
        case 227:
            return 176;
        case 228:
            return 156;
        case 229:
            return 169;
        case 230:
            return 160;
        case 231:
            return 81;
        case 232:
            return 11;
        case 233:
            return 245;
        case 234:
            return 22;
        case 235:
            return 235;
        case 236:
            return 122;
        case 237:
            return 117;
        case 238:
            return 44;
        case 239:
            return 215;
        case 240:
            return 79;
        case 241:
            return 174;
        case 242:
            return 213;
        case 243:
            return 233;
        case 244:
            return 230;
        case 245:
            return 231;
        case 246:
            return 173;
        case 247:
            return 232;
        case 248:
            return 116;
        case 249:
            return 214;
        case 250:
            return 244;
        case 251:
            return 234;
        case 252:
            return 168;
        case 253:
            return 80;
        case 254:
            return 88;
        case 255:
            return 175;
        default:
            //printf("Error in numToAlph: (%d)\n ",alph);
            return 0;
    }
}

/** ----------------------------------------------------------
 * @fn void unsignedMod(unsigned int *generatorAlph)
 * @brief find the molulo of a number and 255
 * @param *generatirAlph
 * @return void, stores the result back into the array
 * ----------------------------------------------------------
 */
void unsignedMod(unsigned int *generatorAlph) {
    unsigned int temp[11];
    for (int i = 0; i < 11; i++) {
        if (generatorAlph[i] > 254) { temp[i] = generatorAlph[i] - 255; }
        else { temp[i] = generatorAlph[i]; }
    }
    for (int i = 0; i < 11; i++) {
        generatorAlph[i] = temp[i];
    }
}

void flipBit(int a, int b, qr *qr)
{
    if(qr->array[a][b]==1)
    {
        qr->array[a][b]=0;
    }
    else if(qr->array[a][b]==0)
    {
        qr->array[a][b]=1;
    }
    else
        {
        printf("Error in flipBit");
        };
}

