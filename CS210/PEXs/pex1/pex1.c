#include "pex1.h"

int main() {
    printf("Pig!\n\n");
    printf("Testing drawDie()\nWhich die would you like to draw?\n");
    int pip;
    scanf(" %d", &pip);
    drawDie(pip);
    printf("\nTesting turn Status()\nEnter the die, player, current total, player 0 score, and player 1 score (separated by spaces):\n");
    int die, player, currentTotal1, player0, player1 = 0;
    scanf("%d%d%d%d%d", &die, &player, &currentTotal1, &player0, &player1);
    turnStatus(die, player, currentTotal1, player0, player1);
    printf("\nTesting gameStatus()\n");
    gameStatus(player, player0, player1);

    return 0;
}

int totalScore(int score, int turnTotal) {
    return score + turnTotal;
}

void gameStatus(int player, int score0, int score1) {
    printf("Player 0 score: %d\n", score0);
    printf("Player 1 score: %d\n", score1);
    if (score0 >= 100) {
        printf("Player 0 won!\n");
    } else if (score1 >= 100) {
        printf("Player 0 won!\n");
    } else {
        printf("It is Player %d turn.\n", player);
    }
}

void turnStatus(int die, int player, int currentTotal1, int score0, int score1) {
    if (die == 1) {
        printf("Player %d pigged out.\n", player);
    } else {
        printf("Current score for player %d = %d.\n", player, currentTotal1);
        if (player == 1) {
            printf("Total score = %d.\n", score1);
        } else {
            printf("Total score = %d.\n", score0);
        }
    }
}

int rollDie() {
    return rand() % 6 + 1;
}

void drawDie(int pip) {
    switch (pip) {
        case 1:
            printf(" ------- \n");
            printf("|       |\n");
            printf("|   *   |\n");
            printf("|       |\n");
            printf(" ------- \n");
            break;
        case 2:
            printf(" ------- \n");
            printf("| *     |\n");
            printf("|       |\n");
            printf("|     * |\n");
            printf(" ------- \n");
            break;
        case 3:
            printf(" ------- \n");
            printf("| *     |\n");
            printf("|   *   |\n");
            printf("|     * |\n");
            printf(" ------- \n");
            break;
        case 4:
            printf(" -------  \n");
            printf("| *   * |\n");
            printf("|       |\n");
            printf("| *   * |\n");
            printf(" ------- \n");
            break;
        case 5:
            printf(" ------- \n");
            printf("| *   * |\n");
            printf("|   *   |\n");
            printf("| *   * |\n");
            printf(" ------- \n");
            break;
        case 6:
            printf(" ------- \n");
            printf("| *   * |\n");
            printf("| *   * |\n");
            printf("| *   * |\n");
            printf(" ------- \n");
            break;
        default:
            printf("Error drawing dice\n");
    }
}