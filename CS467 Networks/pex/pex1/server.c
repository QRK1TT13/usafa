//Code obtained and modified from https://www.geeksforgeeks.org/udp-server-client-implementation-c/
//DOcumentation: none
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <ctype.h>

#define PORT 4240
#define MAXLINE 1024

void initializeSocket(int *socketfd, struct sockaddr_in *srvaddr);

void getData(int socketfd, struct sockaddr_in *cliaddr, char buffer[1024]);

void sendData(int socketfd, struct sockaddr_in *cliaddr, char *message);

void sendAudioFile(int socketfd, struct sockaddr_in *cliaddr, char *fileName);

int main()
{
    int socketfd;  //Socket descriptor, like a file-handle
    struct sockaddr_in srvaddr, cliaddr; //Stores IP address, address family (ipv4), and port
    char buffer[MAXLINE]; //buffer to store message from client
    initializeSocket(&socketfd, &srvaddr);
    while(1)
    {
        printf("Waiting for connection...\n");
        getData(socketfd, &cliaddr, buffer);
        printf("Data gotten: %s\n", buffer);
        if(strcmp(buffer, "LIST_REQUEST")==0)
        {
            printf("List request received!\n");
            sendData(socketfd, &cliaddr, "LIST_REPLY\nSuzanne Vega - Toms Dinner.mp3\nBilly Joel - We Didn't Start the Fire.mp3\n\0");
            printf("MP3 list sent to client\n");
        }
        else if(strstr(buffer, "Suzanne Vega - Toms Dinner.mp3" )!=NULL)
        {
            sendAudioFile(socketfd, &cliaddr, "Suzanne Vega - Toms Dinner.mp3");
        }
        else if (strstr(buffer, "Billy Joel - We Didn't Start the Fire.mp3")!=NULL)
        {
            sendAudioFile(socketfd, &cliaddr, "Billy Joel - We Didn't Start the Fire.mp3");
        }
        else if(strcmp(buffer, "SEVER_TERMINATE_CONNECTION")==0)
        {
            break;
        }
        else
        {
            sendData(socketfd, &cliaddr, "COMMAND_ERROR\n");
        }
    }
    close(socketfd); // Close socket
    return 0;
}
/**
 * gets the info, calculates, frames, and sends info over to the client
 * @param socketfd
 * @param cliaddr
 * @param fileName
 */
void sendAudioFile(int socketfd, struct sockaddr_in *cliaddr, char *fileName) {
    int bitrate_lookup[] = {0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, -1};
    int samplerate_lookup[] = {44100, 48000, 32000, -1};
    char packetData[1024];
    int frameCounter=0;
    FILE *fp = fopen(fileName, "rb");;
    if (fp == NULL) {
        fprintf(stderr, "cannot open input file\n");
        return;
    }
    unsigned int c = '\xff';
    printf("%x\n",c);
    unsigned int frame1 = '\xff'& 0xff;
    unsigned int frame2 = '\xfb'& 0xff;
    short flip = 0;
    //sendData(socketfd, cliaddr, "STREAM_START");
    while (!feof(fp)){                              // while not end of file
        c=fgetc(fp) & 0xff;                         // get a character/byte from the file
        if(c==frame2&&flip==1)
        {
            //printf("Found frame 0 at \n");
            flip=0;
            break;
        }
        else if (c==frame1)
        {
            flip=1;
        }
        else
        {
            flip=0;
        }
    }                                           //pointer should be after 0xfffb in file in first frame
    int bitrate=0;
    int samplerate=0;
    int paddingbit=0;
    int frameSize=0;
    char *packetHeader = "STREAM_DATA\n";
    while (!feof(fp))
    {
        c=fgetc(fp) & 0xff;
        printf("Bit is : %x\n", c);
        bitrate = bitrate_lookup[c>>4];
        samplerate = samplerate_lookup[(c&0x0c)>>2];
        paddingbit = (c&0x02)>>1;
        frameSize = 0;
        if (paddingbit==1)
        {
            frameSize = ((144 * bitrate *1000)/samplerate)+1;
        }
        else if (paddingbit==0)
        {
            frameSize = (144 * bitrate *1000)/samplerate;
        }
        else
        {
            printf("frame size calculation error: padding bit\n");
        }
        frameCounter++;
        printf("Frame %i found at %li with size %i\n",frameCounter,ftell(fp)-3, frameSize);
        strcpy(packetData, packetHeader);
        fseek(fp, -1, SEEK_CUR);
        for (int i = 0; i<frameSize;i++){packetData[strlen(packetHeader)+i]=fgetc(fp) & 0xff;}
        //printf("Packet data: %s\n",packetData);
        //printf("size of packet is: %lu\n",strlen(packetData));
        socklen_t len=sizeof(*cliaddr);
        sendto(socketfd, (const char *)packetData, frameSize+strlen(packetHeader), 0, (const struct sockaddr *) cliaddr, len);
    }
    sendData(socketfd, cliaddr, "STREAM_DONE");
    fclose(fp);

}
/**
 * sends message to the client
 * @param socketfd
 * @param cliaddr
 * @param message
 */
void sendData(int socketfd, struct sockaddr_in *cliaddr, char *message) {
// Respond to client
    socklen_t len=sizeof(*cliaddr);
    sendto(socketfd, (const char *)message, strlen(message), 0, (const struct sockaddr *) cliaddr, len);
}
/**
 * receive data from the client and pass the info back in the buffer
 * @param socketfd
 * @param cliaddr
 * @param buffer
 */
void getData(int socketfd, struct sockaddr_in *cliaddr, char buffer[1024]) {
    // Receive message from client
    socklen_t len=sizeof(*cliaddr), n; //must initialize len to size of buffer
    if((n = recvfrom(socketfd, (char *)buffer, MAXLINE, 0, ( struct sockaddr *) cliaddr, &len))<0)
    {
        perror("ERROR\n");
        printf("Errno: %d. \n",errno);
    }
    buffer[n] = '\0';  //terminate message
    //("Client : %s\n", buffer);
}
/**
 * gets info about what port and adress and creates a socket ready for client to send data in udp
 * @param socketfd
 * @param srvaddr
 */
void initializeSocket(int *socketfd, struct sockaddr_in *srvaddr) {
    // Creating socket file descriptor
    // AF_INET = "domain" = IPv4
    // SOCK_DGRAM = "type" = UDP, unreliable
    // protocol=0, specifies UDP within SOCK_DGRAM type.
    if ((*socketfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    srvaddr->sin_family = AF_INET;           // IPv4
    srvaddr->sin_addr.s_addr = INADDR_ANY;   // All available interfaces
    srvaddr->sin_port = htons( PORT );       // port, converted to network byte order (prevents little/big endian confusion between hosts)

    // Forcefully attaching socket to the port
    // Bind expects a sockaddr, we created a sockaddr_in.  (struct sockaddr *) casts pointer type to sockaddr.
    if (bind(*socketfd, (const struct sockaddr *)srvaddr, sizeof(*srvaddr))<0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
}
