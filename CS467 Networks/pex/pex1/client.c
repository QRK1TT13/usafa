//Code obtained and modified from https://www.geeksforgeeks.org/udp-server-client-implementation-c/
//DOcumentation: none
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <ctype.h>

#define PORT     4240
#define MAXLINE 1024

void initializeSocket(int *sockfd, struct sockaddr_in *servaddr);

void sendData(int sockfd, struct sockaddr_in servaddr, char *message);

int getData(int sockfd, struct sockaddr_in servaddr, char *buffer);

void getUserOption(int *option);


void printSongs(int sockfd, struct sockaddr_in servaddr);

int strcasecompare(char *header, char *message);

void streamSong(int sockfd, struct sockaddr_in servaddr);

void removeFrameHeaderStream(char *buffer);


int processStreamData(FILE *fp, char buffer[1024], int *frameCount);

// Driver code
int main() {
    int sockfd; //Socket descriptor, like a file-handle
    char buffer[MAXLINE]; //buffer to store message from server
    struct sockaddr_in servaddr;  //we don't bind to a socket to send UDP traffic, so we only need to configure server address
    int option=0;
    initializeSocket(&sockfd, &servaddr);
    while(1)
    {
        getUserOption(&option);
        if(option==1)
        {
            printSongs(sockfd, servaddr);
        }
        else if (option==2)
        {
            streamSong(sockfd, servaddr);
        }
        else
        {
            break;
        }
    }
    close(sockfd);
    return 0;
}
/**
 * ask the server for a song and then collect what the server sends back, then store in the file
 * @param sockfd
 * @param servaddr
 */
void streamSong(int sockfd, struct sockaddr_in servaddr) {
    char songName[MAXLINE];
    char buffer[MAXLINE];
    char requestMessage[MAXLINE];
    strcpy(requestMessage, "START_STREAM\n");
    int frameCount=0;
    while(1)
    {
        printf("Please enter a song name:\n> ");\
//        fseek(stdin,0,SEEK_END);
//        fflush(stdin);
        fgets(songName, MAXLINE, stdin);
        strcat(requestMessage, songName);
        //printf("%s", requestMessage);
        //printf("Sending song request\n");
        sendData(sockfd,servaddr, requestMessage);
        if(getData(sockfd, servaddr, buffer)==0)
        {
            return;
        }
        //printf("%s\n", buffer);
        if(strcasecompare(buffer, "COMMAND_ERROR\n"))
        {
            printf("\nThat was not a valid input.\n\n");
            return;
        }
        break;
    }
    FILE *fp;
    char fileName[MAXLINE];
    strncpy(fileName,songName,strlen(songName)-5);
    strcat(fileName,".mp3");
    fp = fopen(fileName, "wb");
    int end = 0;
    if (fp == NULL) {
        fprintf(stderr, "cannot open output file\n");
        return;
    }
    end = processStreamData(fp, buffer, &frameCount);
    while(end==0)
    {
        if(getData(sockfd, servaddr, buffer)==0)
        {
            return;
        }
        end=processStreamData(fp, buffer, &frameCount);
    }
    printf("\n");
    fclose(fp);
}

int processStreamData(FILE *fp, char buffer[1024], int *frameCount) {
    if(strcasecompare(buffer, "STREAM_DONE"))
    {
        return 1;
    }
    removeFrameHeaderStream(buffer);
    int bitrate_lookup[] = {0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, -1};
    int samplerate_lookup[] = {44100, 48000, 32000, -1};
    int bitrate=0;
    int samplerate=0;
    int paddingbit=0;
    int frameSize =0;
    unsigned int c = buffer[0]& 0xff;
    printf("buffe: %x\n", c);
    bitrate = bitrate_lookup[c>>4];
    samplerate = samplerate_lookup[(c&0x0c)>>2];
    paddingbit = (c&0x02)>>1;
    if (paddingbit==1)
    {
        frameSize = ((144 * bitrate *1000)/samplerate)+1;
    }
    else if (paddingbit==0)
    {
        frameSize = (144 * bitrate *1000)/samplerate;
    }
    else
    {
        printf("frame size calculation error: padding bit\n");
    }
    frameCount++;
    printf("Frame %d found with %d bytes\n", *frameCount, frameSize);
    for(int i = 0; i<frameSize;i++)
    {
        fprintf(fp, "%c", buffer[i]);
    }
    return 0;
}


/**
 * when a frame gets back from the server, it will have a header stating that its stream data, this will remove that
 * @param buffer
 */
void removeFrameHeaderStream(char *buffer) {
    char* header = "STREAM_DATA\n";
    for (int i = 0; i<15;i++)
    if(strncmp(buffer, header, strlen(header))!=0)
    {
        printf("Invalid response from server");
        return;
    }
    char newBuffer[MAXLINE];
    for(int i =0; i<strlen(buffer)-strlen(header);i++)
    {
        newBuffer[i] = buffer[strlen(header)+i];
    }
    strcpy(buffer, newBuffer);
}
/**
 * ask server for the list of songs that are availible and then print them to the screen
 * @param sockfd
 * @param servaddr
 */
void printSongs(int sockfd, struct sockaddr_in servaddr) {
    sendData(sockfd, servaddr, "LIST_REQUEST");
    printf("Requesting a list of songs\n\n");
    char response[MAXLINE];
    if (getData(sockfd, servaddr, response)==0)
    {
        return;
    }
    if (strstr(response, "LIST_REPLY")!=NULL)
    {
        for(int i = strlen("LIST_REPLY");i<(int)strlen(response);i++)
        {
            printf("%c", response[i]);
        }
    }
    printf("\n");
}
/**
 * compare the strings even if they have different cases
 * @param header
 * @param message
 * @return
 */
int strcasecompare(char *header, char *message) {
    if(strlen(header)!=strlen(message))
    {
        return 0;
    }
    for(int i = 0; i<(int)strlen(message);i++)
    {
        if(tolower(header[i])!=tolower(message[i]))
        {
            return 0;
        }
    }
    return 1;
}
/**
 * gets which action the user wants to do, with error checking
 * @param option
 */
void getUserOption(int * option) {
    do{
        printf("Please choose one of the following options:\n");
        printf("[1] List the songs to stream\n");
        printf("[2] Stream a song\n");
        printf("[3] Exit\n> ");
        fseek(stdin,0,SEEK_END);
        scanf("%d",option);
        if(*option>=1&&*option<=3)
        {
            break;
        }
        printf("\nThat was not a valid input. Please try again.\n");
    }while(1);
}
/**
 * get data from the server, this impliments a timeout incase the server is not running
 * @param sockfd
 * @param servaddr
 * @param buffer
 * @return
 */
int getData(int sockfd, struct sockaddr_in servaddr, char *buffer) {
    int n = sizeof(servaddr);
    socklen_t len = sizeof(servaddr);
    //setup time for timeout
    struct timeval timeout; //structure to hold our timeout
    timeout.tv_sec = 5; //5 second timeout
    timeout.tv_usec = 0; //0 milliseconds
    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(timeout)) < 0){
        perror("setsockopt failed");
        return 0;
    }

    // Receive message from server
    if(( n = recvfrom(sockfd, (char *)buffer, MAXLINE, 0, (struct sockaddr *) &servaddr, &len))<0)
    {
        printf("The connection timed out while connecting to the server. Please make sure the server is running next time\n");
        return 0;
    }
    buffer[n] = '\0'; //terminate message
    //printf("Server : %s\n", buffer);
    return 1;
}
/**
 * send data to the server in the proper format
 * @param sockfd
 * @param servaddr
 * @param message
 */
void sendData(int sockfd, struct sockaddr_in servaddr, char *message) {
    //printf("Sending something\n");
    socklen_t len = sizeof(servaddr);
    //Sending message to server
    sendto(sockfd, (const char *)message, strlen(message), 0, (const struct sockaddr *) &servaddr, sizeof(servaddr));
}
/**
 * create a socket that it is able to listen for the server on
 * @param sockfd
 * @param servaddr
 */
void initializeSocket(int *sockfd, struct sockaddr_in *servaddr) {
    // Creating socket file descriptor
    if ( (*sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
    // Filling server information
    servaddr->sin_family = AF_INET; //IPv4
    servaddr->sin_port = htons(PORT); // port, converted to network byte order (prevents little/big endian confusion between hosts)
    servaddr->sin_addr.s_addr = INADDR_ANY; //localhost
}
